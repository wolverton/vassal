import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys
import numpy as np

## Read in data
if (len(sys.argv) != 2):
	print "Usage: python %s [filename]"%(sys.argv[0])
	sys.exit(-1)
fp = open(sys.argv[1], 'r')
fp.readline()
cellvecs = [];
for i in range(3):
	words = fp.readline().split()
	cellvecs.append(np.array([ float(x) for x in words ]))
fp.readline()
edges = []
points = []
for line in fp:
	words = line.split()
	if len(words) != 6: continue
	p1 = [float(x) for x in words[:3]]
	p2 = [float(x) for x in words[3:]]
	edges.append([p1, p2])
	points.append(p1)
	points.append(p2)
fp.close()
print "Read in %d edges from %s"%(len(edges), sys.argv[1])

## Plot the cell
fig = plt.figure()
ax = fig.gca(projection='3d')
for i in range(3):
	toPlot = zip([0, 0, 0], cellvecs[i]);
	ax.plot(toPlot[0], toPlot[1], toPlot[2], 'k--')
	other_dirs = [ (i + j + 1) % 3 for j in range(2) ]
	starts = [cellvecs[other_dirs[0]], cellvecs[other_dirs[1]],
		cellvecs[other_dirs[0]] + cellvecs[other_dirs[1]]]
	for start in starts:
		end = start + cellvecs[i]
		toPlot = zip(start, end);
		ax.plot(toPlot[0], toPlot[1], toPlot[2], 'k--')

## Plot each edge
for edge in edges:
	x = [edge[0][0], edge[1][0]]
	y = [edge[0][1], edge[1][1]]
	z = [edge[0][2], edge[1][2]]
	ax.plot(x, y, z)
xs = [ x[0] for x in points ]
ys = [ x[1] for x in points ]
zs = [ x[2] for x in points ]
ax.scatter(xs,ys,zs)

## Make it pretty
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
plt.show()
