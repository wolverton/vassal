package vassal.analysis.voronoi;

import vassal.analysis.voronoi.java.VoronoiCell;
import java.util.*;
import vassal.data.*;
import vassal.analysis.PairDistanceAnalysis;
import vassal.analysis.voronoi.voroplusplus.VoroPlusPlusWrapper;

/**
 * Computes Voronoi tessellation of a cell.
 *
 * @author Logan Ward
 */
abstract public class VoronoiTessellationCalculator {

    /**
     * Whether to use Voro++, or local implementation
     */
    static public boolean UseVoroPlusPlus = true;

    /**
     * Compute Voronoi tessellation of a structure. Can either use a standard
     * Voronoi tessellation or the radical plane method. The radical plane
     * method takes the radii of atoms into account when partitioning the cell.
     *
     * <p>
     * Citation for Radical Voronoi method:
     * <br><a href="http://www.sciencedirect.com/science/article/pii/002230938290093X">
     * Gellatly and Finney. JNCS (1970)</a>
     *
     * @param cell Structure to analyze
     * @param radical Whether to use the radical-plane Voronoi method
     * @return Voronoi cell for each atom
     * @throws java.lang.Exception If structure fails to be created
     */
    static public List<BaseVoronoiCell> compute(Cell cell, boolean radical)
            throws Exception {
        if (UseVoroPlusPlus && cell.nAtoms() > 2) {
            return VoroPlusPlusWrapper.runVoroPlusPlus(cell, radical);
        } else {
            return runLocalTessellation(cell, radical);
        }
    }

    /**
     * Run a tessellation using the Java implementation of the voronoi
     * tessellation provided with Vassal.
     *
     * <p>
     * Citation for the computation method:
     * <br><a href="http://linkinghub.elsevier.com/retrieve/pii/0021999178901109">
     * Brostow, Dessault, Fox. JCP (1978)</a>
     *
     * @param cell Cell to be tessellation
     * @param radical Whether to perform a radical plane tessellation
     * @return Voronoi cell for each atom
     * @throws Exception
     */
    public static List<BaseVoronoiCell> runLocalTessellation(Cell cell, boolean radical) throws Exception {
        List<BaseVoronoiCell> output = new ArrayList<>(cell.nAtoms());

        // Initialize Voronoi cells
        List<Atom> atoms = cell.getAtoms();
        for (Atom atom : atoms) {
            output.add(new VoronoiCell(atom, radical));
        }

        // Create tool to find closest images
        PairDistanceAnalysis imageFinder = new PairDistanceAnalysis();
        double atomLengthScale = Math.pow(cell.volume() / cell.nAtoms(), 1.0 / 3.0);
        imageFinder.analyzeStructure(cell);

        // Generate cells
        for (BaseVoronoiCell c : output) {
            // Cast as a VoronoiCell
            ((VoronoiCell) c).computeCell(imageFinder, atomLengthScale * 6);
        }

        // Read the
        return output;
    }
}
