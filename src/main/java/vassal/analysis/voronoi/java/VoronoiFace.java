package vassal.analysis.voronoi.java;

import java.util.*;
import org.apache.commons.math3.geometry.euclidean.threed.*;
import vassal.analysis.voronoi.BaseVoronoiEdge;
import vassal.analysis.voronoi.BaseVoronoiFace;
import vassal.analysis.voronoi.BaseVoronoiVertex;
import vassal.data.Atom;
import vassal.data.AtomImage;

/**
 * Representation of a face on a Voronoi diagram.
 * @author Logan Ward
 */
public class VoronoiFace extends BaseVoronoiFace {

	/**
	 * Create a blank Voronoi face associated with a neighboring atom. It will be
	 *  necessary to define the edges.
	 *
     * @param insideAtom Atom "inside" this face
     * @param outsideAtom; Image of atom "outside" this face
     * @param radical Whether this face is in a radical plane tessellation
     * @throws java.lang.Exception
	 */
	public VoronoiFace(Atom insideAtom, AtomImage outsideAtom, boolean radical)
            throws Exception {
        super(insideAtom, outsideAtom, radical);
	}

    /**
     * Given a list of faces that will intersect this face, construct the edges.
     * Used to construct faces for the direct polyhedron only! It assumes that
     * {@linkplain #FaceCenter} is inside the center of the face
     *
     * @param faces [in] Other faces
     * @return Whether the face assembled correctly
     * @throws java.lang.Exception
     */
    public boolean assembleFaceFromFaces(Collection<VoronoiFace> faces)
            throws Exception {
        // Generate a local copy of this list
        List<VoronoiFace> otherFaces = new ArrayList(faces);
        otherFaces.remove(this);

        // Find all edges
        List<BaseVoronoiEdge> availableEdges = new ArrayList<>(otherFaces.size());
        for (VoronoiFace otherFace : otherFaces) {
            // Create edge
            VoronoiEdge edge;
            try {
                edge = new VoronoiEdge(this, otherFace);
            } catch (Exception e) {
                continue;
            }

            // Store the data
            availableEdges.add(edge);
        }

        // Error check
        if (availableEdges.size() < 3) {
            throw new Exception("Not enough edges!");
        }

        // Sort edges by distance to the face center
        Collections.sort(availableEdges, new Comparator<BaseVoronoiEdge>() {
            @Override
            public int compare(BaseVoronoiEdge o1, BaseVoronoiEdge o2) {
                double dist1 = o1.getLine().distance(FaceCenter);
                double dist2 = o2.getLine().distance(FaceCenter);
                return Double.compare(dist1, dist2);
            }
        });

        return assembleFaceFromEdges(availableEdges);
    }

        /**
     * Compute intersection between this face and a new face. If the face intersects,
     * update the list of edges.
     *
     * <p>Special case: If a current edge of this face is completely on the new
     * face (i.e., both vertices are on the face), that edge will be replaced
     * with a new edge that is the result of the intersection between the new face
     * and this face.
     * @param newFace Face in consideration
     * @return If the new face intersect, return the pair of the edge that was formed. Returns
     * null if no edge was formed.
     * @see VoronoiEdge#generatePair()
     */
    public VoronoiEdge computeIntersection(VoronoiFace newFace) throws Exception {
        // Clear cached area
        FaceArea = Double.NaN;
        // See if any vertex is outside this new face
        //   or if any vertices are on this face
        BaseVoronoiVertex outsideVertex = null;
        int nContacting = 0;
        for (BaseVoronoiVertex v : Vertices) {
            int relPos = newFace.positionRelativeToFace(v.getPosition());
            if (relPos > 0) {
                outsideVertex = v;
                break;
            } else if (relPos == 0) {
                nContacting++;
            }
        }
        // If it doesn't intersect, return "null"
        if (outsideVertex == null) {
            // Check if two vertices are on the new face. If so, they must
            //  share an edge because these faces are convex
            if (nContacting < 2) {
                return null;
            }
            // If so, find what edge they correspond to
            BaseVoronoiEdge newEdge;
            BaseVoronoiEdge edgeToReplace;
            try {
                newEdge = new VoronoiEdge(this, newFace);
            } catch (Exception e) {
                throw new Exception("New face does not intersect this face.");
            }
            try {
                edgeToReplace = null;
                for (BaseVoronoiEdge edge : Edges) {
                    if (newFace.positionRelativeToFace(edge.getStartVertex().getPosition()) == 0 && newFace.positionRelativeToFace(edge.getEndVertex().getPosition()) == 0) {
                        edgeToReplace = edge;
                        break;
                    }
                }
            } catch (Exception e) {
                throw new Exception("Face was not closed to begin with");
            }
            // Compute the new edges
            if (edgeToReplace == null) {
                throw new Exception("Edge on the new face was not found.");
            }
            try {
                VoronoiEdge.joinEdges(newEdge, edgeToReplace.getPreviousEdge());
                VoronoiEdge.joinEdges(newEdge, edgeToReplace.getNextEdge());
            } catch (Exception e) {
                throw new Exception("New edge and old are not parallel.");
            }
            // Replace edge in the lineup
            Edges.set(Edges.indexOf(edgeToReplace), newEdge);
            // Recompute vertices
            computeVertices();
            // Return pair of this edge
            return (VoronoiEdge) newEdge.generatePair();
        }

        // Get this vertex's ID number
        int outsideVertexID = Vertices.indexOf(outsideVertex);
        // Traverse forward until a vertex inside is found
        int lastVertex = outsideVertexID;
        while (newFace.positionRelativeToFace(Vertices.get(lastVertex).getPosition()) >= 0) {
            lastVertex = (lastVertex + 1) % Vertices.size();
            // If we go all the way around the loop, all vertices are outside
            //  this face. Clear everything and return "null" because no
            //  edge is formed
            if (outsideVertexID == lastVertex) {
                Edges.clear();
                Vertices.clear();
                return null;
            }
        }
        // Traverse backwords until you find the first vertex that is
        //  inside the intersecting plane
        int firstVertex = outsideVertexID;
        while (newFace.positionRelativeToFace(Vertices.get(firstVertex).getPosition()) >= 0) {
            firstVertex -= 1;
            if (firstVertex < 0) {
                firstVertex += Vertices.size();
            }
        }
        // Create the new edge
        VoronoiEdge newEdge;
        try {
            newEdge = new VoronoiEdge(this, newFace);
        } catch (Exception e) {
            throw new Exception("New face doesn't intersect this face!");
        }
        VoronoiEdge startEdge = (VoronoiEdge) ((VoronoiVertex) Vertices.get(firstVertex)).getNextEdge();
        VoronoiEdge endEdge = (VoronoiEdge) ((VoronoiVertex) Vertices.get(lastVertex)).getPreviousEdge();
        // Eliminate edges that are outside the face
        VoronoiEdge curEdge = (VoronoiEdge) startEdge.getNextEdge();
        while (curEdge != endEdge) {
            if (!Edges.remove(curEdge)) {
                throw new Exception("Edge not found");
            }
            curEdge = curEdge.getNextEdge();
        }
        // Join start and edges with this new edge
        try {
            VoronoiEdge.joinEdges(startEdge, newEdge);
            VoronoiEdge.joinEdges(endEdge, newEdge);
        } catch (Exception e) {
            throw new Exception("New edge doesn't intersect current ones!");
        }
        // Add it to the Edges list
        Edges.add(Edges.indexOf(startEdge) + 1, newEdge);
        // Recompute vertices
        computeVertices();
        // Return the "pair" of the new edge
        return newEdge.generatePair();
    }

    /**
     * Reset edges to a previous state. Used in case cell intersection fails
     * @param edges Previous list of edges
     * @see VoronoiCell#computeIntersection(vassal.analysis.voronoi.VoronoiFace)
     */
    public void resetEdges(List<BaseVoronoiEdge> edges) {
        try {
            for (int i = 0; i < edges.size(); i++) {
                VoronoiEdge.joinEdges(edges.get(i), edges.get((i + 1) % edges.size()));
            }
            Edges.clear();
            Edges.addAll(edges);
            computeVertices();
        } catch (Exception e) {
            throw new Error("Was the stored state in error?");
        }
    }

    /**
     * Given list of edges, assemble face.
     * @param availableEdges List of available edges
     * @return Whether face is closed
     * @throws java.lang.Exception If face assembly fails
     */
    public boolean assembleFaceFromEdges(List<BaseVoronoiEdge> availableEdges)
            throws Exception {
        // Clear cached face area
        FaceArea = Double.NaN;

        // The closest edge is certainly on the face
        List<BaseVoronoiEdge> faceEdges = new LinkedList<>();
        faceEdges.add(availableEdges.get(0));
        VoronoiEdge curEdge = (VoronoiEdge) faceEdges.get(0);

        // Now, loop through all the edges and compute their intersection with
        //  all other edges on the face. This is accomplished by finding the
        //  next edge and proceeding until we reach the beginning or none is found
        while (true) {
            VoronoiEdge nextEdge = curEdge.findNextEdge(availableEdges);
            if (nextEdge == faceEdges.get(0)) {
                break;
            } else if (nextEdge == null) {
                throw new Exception("Face is not closed");
            } else if (faceEdges.size() > availableEdges.size()) {
                // This happens if the first vertex is not a valid edge.
                //  Once the edge finder makes a complete circuit, it doesn't
                //  find the first edge and continues from a different starting
                //  point. To address this, easy way is to remove the problem vertex
                //  and try again
                List<BaseVoronoiEdge> newAvail = new ArrayList<>(availableEdges);
                newAvail.remove(0);
                if (newAvail.size() < 3) {
                    throw new Exception("Face failed to build");
                }
                return assembleFaceFromEdges(newAvail);
            }
            faceEdges.add(nextEdge);
            curEdge = nextEdge;
        }

        // Compute intersections, and add edges
        Edges.clear();
        for (int i=0; i<faceEdges.size(); i++) {
            VoronoiEdge.joinEdges(faceEdges.get(i),
                    faceEdges.get((i + 1) % faceEdges.size()));
            Edges.add(faceEdges.get(i));
        }

        // Store vertices
        computeVertices();

        return isClosed();
    }

    /**
     * Recompute vertices. Should be run after each time the edges are updated
     * @throws java.lang.Exception
     */
    protected void computeVertices() throws Exception {
        Vertices.clear();
        try {
            for (BaseVoronoiEdge edge : Edges) {
                Vertices.add(edge.getStartVertex());
            }
        } catch (Exception e) {
            throw new Exception("Vertex computation error");
        }

        // Remove area
        getArea();
    }


    /**
     * Compute length of new edge created by intersection.
     *
     * @param newFace Face intersecting with this one
     * @return Cut length
     */
    public double getCutLength(VoronoiFace newFace) {
        // Find a single vertex that is outside the face
        BaseVoronoiVertex outsideVertex = null;
        for (BaseVoronoiVertex vert : Vertices) {
            if (newFace.positionRelativeToFace(vert.getPosition()) > 0) {
                outsideVertex = vert;
                break;
            }
        }

        // If no vertex is outside, cut length == 0
        if (outsideVertex == null) {
            return 0;
        }

        // Get this vertex's ID number
        int outsideVertexID = Vertices.indexOf(outsideVertex);

        // Traverse forward until a vertex inside is found
        int lastVertex = outsideVertexID;
        while (newFace.positionRelativeToFace(
                Vertices.get(lastVertex).getPosition()) >= 0) {
            lastVertex = (lastVertex + 1) % Vertices.size();
            // If we go all the way around the loop, all vertices are outside
            //  this face. Clear everything and return "null" because no
            //  edge is formed
            if (outsideVertexID == lastVertex) {
                Edges.clear();
                Vertices.clear();
                return Double.POSITIVE_INFINITY;
            }
        }

        // Traverse backwords until you find the first vertex that is
        //  inside the intersecting plane
        int firstVertex = outsideVertexID;
        while (newFace.positionRelativeToFace(
                Vertices.get(firstVertex).getPosition()) >= 0) {
            firstVertex -= 1;
            if (firstVertex < 0) {
                firstVertex += Vertices.size();
            }
        }

        // Create the new edge
        VoronoiEdge newEdge;
        try {
            newEdge = new VoronoiEdge(this, newFace);
        } catch (Exception e) {
            throw new Error("New face doesn't intersect this face!");
        }
        VoronoiEdge startEdge = (VoronoiEdge) ((VoronoiVertex) Vertices.get(firstVertex)).getNextEdge();
        VoronoiEdge endEdge = (VoronoiEdge) ((VoronoiVertex) Vertices.get(lastVertex)).getPreviousEdge();
        double dist = Vector3D.distance(startEdge.getLine().intersection(newEdge.getLine()),
                endEdge.getLine().intersection(newEdge.getLine()));
        return dist;
    }

    /**
     * Compute whether another face intersects this one. This occurs if any
     * vertex is outside the other face.
     * @param otherFace Face of interest
     * @return Whether the cut created by this intersection is greater than the
     * minimum edge length
     */
    public boolean isIntersectedBy(VoronoiFace otherFace) {
        for (BaseVoronoiVertex v : Vertices) {
            if (otherFace.positionRelativeToFace(v.getPosition()) >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check whether this face is contacted by another face. This occurs
     * if any of the vertices are outside or on the other face.
     * @param otherFace Face of interest
     * @return Whether it contacts this face
     */
    public boolean isContactedBy(VoronoiFace otherFace) {
        for (BaseVoronoiVertex v : Vertices) {
            if (otherFace.positionRelativeToFace(v.getPosition()) >= 0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Compute whether this face is completely outside of this one
     * @param otherFace Face of interest
     * @return Whether any vertex of this face is in outside or <i>in contact</i>
     * with the other face
     */
    public boolean isCompletelyOutside(VoronoiFace otherFace) {
        // Check whether any vertices are outside this face
        for (BaseVoronoiVertex v : Vertices) {
            if (otherFace.positionRelativeToFace(v.getPosition()) < 0) {
                return false;
            }
        }
        return true;
    }


	/**
	 * Check whether face contains vertex
	 * @param vertex Vertex in question
	 * @return Whether it contains vertices
	 */
	public boolean containsVertex(VoronoiVertex vertex) {
		return Vertices.contains(vertex);
	}


	@Override
	public boolean isClosed() {
		if (Edges.size() < 3) {
            return false;
        }
        for (int i=0; i<Vertices.size()-1; i++) {
            if (! (Edges.contains(Edges.get(i).getNextEdge()) &&
                    Edges.contains(Edges.get(i).getPreviousEdge()))) {
                return false;
            }
        }
        return true;
	}

}
