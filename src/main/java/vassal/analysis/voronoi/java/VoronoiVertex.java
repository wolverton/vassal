package vassal.analysis.voronoi.java;

import vassal.analysis.voronoi.BaseVoronoiEdge;
import vassal.analysis.voronoi.BaseVoronoiVertex;

/**
 * Voronoi vertex for Java implementation
 * @author Logan Ward
 */
public class VoronoiVertex extends BaseVoronoiVertex {
    /** Edge that is after this vertex */
    protected BaseVoronoiEdge NextEdge;
    /** Edge that is before this vertex */
    protected BaseVoronoiEdge PreviousEdge;

    /**
     * Create a new vertex as the intersection of two edges.
	 *  for the first time.
	 * @param edge1 First edge
	 * @param edge2 Second edge
     * @throws java.lang.Exception
     */
    public VoronoiVertex(VoronoiEdge edge1, VoronoiEdge edge2) throws Exception {
        super(edge1.getEdgeFace().getInsideAtom(), edge1.getLine().intersection(edge2.getLine()));

        // Store next and previous edges
        if (edge1.isCCW(edge2)) {
            PreviousEdge = edge1;
            NextEdge = edge2;
        } else {
            PreviousEdge = edge2;
            NextEdge = edge1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof VoronoiVertex) {
            VoronoiVertex other = (VoronoiVertex) obj;
            return other.PreviousEdge.equals(PreviousEdge) &&
                    other.NextEdge.equals(NextEdge);
        }
        return false;
    }

    @Override
    public int compareTo(BaseVoronoiVertex other) {
        // Cast this vertex as a VoronoiVertex type
        VoronoiVertex o = (VoronoiVertex) other;
        // Do the comparison
        if (PreviousEdge.equals(o.PreviousEdge)) {
            return NextEdge.compareTo(o.NextEdge);
        } else {
            return PreviousEdge.compareTo(o.PreviousEdge);
        }
    }

    /**
     * Get edge after this vertex
     * @return Next edge
     */
    public BaseVoronoiEdge getNextEdge() {
        return NextEdge;
    }

    /**
     * Get the edge before this vertex
     * @return Previous edge
     */
    public BaseVoronoiEdge getPreviousEdge() {
        return PreviousEdge;
    }

    /**
     * Check if a vertex is on a certain edge
     * @param edge Edge in question
     * @return Whether the vertex is at the end of this edge
     */
    public boolean isOnEdge(BaseVoronoiEdge edge) {
        return PreviousEdge.equals(edge) || NextEdge.equals(edge);
    }

}
