package vassal.analysis.voronoi.voroplusplus;

import java.util.Set;
import java.util.TreeSet;
import vassal.analysis.voronoi.BaseVoronoiEdge;
import vassal.analysis.voronoi.BaseVoronoiFace;
import vassal.analysis.voronoi.BaseVoronoiVertex;

/**
 * Implementation of a Voronoi edge for use with Voro++
 * @author Logan Ward
 */
public class VoronoiEdge extends BaseVoronoiEdge {
    /** Vertex at the beginning of the edge */
    final protected BaseVoronoiVertex StartVertex;
    /** Vertex at the end of the edge */
    final protected BaseVoronoiVertex EndVertex;

    /**
     * Create a new Voronoi edge from the start and end vertexes
     * @param startVertex Vertex at start of edge
     * @param endVertex Vertex at end of edge
     * @param edgeFace Face on which this edge resides
     */
    public VoronoiEdge(BaseVoronoiVertex startVertex,
            BaseVoronoiVertex endVertex, BaseVoronoiFace edgeFace) {
        // Define the start and end vertices
        StartVertex = startVertex;
        EndVertex = endVertex;

        // Define the edge face
        EdgeFace = edgeFace;

        // Determine the intersecting face
        Set<VoronoiFace> faceCandidates = ((VoronoiVertex) startVertex).getFaces();
        faceCandidates.retainAll(((VoronoiVertex) endVertex).getFaces());
        faceCandidates.remove((VoronoiFace) edgeFace);
        if (faceCandidates.size() != 1) {
            throw new RuntimeException("Found wrong number of shared faces: " + faceCandidates.size());
        }
        IntersectingFace = faceCandidates.iterator().next();
    }

    @Override
    public BaseVoronoiVertex getEndVertex() throws Exception {
        return EndVertex;
    }

    @Override
    public BaseVoronoiVertex getStartVertex() throws Exception {
        return StartVertex;
    }

    @Override
    public BaseVoronoiEdge generatePair() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
