package vassal.analysis.voronoi.voroplusplus;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.analysis.function.Rint;
import org.apache.commons.math3.exception.MathArithmeticException;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealVector;
import vassal.Vassal;
import vassal.analysis.voronoi.BaseVoronoiCell;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.data.Cell;

/**
 * Class to interface with Voro++. Calls a special interface to that library in
 * order to perform a tessellation.
 *
 * @author Logan Ward
 */
public class VoroPlusPlusWrapper {
    static boolean DebugExeSearch = false;

    /**
     * Use Voro++ to perform a Voronoi tessellation
     * @param strc Structure to be tessellated
     * @param radical Whether to perform a radical plane tessellation
     * @return Result as a list of voronoi cells, in same order as atoms in
     * structure
     * @throws java.lang.Exception
     */
    public static List<BaseVoronoiCell> runVoroPlusPlus(Cell strc, boolean radical)
            throws Exception {

        // Launch Voro++ wrapper
        String exePath = locateExecutable();
        final Process voroPlusPlus = new ProcessBuilder(exePath).start();

        // Launch a thread to clear crap out of the stderr
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    while (voroPlusPlus.getErrorStream().read() != -1) {};
                } catch (IOException e) {
                    return;
                }
            }
        });
        t.start();

        // Send structure to process
        //writeStructure(new FileOutputStream("temp.file"), strc, radical);
        writeStructure(voroPlusPlus.getOutputStream(), strc, radical);

        // Parse results
        List<BaseVoronoiCell> output;
        try {
            BufferedReader fp = new BufferedReader(new InputStreamReader(voroPlusPlus.getInputStream()));
            output = parseResults(strc, radical, fp);
        } catch (Exception e) {
            voroPlusPlus.destroy();
            System.err.println("Voro++ failed due to a: " + e.toString());
            throw e;
        }

        // Kill process
        voroPlusPlus.destroy();
        return output;
    }

    /**
     * Determine the path to the Voro++ executable
     * @return Path to executable if found. Otherwise, assumes it is on the path
     */
    protected static String locateExecutable() {
        // Get name of executable
        String name;
        if (System.getProperty("os.name").startsWith("Windows")) {
            name = "voroExt_windows.exe";
        } else {
            name = "voroExt.exe";
        }

        // Get the path of Vassal
        File vassalPath = null;
        try {
            vassalPath = new File(Vassal.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            // Nothing
        }

        // Get directories to look in
        List<File> dirs = new ArrayList<>();
        dirs.add(new File("."));
        dirs.add(new File("voro++"));
        dirs.add(new File("exes"));
        if (vassalPath != null) {
            dirs.add(vassalPath);
            dirs.add(vassalPath.getParentFile());
            dirs.add(vassalPath.getParentFile().getParentFile());
            dirs.add(new File(vassalPath, "voro++"));
            dirs.add(new File(vassalPath, "exes"));
            dirs.add(new File(vassalPath.getParentFile(), "voro++"));
            dirs.add(new File(vassalPath.getParentFile(), "exes"));
            dirs.add(new File(vassalPath.getParentFile().getParentFile(), "voro++"));
            dirs.add(new File(vassalPath.getParentFile().getParentFile(), "exes"));
        }
        if (DebugExeSearch) {
            DebugExeSearch = false;
            System.out.println("Looking for " + name + " in:");
            for (File dir : dirs) {
                System.out.println("\t" + dir.getAbsolutePath());
            }
        }

        // Look for the file
        for (File dir : dirs) {
            File path = new File(dir, name);
            if (path.isFile()) {
                // Make sure it is executable
                if (! path.canExecute()) {
                    path.setExecutable(true);
                }
                return path.getAbsolutePath();
            }
        }

        // Otherwise, just assume it is ont the path
        return name;
    }

    /**
     * Write structure to voro++ process
     * @param outStream Already-started voroPlusPlus process
     * @param strc Structure to be tessellated
     * @param radical Whether to perform a radical plane tessellation
     */
    protected static void writeStructure(OutputStream outStream, Cell strc, boolean radical) {
        // Open up the print writier
        PrintWriter fo = new PrintWriter(new OutputStreamWriter(outStream));

        // Print out basis
        double[][] alignedBasis = strc.getAlignedBasis();
        fo.format("basis %.9f %.9f %.9f %.9f %.9f %.9f\n", alignedBasis[0][0],
                alignedBasis[0][1], alignedBasis[1][1],
                alignedBasis[0][2], alignedBasis[1][2], alignedBasis[2][2]);

        // Print out periodicity
        fo.print("periodicity");
        for (int d=0; d<3; d++) {
            fo.print(" ");
            fo.print(strc.directionIsPeriodic(d) ? "True" : "False");
        }
        fo.println();

        // Print out number of atoms
        fo.format("natoms %d\n", strc.nAtoms());

        // Print out whether to do radical tessellation
        fo.format("poly %s\n", radical ? "True" : "False");

        // Print out atoms
        for (Atom atom : strc.getAtoms()) {
            // Print fractional coordinates
            for (double x : atom.getPosition()) {
                fo.format("%.9f ", x);
            }

            // If radical, write out radius
            if (radical) {
                fo.format("%.9f", atom.getRadius());
            }

            fo.println();
        }

        fo.flush();
    }

    /**
     * Parse a Voronoi tessellation
     *
     * @param strc Structure being tessellated
     * @param fp Output from Voro++ wrapper
     * @param radical Whether Voro++ performed radical plane tessellation
     * @return Tessellation
     * @throws java.lang.Exception
     */
    public static List<BaseVoronoiCell> parseResults(Cell strc, boolean radical,
            BufferedReader fp) throws Exception {
        // Read in the basis and make sure it is correct
        checkBasis(fp, strc);

        // Check periodicity of structure
        checkPeriodicity(fp, strc);

        // Check whether Voro++ ran correct type of tessellation
        checkPolydisperse(fp, radical);

        // Check the atom positions and types
        checkAtoms(fp, strc);

        // Load in the tessellation
        BaseVoronoiCell[] output = new BaseVoronoiCell[strc.nAtoms()];
        for (int a = 0; a < strc.nAtoms(); a++) {
            // Read in cell
            Pair<Integer, VoronoiCell> cell = createCell(fp, strc, radical);

            // Make sure the atom ID is correct
            output[cell.getKey()] = cell.getValue();
        }

        return Arrays.asList(output);
    }

    /**
     * Read in a Voronoi cell from Voro++ output
     *
     * @param fp Output of Voro++, just before start of new atom block
     * @param strc Structure being tessellated
     * @param radical Whether a radical plane tessellation is being performed
     * @return Voronoi cell for this atom
     * @throws Exception
     */
    private static Pair<Integer, VoronoiCell>
            createCell(BufferedReader fp, Cell strc, boolean radical) throws Exception {
        // Make a version of the structure with the basis aligned with Voro++ output
        Cell strcAligned = strc.clone();
        strcAligned.setBasis(strc.getAlignedBasis());

        // First line: Atom ID
        String line = fp.readLine();
        String[] words = line.split(" ");
        int atomID = Integer.parseInt(words[1]);
        Atom atom = strc.getAtom(atomID);
        Atom atomAligned = strcAligned.getAtom(atomID);

        // Get displacement between Vassal and voro++ coordinates
        Vector3D voroDisp = getVoroPlusPlusPositionDelta(strcAligned, atomID);
        Vector3D expectedPos = new Vector3D(atomAligned.getPositionCartesian())
                .add(voroDisp);

        // Second line: Position. Check if it is actually
        line = fp.readLine();
        words = line.split(" ");
        double[] actualPosAr = new double[3];
        for (int d=0; d<3; d++) {
            double pos = Double.parseDouble(words[d+1]);
            double diff = Math.abs(pos - expectedPos.toArray()[d]);
            actualPosAr[d] = pos;
        }
        Vector3D actualPos = new Vector3D(actualPosAr);
        if (actualPos.distance(expectedPos) > 1e-6) {
            // Assume there is some kind of numerical issue
            //  Ex: A coordinate is negative, but almost zero
            voroDisp = actualPos.subtract(new Vector3D(atomAligned.getPositionCartesian()));
        }

        // Third line: Volume (store as checksum)
        line = fp.readLine();
        double volume = Double.parseDouble(line.split(" ")[1]);

        // Read in vertices
        line = fp.readLine();
        int nVerts = Integer.parseInt(line.split(" ")[1]);
        List<Vector3D> vertexPos = new ArrayList<>(nVerts);
        for (int v = 0; v < nVerts; v++) {
            // Read in positions
            line = fp.readLine();
            words = line.split(" ");

            // Convert to double
            double[] pos = new double[3];
            for (int d = 0; d < 3; d++) {
                pos[d] = Double.parseDouble(words[d]);
            }

            // Displace it to be consistant with Voro++ supercell
            Vector3D temp = new Vector3D(pos).subtract(voroDisp);

            // Store it for later
            vertexPos.add(temp);
        }

        // Read in face information
        line = fp.readLine();
        int nFaces = Integer.parseInt(line.split(" ")[1]);

        //   Prepare to store neighbor ID, areas, face normals, and vertex IDs
        List<Integer> faceNeighborID = new ArrayList<>(nFaces);
        List<Double> faceArea = new ArrayList<>(nFaces);
        List<Vector3D> faceNormal = new ArrayList<>(nFaces);
        List<List<Integer>> faceVertices = new ArrayList<>(nFaces);
        double surfaceArea = 0.0;

        //   Parse through each face
        for (int f = 0; f < nFaces; f++) {
            line = fp.readLine();
            words = line.split(" ");

            // Compute number of vertices for this face
            int nFaceVerts = words.length - 5; // ID, area, normal

            // Store the face information
            faceNeighborID.add(Integer.parseInt(words[0]));
            double area = Double.parseDouble(words[1]);
            surfaceArea += area;
            faceArea.add(area);
            double[] normal = new double[3];
            for (int d = 0; d < 3; d++) {
                normal[d] = Double.parseDouble(words[2 + d]);
            }
            faceNormal.add(new Vector3D(normal));

            // Read in the face vertices
            List<Integer> vertices = new ArrayList<>(nFaceVerts);
            for (int v = 0; v < nFaceVerts; v++) {
                int vertID = Integer.parseInt(words[5 + v]);
                vertices.add(vertID);
            }
            faceVertices.add(vertices);
        }

        // Check if any faces failed to form, which can be determined if the norm
        //   of the face is ~0. If so, call the fixer
        for (Vector3D norm : faceNormal) {
            if (norm.getNorm() < 1e-6) {
                eliminateBadFaces(vertexPos, faceVertices, faceNormal);
            }
        }

        // Mark which faces each vertex is on
        List<Set<Integer>> vertexFaces = new ArrayList<>(vertexPos.size());
        for (int i=0; i<vertexPos.size(); i++) {
            vertexFaces.add(new TreeSet<Integer>());
        }
        for (int f=0; f<faceVertices.size(); f++) {
            // Skip broken faces
            if (faceVertices.get(f).size() < 2) continue;

            // Update vertices that are on this face
            for (Integer vert : faceVertices.get(f)) {
                vertexFaces.get(vert).add(f);
            }
        }

        // Figure out the images associated with each face, initialize faces
        List<VoronoiFace> faces = new ArrayList<>(nFaces);
        for (int f=0; f<nFaces; f++) {
            // If this is a bad face add a null
            if (faceVertices.get(f).size() < 3) {
                faces.add(null);
            } else {
                // Find the image
                AtomImage outsideAtom = determineImage(strc, strcAligned, radical,
                        atomID, faceNeighborID.get(f),
                        vertexPos.get(faceVertices.get(f).get(0)),
                        faceNormal.get(f));
                faces.add(new VoronoiFace(atom, outsideAtom, radical));
            }
        }

        // Create vertices
        List<VoronoiVertex> vertexPool = new ArrayList<>(nVerts);
        for (int v=0; v<nVerts; v++) {
            // Assemble list of faces
            Set<VoronoiFace> myFaces = new TreeSet<>();
            for (int f : vertexFaces.get(v)) {
                if (faces.get(f) != null) {
                    myFaces.add(faces.get(f));
                }
            }

            // Convert to original coordinate system
            double[] mappedPos = strc.convertFractionalToCartestian(
                    strcAligned.convertCartesianToFractional(vertexPos.get(v).toArray()));

            // Make the vertex
            if (myFaces.isEmpty()) {
                vertexPool.add(null);
            } else {
                VoronoiVertex vert = new VoronoiVertex(mappedPos, myFaces);
                vertexPool.add(vert);
            }
        }

        // Add vertices to faces
        for (int f=0; f<nFaces; f++) {
            if (faces.get(f) != null) { // Skip bad faces
                // Gather faces
                List<VoronoiVertex> myVerts = new ArrayList<>(faceVertices.get(f).size());
                for (int v : faceVertices.get(f)) {
                    myVerts.add(vertexPool.get(v));
                }

                // Assemble faces
                faces.get(f).assembleFromVertices(myVerts);

                // Test area
                double diff = faces.get(f).getArea() - faceArea.get(f);
                if (Math.abs(diff) > 1e-2 * surfaceArea) {
                    throw new RuntimeException(String.format(
                            "Face area inconsistant with Voro++ for atom %d, face %d. Error: %.2f %%",
                            atomID, f, diff / faceArea.get(f) * 100));
                }
            }
        }

        // Remove bad faces from faces list
        while (faces.remove(null)) {};

        // Create the cell!
        VoronoiCell cell = new VoronoiCell(atom, radical, faces);

        // Check the volume
        double diff = cell.getVolume() - volume;
        if (Math.abs(diff) > 1e-2 * volume) {
            throw new RuntimeException(String.format(
                    "Volume inconsistant with Voro++ for atom %d. Error: %.2f %%",
                    atomID, diff / volume * 100));
        }
        // Output cell
        return new ImmutablePair<>(atomID, cell);
    }

    /**
     * Determine the periodic image of an image associated with a face.
     *
     * @param strc Structure being tessellated.
     * @param strcAligned Structure being tessellated, aligned to the same
     * basis as Voro++ (e.g., a parallel to x axis)
     * @param radical Whether we are performing a radical plane tessellation
     * @param centerID ID of central atom
     * @param neighborID ID of neighboring atom
     * @param point Coordinates of a vertex on this face
     * @param normal Normal of the face
     * @return Atom image on the outside of this face
     */
    protected static AtomImage determineImage(Cell strc, Cell strcAligned,
            boolean radical, int centerID, int neighborID,
            Vector3D point, Vector3D normal) {
        // Get the center and neighboring atom, from the aligned basis
        Atom centerAtom = strcAligned.getAtom(centerID);
        Atom neighborAtom = strcAligned.getAtom(neighborID);

        // Get the plane of this face
        Plane plane;
        try {
            plane = new Plane(point, normal, 1e-6);
        } catch (MathArithmeticException e) {
            throw new RuntimeException(e);
        }

        // Make sure the center atom is on the inside of this plane
        double dir = plane.getOffset(new Vector3D(strcAligned.getAtom(centerID).getPositionCartesian()));
        if (dir > 0) {
            plane = new Plane(plane.getOrigin(), plane.getNormal().negate(), 1e-6);
        }

        // Compute distance between center and this plane
        double toPlane = Math.abs(plane.getOffset(new Vector3D(centerAtom.getPositionCartesian())));

        // Compute the distance between center atom and neighboring atom
        double toNeigh;
        if (radical) {
            double rsCenter = centerAtom.getRadius() * centerAtom.getRadius();
            double rsNeigh = neighborAtom.getRadius() * neighborAtom.getRadius();
            toNeigh = Math.sqrt(toPlane * toPlane - (rsCenter - rsNeigh)) + toPlane;
        } else {
            toNeigh = toPlane * 2;
        }

        // Compute displacement vector between center and neighbor atom
        RealVector imageDisp = new ArrayRealVector(plane.getNormal().toArray()).unitVector().mapMultiply(toNeigh);

        // Compute displacement between center atom and neighbor inside the simulation
        RealVector incellDisp = new ArrayRealVector(neighborAtom.getPositionCartesian())
                .subtract(new ArrayRealVector(centerAtom.getPositionCartesian()));

        // Compute supercell vector
        RealVector supercellVector = imageDisp.subtract(incellDisp);

        // Determine the periodic image of neighbor
        RealVector superCell = new BlockRealMatrix(strcAligned.getInverseBasis())
                .operate(supercellVector);
        double error = superCell.subtract(superCell.map(new Rint())).getL1Norm();
        if (error > 5e-2) {
            throw new RuntimeException("Failed to match image to cell");
        }
        superCell.mapToSelf(new Rint());

        // Make the atom image
        return new AtomImage(strc.getAtom(neighborID), new int[]{
            (int) superCell.getEntry(0),
            (int) superCell.getEntry(1),
            (int) superCell.getEntry(2)
        });
    }

    /**
     * Make sure that the number of atoms is the same
     *
     * @param fp Output from Voro++, starting after reading polydispersity
     * @param strc Structure being tessellated
     * tessellation
     * @throws Exception
     */
    private static void checkAtoms(BufferedReader fp, Cell strc) throws Exception {
        // Check the number of atoms
        String line = fp.readLine();
        String[] words = line.split(" ");
        if (!words[3].equalsIgnoreCase(Integer.toString(strc.nAtoms()))) {
            throw new Exception(String.format("Number of atoms differs. %d != %s",
                    strc.nAtoms(), words[3]));
        }
    }

    /**
     * Check whether Voro++ ran a radical plane Voronoi tessellation if desired.
     *
     * @param fp Output from Voro++, just after reading periodicity
     * @param radical Whether user wanted to run radical plane tessellation
     * @throws Exception
     */
    private static void checkPolydisperse(BufferedReader fp, boolean radical) throws Exception {
        String line = fp.readLine();
        String[] words = line.split(" ");
        if (Boolean.parseBoolean(words[1].toLowerCase()) != radical) {
            throw new Exception("Voro++ ran wrong kind of tessellation");
        }
    }

    /**
     * Make sure that the periodicity of Voro++ output matches input structure
     *
     * @param fp Output from Voro++ right after reading basis
     * @param strc Input structure
     * @throws Exception
     */
    private static void checkPeriodicity(BufferedReader fp, Cell strc) throws Exception {
        String line = fp.readLine();
        String[] words = line.split(" ");
        for (int d = 0; d < 3; d++) {
            if (strc.directionIsPeriodic(d) != words[d + 1].equalsIgnoreCase("true")) {
                throw new Exception("Periodicity in direction #" + d + " incorrect");
            }
        }
    }

    /**
     * Make sure the basis of the Voro++ output matches this structure.
     *
     * @param strc Structure being tessellated
     * @param fp Output from Voro++ output. Starting on 1st line
     * @throws Exception If the basis is different
     */
    protected static void checkBasis(BufferedReader fp, Cell strc) throws Exception {
        // Read in the header containing the basis
        String line = fp.readLine();
        if (line == null || (!line.contains("Basis:"))) {
            throw new Exception("Parsing failed: First line not \"Basis:\"");
        }

        // Get the expected basis
        double[][] basis = strc.getAlignedBasis();

        // Loop through each basis vector point
        for (int dir1 = 0; dir1 < 3; dir1++) {
            line = fp.readLine();
            String[] words = line.split(" ");
            if (words.length != 4) {
                throw new Exception("Expected basis, found: " + line);
            }

            for (int dir2 = 0; dir2 < 3; dir2++) {

                // Compute the difference
                double diff = Math.abs(basis[dir2][dir1] - Double.parseDouble(words[dir2 + 1]));

                // See if it exceeds tolerance
                if (diff > 1e-6) {
                    throw new Exception(String.format("Basis error. Difference in %d,%d = %.1e",
                            dir1, dir2, diff));
                }
            }
        }
    }

    /**
     * Compute the distance between atom atom and its corresponding
     * image in Voro++. This will be used to shift the atom and vertex
     * positions to line up with coordinates in Vassal.
     *
     * <p>Voro++ adjusts atom positions such that they reside within the
     * orthorhombic box defined by 0&le;x&lt;a<sub>x</sub>,
     * 0&le;y&lt;b<sub>y</sub>, and 0&le;z&lt;c<sub>z</sub>, where a, b, and c
     * are the lattice vectors.
     *
     * @param strc Structure being tessellated, adjusted to have the same
     * basis set as Voro++.
     * @param atomID ID of atom at the center of this Voronoi cell. Should
     * @return Difference between center of atom in Vassal and Voro++
     */
    static protected Vector3D getVoroPlusPlusPositionDelta(Cell strc, int atomID) {
        // Get the position of the atom in Vassal
        Atom atom = strc.getAtom(atomID);
        Vector3D vassalPos = new Vector3D(atom.getPositionCartesian());
        Vector3D voroPos = vassalPos.scalarMultiply(1);

        // Get what its position in Voro++ would be
        //   see: container_periodic_base::put_locate_block
        Vector3D[] basis = strc.getLatticeVectors();

        //   Get the width of the simulation cell in each direction
        double dx = basis[0].getX(), dy = basis[1].getY(),
                dz = basis[2].getZ();

        //   Adjust the z coordinate such that it is between 0 and dz
        double step = stepDiv(voroPos.getZ(), dz);
        voroPos = voroPos.subtract(step, basis[2]);

        //   Adjust the y coordinate such that it is between 0 and dy
        step = stepDiv(voroPos.getY(), dy);
        voroPos = voroPos.subtract(step, basis[1]);

        //   Adjust the x coordinate such that it is between 0 and dx
        step = stepDiv(voroPos.getX(), dx);
        voroPos = voroPos.subtract(step, basis[0]);

        // Return the difference between the Voro++ position and atom
        return voroPos.subtract(vassalPos);
    }

    /**
     * Divide two doubles following the scheme used by <a href=
     * "http://math.lbl.gov/voro++/doc/refman/classvoro_1_1voro__base.html#a59a281a5e25e13f681c4f6a2c8bd1ca7"
     * >step_div in Voro++</a>
     * @param x
     * @param y
     * @return x/y
     */
    static protected double stepDiv(double x, double y) {
        return x >= 0 ? Math.floor(x/y) : -1.0 + (int) ((x+1)/y);
    }

    /**
     * Eliminate bad faces by iteratively merging vertices
     *
     * <p>Procedure:
     *
     * <ol>
     * <li>Check if any faces with a small normal (i.e., ones that did not form correctly)
     * has more vertices.
     *  <ol>
     *  <li>If not, return
     *  </ol>
     * <li>Find the closest two vertices on that face
     * <li>Merge those two vertices
     *  <ol>
     *  <li>Randomly select one vertex to keep
     *  <li>If a face has both vertices, keep only the selected one
     *  <li>If a face has only one of the vertices and that vertex is the one
     *   to be eliminated, replace that vertex with the selected one
     *  </ol>
     * <li>Go back to step 1
     * </ol>
     *
     * @param vertexPos Positions of each vertex
     * @param faceVertices Vertices on each face
     * @param faceNormal Normals on each face
     */
    static private void eliminateBadFaces(List<Vector3D> vertexPos,
            List<List<Integer>> faceVertices,
            List<Vector3D> faceNormal) {

        while (true) {
            // Check if any non-complete faces need to be trimmed down
            boolean done = true;
            int badFace = -1;
            for (int f=0; f<faceVertices.size(); f++) {
                if ((faceNormal.get(f).getNorm() < 1e-6) &&
                        (faceVertices.get(f).size() > 2)) {
                    done = false;
                    badFace = f;
                    break;
                }
            }
            if (done) {
                return;
            }

            // Find the closest two vertices on that face
            Integer vertToKeep=-1, vertToRemove=-1;
            double shortestDistance = Double.MAX_VALUE;
            int badFaceSize = faceVertices.get(badFace).size();
            for (int v=0; v<badFaceSize; v++) {
                double dist = vertexPos.get(faceVertices.get(badFace).get(v)).
                        distance(vertexPos.get(faceVertices.get(badFace).get((v+1)%badFaceSize)));
                if (dist < shortestDistance) {
                    shortestDistance = dist;
                    vertToKeep = faceVertices.get(badFace).get(v);
                    vertToRemove = faceVertices.get(badFace).get((v+1) % badFaceSize);
                }
            }

            // Loop over all faces, removing the vertexToRemove
            for (List<Integer> faceVerts : faceVertices) {
                // Check if it has the vertex to be removed
                if (faceVerts.contains(vertToRemove)) {
                    if (faceVerts.contains(vertToKeep)) {
                        // If it also contains the good vertex, just remove this vert
                        faceVerts.remove(vertToRemove);
                    } else {
                        // If it doesn't replace it
                        int myIndex = faceVerts.indexOf(vertToRemove);
                        faceVerts.set(myIndex, vertToKeep);
                    }
                }
            }
        }
    }
}
