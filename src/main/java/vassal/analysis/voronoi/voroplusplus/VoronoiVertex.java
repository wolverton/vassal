package vassal.analysis.voronoi.voroplusplus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.analysis.voronoi.BaseVoronoiVertex;

/**
 * Stores information about a vertex in a Voro++ output
 * @author Logan Ward
 */
public class VoronoiVertex extends BaseVoronoiVertex {
    /** List of faces on which this vertex resides */
    protected final Set<VoronoiFace> Faces = new TreeSet<>();

    /**
     * Initialize a vertex.
     * @param position Position of vertex
     * @param faces Faces on which vertex resides
     */
    public VoronoiVertex(double[] position, Collection<VoronoiFace> faces) {
        // Initialize vertex
        super(faces.iterator().next().getInsideAtom(), new Vector3D(position));

        // Store face information
        Faces.addAll(faces);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof VoronoiVertex) {
            return ((VoronoiVertex) obj).Faces.equals(this.Faces);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Faces.hashCode();
    }

    @Override
    public int compareTo(BaseVoronoiVertex o) {
        VoronoiVertex other = (VoronoiVertex) o;

        // If face sizes are different
        if (other.Faces.size() != Faces.size()) {
            return Integer.compare(other.Faces.size(), Faces.size());
        }

        // Make sorted lists of the faces
        List<VoronoiFace> myFaces = new ArrayList<>(Faces);
        List<VoronoiFace> yourFaces = new ArrayList<>(other.Faces);
        Collections.sort(myFaces);
        Collections.sort(yourFaces);

        // Compare each entry
        for (int i=0; i<myFaces.size(); i++) {
            int comp = myFaces.get(i).compareTo(yourFaces.get(i));
            if (comp != 0) {
                return comp;
            }
        }
        return 0;
    }

    /**
     * Get the list of faces on which this vertex resides
     * @return Set of faces, no particular order
     */
    public Set<VoronoiFace> getFaces() {
        return new TreeSet<>(Faces);
    }
}
