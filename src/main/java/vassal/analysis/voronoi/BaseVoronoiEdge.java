package vassal.analysis.voronoi;

import java.util.Objects;
import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Base class describing a edge of a cell in a Voronoi tessellation
 * @author Logan Ward
 */
public abstract class BaseVoronoiEdge implements Comparable<BaseVoronoiEdge> {

    /**
     * Computes the intersection between two vectors. If they intersect, within
     * the bounds of each edge, set new boundaries for each edge and change the
     * next and previous edge as appropriate
     *
     * @param edge1 An edge
     * @param edge2 A second edge
     * @return Whether the two edges intersect
     */
    public static boolean computeIntersection(BaseVoronoiEdge edge1, BaseVoronoiEdge edge2) {
        // Determine the point at which the edges intersect
        Vector3D point = edge1.Line.intersection(edge2.Line);
        if (point == null) {
            return false;
        }

        // Determine relationship between edges (using their directions)
        boolean isForward = edge1.isCCW(edge2);

        // Using the direction, check whether intersection is within bounds of
        //  each edge
        double edge1Terminus = edge1.Line.getAbscissa(point);
        double edge2Terminus = edge2.Line.getAbscissa(point);
        boolean withinBounds;
        if (isForward) {
            withinBounds = edge1Terminus < edge1.End && edge2Terminus > edge2.Beginning;
        } else {
            withinBounds = edge1Terminus > edge1.Beginning && edge2Terminus < edge2.End;
        }
        if (!withinBounds) {
            return false; // The intersect, but not within the bounds of the edge
        }
        // Now, update the edges accordingly
        if (isForward) {
            edge1.End = edge1Terminus;
            edge1.NextEdge = edge2;
            edge2.Beginning = edge2Terminus;
            edge2.PreviousEdge = edge1;
        } else {
            edge1.Beginning = edge1Terminus;
            edge1.PreviousEdge = edge2;
            edge2.End = edge2Terminus;
            edge2.NextEdge = edge1;
        }
        return true;
    }

    /**
     * Compute whether, given a face normal, edges with two directions are CCW
     * @param normal Face normal
     * @param aDirection Direction of edge A
     * @param bDirection Direction of edge B
     * @return Whether they are CCW
     */
    protected static boolean isCCW(Vector3D normal, Vector3D aDirection, Vector3D bDirection) {
        double ccw = normal.getX() * (aDirection.getY() * bDirection.getZ() - aDirection.getZ() * bDirection.getY()) - normal.getY() * (aDirection.getX() * bDirection.getZ() - aDirection.getZ() * bDirection.getX()) + normal.getZ() * (aDirection.getX() * bDirection.getY() - aDirection.getY() * bDirection.getX());
        return ccw > 0;
    }

    /**
     * Join two edges. Will check orientation of edges to see if edge1 is the
     * edge following edge2 (or vis versa), and join them accordingly.
     *
     * <p>Unlike {@linkplain #computeIntersection(vassal.analysis.voronoi.VoronoiEdge, vassal.analysis.voronoi.VoronoiEdge) }
     * this operation does not check if the edges intersect within their
     * current bounds.
     * @param edge1 First edge
     * @param edge2 Second edge
     * @throws java.lang.Exception
     */
    public static void joinEdges(BaseVoronoiEdge edge1, BaseVoronoiEdge edge2) throws Exception {
        // Determine the point at which the edges intersect
        Vector3D point = edge1.Line.intersection(edge2.Line);
        if (point == null) {
            throw new Exception("Edges do not intersect");
        }
        // Compute the location along each line
        double edge1Terminus = edge1.Line.getAbscissa(point);
        double edge2Terminus = edge2.Line.getAbscissa(point);
        // Determine relationship between edges (using their directions)
        boolean isForward = edge1.isCCW(edge2);
        // Now, update the edges accordingly
        if (isForward) {
            edge1.End = edge1Terminus;
            edge1.NextEdge = edge2;
            edge2.Beginning = edge2Terminus;
            edge2.PreviousEdge = edge1;
        } else {
            edge1.Beginning = edge1Terminus;
            edge1.PreviousEdge = edge2;
            edge2.End = edge2Terminus;
            edge2.NextEdge = edge1;
        }
    }

    /** EdgeFace on which this edge exists */
    protected BaseVoronoiFace EdgeFace;
    /** EdgeFace that intersected {@link #EdgeFace} to form this edge */
    protected BaseVoronoiFace IntersectingFace;
    /** Line representing this edge */
    protected Line Line;
    /** Direction of edge */
    protected Vector3D Direction;
    /** Point marking the beginning of this edge */
    protected double Beginning = Double.NEGATIVE_INFINITY;
    /** Edge after this edge */
    protected BaseVoronoiEdge NextEdge = null;
    /** Point marking the end of this edge */
    protected double End = Double.POSITIVE_INFINITY;
    /** Edge before this edge */
    protected BaseVoronoiEdge PreviousEdge = null;

    protected BaseVoronoiEdge() {
    }

    /**
     * Initialize a edge by defining the face on which the edge exists, and
     * the
     * @param face Plane defining one face containing this edge
     * @param intersectingFace Plane defining the other face defining this edge
     * @throws java.lang.Exception If faces are parallel
     */
    protected BaseVoronoiEdge(BaseVoronoiFace face,
            BaseVoronoiFace intersectingFace) throws Exception {
        // Store basic information
        EdgeFace = face;
        IntersectingFace = intersectingFace;

        // Compute the line
        Line = face.getPlane().intersection(intersectingFace.getPlane());
        if (Line == null) {
            throw new Exception("Planes are parallel");
        }

        // Ensure vector is CCW wrt EdgeFace
        Vector3D cutDirection = IntersectingFace.getNormal().negate();
        Direction = Line.getDirection();
        if (! isCCW(EdgeFace.getNormal(), Direction, cutDirection)) {
            Line = Line.revert();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof BaseVoronoiEdge) {
            BaseVoronoiEdge other = (BaseVoronoiEdge) obj;
            return other.EdgeFace.equals(EdgeFace) && other.IntersectingFace.equals(IntersectingFace);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.EdgeFace);
        hash = 43 * hash + Objects.hashCode(this.IntersectingFace);
        return hash;
    }

    @Override
    public int compareTo(BaseVoronoiEdge o) {
        if (EdgeFace.equals(o.EdgeFace)) {
            return IntersectingFace.compareTo(o.IntersectingFace);
        }
        return EdgeFace.compareTo(o.EdgeFace);
    }

    /**
     * Get the line defining this edge
     * @return Line
     */
    public Line getLine() {
        return Line;
    }

    /**
     * Get the face containing this edge
     * @return Face containing this edge
     */
    public BaseVoronoiFace getEdgeFace() {
        return EdgeFace;
    }

    /**
     * Get the other face associated with this edge
     * @return Intersecting edges
     */
    public BaseVoronoiFace getIntersectingFace() {
        return IntersectingFace;
    }

    /**
     * Get the next edge on this face
     * @return Next edge (null if no such edge)
     */
    public BaseVoronoiEdge getNextEdge() {
        return NextEdge;
    }

    /**
     * Get the next edge on this face
     * @return Next edge (null if no such edge)
     */
    public BaseVoronoiEdge getPreviousEdge() {
        return PreviousEdge;
    }

    /**
     * Get the vertex at the beginning of this vector
     * @return Starting vertex
     * @throws java.lang.Exception
     */
    public abstract BaseVoronoiVertex getStartVertex() throws Exception;

    /**
     * Get the vertex at the end of this vector
     * @return Ending vertex
     * @throws java.lang.Exception
     */
    public abstract BaseVoronoiVertex getEndVertex() throws Exception;

    /**
     * Get the length of this edge
     * @return Length
     */
    public double getLength() {
        return Line.pointAt(End).distance(Line.pointAt(Beginning));
    }

    @Override
    public String toString() {
        return "(" + EdgeFace.toString() + "," + IntersectingFace.toString() + ")";
    }

    /**
     * Compute whether an edge can be the edge after this one on a face.
     * @param edge A second edge
     * @return Whether these edges form a CCW arrangement
     */
    public boolean isCCW(BaseVoronoiEdge edge) {
        Vector3D myDirection = Direction;
        Vector3D yourDirection = edge.Direction;
        Vector3D normal = EdgeFace.getNormal();
        return isCCW(normal, myDirection, yourDirection);
    }

    /**
     * Generate the edge that corresponds to this edge on the intersecting face.
     * @return Newly instantiated edge
     */
    public abstract BaseVoronoiEdge generatePair();

}
