package vassal.analysis.voronoi.output;

import vassal.analysis.voronoi.java.VoronoiCell;
import java.io.*;
import java.util.*;
import vassal.analysis.voronoi.*;

/**
 * Output a VoronoiCell (or several) to disk.
 * @author Logan Ward
 */
abstract public class VoronoiCellWriter {
	/**
	 * Given a list of cells, write the output
	 * @param cells Cells to be outputted
	 * @return List of lines to be written to file
	 */
	abstract protected List<String> renderCells(List<BaseVoronoiCell> cells);

	/**
	 * Given list of cells, write them to disk in a format that can be plotted.
	 * @param cells List of cells
	 * @param filename Filename
     * @throws java.io.IOException
	 */
	public void writeToFile(List<BaseVoronoiCell> cells, String filename) throws IOException {
		// Open file
		PrintWriter out = new PrintWriter(filename);

		// Render cells
		List<String> toWrite = renderCells(cells);

		// Output it
		for (String line : toWrite) {
			out.println(line);
		}

		// Close up shop
		out.close();
	}

    /**
     * Write a single cell to file.
     * @see #writeToFile(java.util.List, java.lang.String)
     * @param cell Cell to write to file
     * @param filename Path to output file
     * @throws IOException
     */
    public void writeToFile(BaseVoronoiCell cell, String filename) throws IOException {
        List<BaseVoronoiCell> cells = new ArrayList<>(1);
        cells.add(cell);
        writeToFile(cells, filename);
    }
}
