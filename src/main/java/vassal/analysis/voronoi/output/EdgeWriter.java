package vassal.analysis.voronoi.output;

import java.text.NumberFormat;
import java.util.*;
import vassal.analysis.voronoi.*;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.math3.geometry.euclidean.threed.*;

/**
 * Writes down file containing each unique edge. First lists cell vectors. Format:
 *
 *  <p>x y z
 * <br>&lt;a<sub>x</sub>&gt; &lt;a<sub>y</sub>&gt; &lt;a<sub>z</sub>&gt;
 * <br>&lt;b<sub>x</sub>&gt; &lt;b<sub>y</sub>&gt; &lt;b<sub>z</sub>&gt;
 * <br>&lt;c<sub>x</sub>&gt; &lt;c<sub>y</sub>&gt; &lt;c<sub>z</sub>&gt;
 * <br>x1 y1 z1 x2 y2 z2
 * <br>&lt;edge #1 start, x&gt; &lt;edge #1 start, y&gt; &lt;...&gt;
 * @author Logan Ward
 */
public class EdgeWriter extends VoronoiCellWriter {

	@Override
	protected List<String> renderCells(List<BaseVoronoiCell> cells) {
		// Get list of faces
		Set<BaseVoronoiFace> faces = new TreeSet<>();
		for (BaseVoronoiCell cell : cells) {
			faces.addAll(cell.getFaces());
		}

		// Get all edges
		Set<Pair<Vector3D,Vector3D>> edges = new HashSet<>();
		for (BaseVoronoiFace face : faces) {
			for (BaseVoronoiEdge edge : face.getEdges()) {
                BaseVoronoiVertex start, end;
                try {
                    start = edge.getStartVertex();
                    end = edge.getEndVertex();
                } catch (Exception e) {
                    continue;
                }

				// Ensure the lower vertex is always first (for set purposes)
				if (start.compareTo(end) < 0) {
					edges.add(new ImmutablePair<>(start.getPosition(),
							end.getPosition()));
				} else {
					edges.add(new ImmutablePair<>(end.getPosition(),
							start.getPosition()));
				}
			}
		}

        // Set formats
		NumberFormat nFormat = NumberFormat.getNumberInstance();
		nFormat.setMaximumFractionDigits(6);
		nFormat.setMaximumFractionDigits(6);
		Vector3DFormat vFormat = new Vector3DFormat("", "", " ", nFormat);

        // Output cell vectors
        List<String> output = new ArrayList<>(edges.size() + 5);
        output.add("x y z");
        for (Vector3D cellVec : cells.get(0).getAtom().getCell().getLatticeVectors()) {
            output.add(vFormat.format(cellVec));
        }

		// Output edges
		output.add("x1 y1 z1 x2 y2 z2");
		for (Pair<Vector3D, Vector3D> edge : edges) {
			// Write line
			Vector3D left = edge.getLeft(), right = edge.getRight();
			output.add(String.format("%s %s", vFormat.format(left),
					vFormat.format(right)));
		}

		return output;
	}

}


