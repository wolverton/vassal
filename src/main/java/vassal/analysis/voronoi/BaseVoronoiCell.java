package vassal.analysis.voronoi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.data.Atom;
import vassal.data.AtomImage;

/**
 * Base class describing a single cell in a Voronoi tessellation.
 *
 * @author Logan Ward
 */
public class BaseVoronoiCell {
    /** Atom at the center of this cell */
    protected final Atom Atom;
    /** List of faces */
    protected final List<BaseVoronoiFace> Faces = new LinkedList<>();
    /** Whether to use radical plane method */
    protected final boolean Radical;
    /** Volume of cell. Cached result */
    protected double Volume = Double.NaN;

    	/**
	 * Initialize a Voronoi cell for a specific atom. Creates a parallelepiped
	 *  around the atom defined by its interactions with the periodic images
	 *  across the cube face of the cell, or the walls of the cell.
	 * @param atom Atom at center of cell
	 * @param radical Whether to add faces using the radical plane Voronoi
	 * tessellation method.
	 * @see VoronoiTessellationCalculator
	 */
	public BaseVoronoiCell(Atom atom, boolean radical) {
		this.Atom = atom;
		this.Radical = radical;
	}

    /**
     * Get atom at center of this cell
     * @return Atom
     */
    public Atom getAtom() {
        return Atom;
    }

    /**
     * Get all faces
     * @return Set of faces
     */
    public Set<BaseVoronoiFace> getFaces() {
        return new TreeSet<>(Faces);
    }

    /**
     * @return Number of faces
     */
    public int nFaces() {
        return Faces.size();
    }

    /**
     * Get all vertices in this cell
     * @return Set of a vertices
     */
    public Set<BaseVoronoiVertex> getVertices() {
        Set<BaseVoronoiVertex> out = new HashSet<>();
        for (BaseVoronoiFace face : Faces) {
            out.addAll(face.getVertices());
        }
        return out;
    }

    /**
     * Get the number of neighbors of each type
     * @return Map: [atom type] : [number of neighbors]
     */
    public Map<Integer, Integer> getNeighborTypes() {
        Map<Integer, Integer> output = new TreeMap<>();
        for (BaseVoronoiFace face : Faces) {
            int atomID = face.getOutsideAtom().getAtomID();
            int atomType = Atom.getCell().getAtom(atomID).getType();
            if (output.containsKey(atomType)) {
                output.put(atomType, output.get(atomType) + 1);
            } else {
                output.put(atomType, 1);
            }
        }
        return output;
    }

    /**
     * Get list of neighboring atoms. Identifies atoms by both their ID and image location
     * @return List of atom images
     */
    public List<AtomImage> getNeighbors() {
        List<AtomImage> output = new ArrayList<>(Faces.size());
        for (BaseVoronoiFace face : Faces) {
            output.add(face.getOutsideAtom());
        }
        return output;
    }

    /**
     * Get distances between central atom and each neighbor.
     * @return List of distances in cartesian units
     */
    public double[] getNeighborDistances() {
        double[] output = new double[nFaces()];
        for (int f = 0; f < nFaces(); f++) {
            output[f] = Faces.get(f).getNeighborDistance();
        }
        return output;
    }

    /**
     * Get faces on the outside of a polyhedron formed by the Voronoi
     * cells and its neighbors.
     *
     * <p>Note: The coordinates of the center of each face may not be correct.
     *
     * @param cells Voronoi cells of all other atoms (arranged by Atom ID)
     * @param index Index of largest neighbor shell to be included
     * (0 == faces of atom, 1 == faces of atom + 1st nearest neighbor shell, etc.)
     * @return List of faces on the outside
     */
    public List<BaseVoronoiFace> getExtendedFaces(List<BaseVoronoiCell> cells, int index) {
        // Special case: Index == 0
        if (index == 0) {
            return new ArrayList<>(Faces);
        }
        // Get atoms to consider
        Set<AtomImage> allImages = new TreeSet<>();
        Set<AtomImage> outsideImages;
        //   Get neighbor list
        List<Set<AtomImage>> neighborList = getNeighborShells(cells, index);
        //   Last shell are atoms on the outside
        outsideImages = neighborList.get(index);
        //   Get all atoms
        for (Set<AtomImage> images : neighborList) {
            allImages.addAll(images);
        }
        // Get the faces of all outside atoms that do not correspond to an atom
        //  on the inside of the polyhedron
        List<BaseVoronoiFace> output = new ArrayList<>();
        for (AtomImage image : outsideImages) {
            // Get coordinates of this image wrt central atom
            int[] imageCoord = image.getSupercell();
            // Get its Voronoi cell
            BaseVoronoiCell cell = cells.get(image.getAtomID());
            // Iterate over faces
            for (BaseVoronoiFace face : cell.getFaces()) {
                // Get the image on the outside of this face
                AtomImage relativeImage = face.getOutsideAtom();
                // Get the coordinates of this image relative to the central atom
                int[] outsideCoord = relativeImage.getSupercell();
                for (int i = 0; i < 3; i++) {
                    outsideCoord[i] += imageCoord[i];
                }
                // Get the actual image
                AtomImage actualImage = new AtomImage(relativeImage.getAtom(), outsideCoord);
                // If this image is not inside of the polyhedron
                if (!allImages.contains(actualImage)) {
                    output.add(face);
                }
            }
        }
        return output;
    }

    /**
     * Get list of atoms in all coordination cells.
     * @param cells Voronoi cells of all other atoms (arranged by atom ID)
     * @param index Index of largest neighbor shell to be considered
     * @return All neighbors in that
     */
    public List<Set<AtomImage>> getNeighborShells(List<BaseVoronoiCell> cells, int index) {
        if (index < 0) {
            // Special case: Bad input
            throw new Error("Shell index must be >= 0.");
        } else if (index == 0) {
            // Special case: 0th cell (this atom)
            Set<AtomImage> innerShell = new TreeSet<>();
            innerShell.add(new AtomImage(Atom, new int[3])); // This atom
            // Prep output
            List<Set<AtomImage>> output = new LinkedList<>();
            output.add(innerShell);
            return output;
        } else {
            // General case: i-th shell
            // Get the previous shell
            List<Set<AtomImage>> previousShells = getNeighborShells(cells, index - 1);
            // For each atom in the previous shell, get its neighbors
            Set<AtomImage> newShell = new TreeSet<>();
            for (AtomImage atom : previousShells.get(index - 1)) {
                // Get its NN, according the the diagram
                List<AtomImage> neighbors = cells.get(atom.getAtomID()).getNeighbors();
                // For each of these neighbors, adjust the image coordinates
                int[] thisImage = atom.getSupercell();
                for (AtomImage neighbor : neighbors) {
                    // Get the coordinates relative to the central atom
                    int[] oldImage = neighbor.getSupercell();
                    for (int i = 0; i < 3; i++) {
                        oldImage[i] += thisImage[i];
                    }
                    // Create a new image, append it to output
                    newShell.add(new AtomImage(neighbor.getAtom(), oldImage));
                }
            }
            // Remove all images corresponding to the shell inside this one
            for (Set<AtomImage> shell : previousShells) {
                newShell.removeAll(shell);
            }
            // Append new shell to output
            previousShells.add(newShell);
            return previousShells;
        }
    }

    /**
     * Get the neighbors in a certain shell, along with weights defined by face sizes.
     *
     * <p>The neighbor shell of an atom is typically defined as all atoms that
     * can be reach ed in no fewer than a certain number of steps along the
     * a network defined by faces of a Voronoi tessellation. While this works
     * in theory, it is complicated by the fact that Voronoi tessellations
     * computed numerically often contain very small faces due to numerical problems.
     * These atoms with very small faces would be conventionally defined as 2nd
     * nearest neighbors but, due to the small faces, would be counted as as
     * 1st nearest neighbors.
     *
     * <p>To combat this problem, we weighing each neighbor based on the face
     * sizes. For a first neighbor shell, the weight of an atom is associated
     * with its the fraction of the surface area of the cell related to the
     * area of the face corresponding to it. To extend this idea to shells
     * past the 1st, we compute the probability of a walker originating from
     * this cell ends up at a certain atom after N, non-backtracking walks where
     * the probability that it will take a certain step is related to the fraction
     * of the face size area.
     *
     * <p>For example, let's assume this cell is part of a simple-cubic crystal,
     * where each cell has 6, equally-sized faces. The probability that it ends up
     * at any first shell neighbor is 1/6. After this step, there are 5 possible
     * steps (no backtracking). So, each walk has an equal probability of 1/30.
     * There are six atoms that are two steps in the same direction (ex: 2,0,0),
     * which can be reached by only 1 path and therefore have 1/30 weight. Additionally,
     * there are 12 atoms that can be reached by 2 different sets of two orthogonal
     * steps (ex: 1,1,0) that, therefore, have a weight of 2/30.
     *
     *
     * @param cells Voronoi cells of all other atoms (arranged by atom ID)
     * @param shell Which neighbor shell to evaluate
     * @return List of atoms in that shell, and their path weights. Weights
     * will add up to 1
     */
    public Map<AtomImage,Double> getNeighborsByWalks(List<BaseVoronoiCell> cells,
            int shell) {
        // Gather all possible paths

        //   Initialize with first step
        List<Pair<List<AtomImage>,Double>> paths = new ArrayList<>();

        //   Add in starting point
        paths.add(new ImmutablePair<>(
                Arrays.asList(new AtomImage[]{new AtomImage(Atom, new int[]{0,0,0})}),
                1.0));

        // Increment until desired shell
        for (int step=0; step<shell; step++) {
            // Get the new list
            List<Pair<List<AtomImage>,Double>> newPaths = new ArrayList<>(paths.size() * 12);

            // For each current path
            for (Pair<List<AtomImage>,Double> path : paths) {
                // Get last atom in current path
                AtomImage lastStep = path.getKey().get(path.getKey().size()-1);

                // Get each possible step
                Map<AtomImage,Double> newSteps = new TreeMap<>();
                double surfaceArea = 0.0; // Surface area of possible steps
                for (BaseVoronoiFace face : cells.get(lastStep.getAtomID()).getFaces()) {
                    // Adjust image based on position of last atom
                    int[] newSupercell = face.getOutsideAtom().getSupercell();
                    int[] lastSupercell = lastStep.getSupercell();
                    for (int d=0; d<3; d++) {
                        newSupercell[d] += lastSupercell[d];
                    }

                    // Store the next step
                    AtomImage nextStep = new AtomImage(face.getOutsideAtom().getAtom(), newSupercell);
                    surfaceArea += face.getArea();
                    newSteps.put(nextStep, face.getArea());
                }

                // Eliminate backtracking steps
                for (AtomImage previousStep : path.getKey()) {
                    Double faceArea = newSteps.remove(previousStep);
                    if (faceArea != null) {
                        surfaceArea -= faceArea;
                    }
                }

                // Create new paths, making sure to update the weights
                for (Map.Entry<AtomImage,Double> newStep : newSteps.entrySet()) {
                    // Increment path
                    List<AtomImage> newPath = new ArrayList<>(path.getKey());
                    newPath.add(newStep.getKey());

                    // Increment weight
                    double newWeight = path.getRight() * newStep.getValue() / surfaceArea;

                    // Add it to newpaths
                    newPaths.add(new ImmutablePair<>(newPath,newWeight));
                }
            }

            // Update paths
            paths = newPaths;
        }

        // Now that all the paths are gathered, output only the last step and
        //  weights of all paths that lead to that step
        Map<AtomImage,Double> output = new TreeMap<>();
        for (Pair<List<AtomImage>,Double> path : paths) {
            // Get the last step
            AtomImage atom = path.getKey().get(path.getKey().size() - 1 );

            // Update map
            if (output.containsKey(atom)) {
                double curWeight = output.get(atom);
                output.put(atom, curWeight + path.getRight());
            } else {
                output.put(atom, path.getRight());
            }
        }

        return output;
    }

    /**
     * Get list of atoms in a certain coordination shell. A neighbor shell includes
     * all atoms that are a certain number of neighbors away that are not in any
     * smaller neighbor shell (e.g. 2nd shell neighbors cannot also be 1st
     * nearest neighbors)
     * @param cells Voronoi cells of all other atoms (arranged by atom ID)
     * @param index Index of neighbor shell
     * @return All neighbors in that
     */
    public Set<AtomImage> getNeighborShell(List<BaseVoronoiCell> cells, int index) {
        return getNeighborShells(cells, index).get(index);
    }

    /**
     * Get the shape of the coordination polyhedron around this atom. This is
     * determined by counting the number of "bonds" between first neighbor
     * shell atoms. Here, atoms are "bonded" if their Voronoi cells share a face.
     * This is similar to {@linkplain #getPolyhedronShape() }. See section
     * H of the paper listed below for more details.
     *
     * <p>Citation: <a href="http://journals.aps.org/prb/abstract/10.1103/PhysRevB.88.134205">
     * Ward <i>et al</i>. PRB (2013)</a>
     * @param cells Voronoi cells of all other atoms (arranged by atom ID)
     * @return Map describing shape: [number of bonds] -&gt;
     *  [Number of first shell neighbros with that many bonds]
     */
    public Map<Integer, Integer> getCoordinationShellShape(List<BaseVoronoiCell> cells) {
        // Get IDs of every neighbor
        List<AtomImage> neighbors = new ArrayList<>(nFaces());
        for (BaseVoronoiFace face : Faces) {
            neighbors.add(face.getOutsideAtom());
        }
        // Get number of mutual neighbor for each neighbor
        Map<Integer, Integer> output = new TreeMap<>();
        for (AtomImage neighbor : neighbors) {
            int nShared = getNumberSharedBonds(cells.get(neighbor.getAtomID()), neighbor.getSupercell(), neighbors);
            if (output.containsKey(nShared)) {
                output.put(nShared, output.get(nShared) + 1);
            } else {
                output.put(nShared, 1);
            }
        }
        return output;
    }

    /**
     * Compute number of shared bonds. Atoms are defined as bonded if they share
     * a face between their Voronoi cells.
     *
     * <p>This code determines how many common entries occur between a Voronoi
     * cell and a list of neighbor IDs from this cell. Note that in order for
     * bonds to be shared, they must connect to the same image of a certain atom.
     * That is where the direction parameter comes into play.
     *
     * @param cell Voronoi cell of neighboring atom
     * @param direction Difference between image of neighboring atom and this cell
     * @param neighbors IDs and Image Positions of all neighboring atoms to this cell
     * @return Number of shared neighbors between this cell and the neighboring
     * atom
     */
    protected int getNumberSharedBonds(BaseVoronoiCell cell, int[] direction, List<AtomImage> neighbors) {
        int nShared = 0;
        for (BaseVoronoiFace face : cell.getFaces()) {
            int otherCellNeighID = face.getOutsideAtom().getAtomID();
            int[] otherCellNeighImage = face.getOutsideAtom().getSupercell();
            for (int i = 0; i < 3; i++) {
                otherCellNeighImage[i] += direction[i];
            }
            // Make that image
            AtomImage otherCellNeigh = new AtomImage(Atom.getCell().getAtom(otherCellNeighID), otherCellNeighImage);
            // Check against every neighbor of this cell
            for (AtomImage neighbor : neighbors) {
                if (otherCellNeigh.equals(neighbor)) {
                    nShared++;
                }
            }
        }
        return nShared;
    }

    /**
     * Compute the polyhedron shape index. This shape is defined by the
     * "Voronoi Index", which is defined as the number of faces with a certain
     * number of sides. This is often expressed as (n<sub>3</sub>, n<sub>4</sub>,
     * n<sub>5</sub>, n<sub>6</sub>), where n<sub>x</sub> is the number of faces
     * with x number of sides.
     *
     * <p>Citation: <a herf="http://rspa.royalsocietypublishing.org/cgi/doi/10.1098/rspa.1970.0190">
     * Finney (1970)</a>
     * @return Map: [number of edges] -> [number of faces with that edge count]
     */
    public Map<Integer, Integer> getPolyhedronShape() {
        Map<Integer, Integer> output = new TreeMap<>();
        for (BaseVoronoiFace face : Faces) {
            int nVert = face.nVertices();
            if (output.containsKey(nVert)) {
                output.put(nVert, output.get(nVert) + 1);
            } else {
                output.put(nVert, 1);
            }
        }
        return output;
    }

    /**
     * Get volume of this cell
     * @return Volume
     */
    public double getVolume() {
        if (Double.isNaN(Volume)) {
            Volume = 0;
            Vector3D atomCenter = new Vector3D(Atom.getPositionCartesian());
            for (BaseVoronoiFace face : Faces) {
                double area = face.getArea();
                Vector3D fromCenter = face.getCentroid().subtract(atomCenter);
                double h = fromCenter.dotProduct(face.getNormal().normalize());
                Volume += area * h / 3; // Face normal is away from center
            }
        }
        return Volume;
    }

    /**
     * Get surface area of cell
     * @return Area
     */
    public double getSurfaceArea() {
        double area = 0;
        for (BaseVoronoiFace face : Faces) {
            area += face.getArea();
        }
        return area;
    }

    /**
     * Get closest distance between any two vertices
     * @return Distance
     */
    public double getMinimumVertexDistance() {
        List<BaseVoronoiVertex> vertices = new ArrayList<>(getVertices());
        double minDist = Double.MAX_VALUE;
        for (int A = 0; A < vertices.size(); A++) {
            for (int B = A + 1; B < vertices.size(); B++) {
                double dist = vertices.get(A).distanceFrom(vertices.get(B));
                minDist = Math.min(dist, minDist);
            }
        }
        return minDist;
    }

    /**
     * Get largest distance between any two vertices
     * @return Distance
     */
    public double getMaximumVertexDistance() {
        List<BaseVoronoiVertex> vertices = new ArrayList<>(getVertices());
        double maxDist = 0;
        for (int A = 0; A < vertices.size(); A++) {
            for (int B = A + 1; B < vertices.size(); B++) {
                double dist = vertices.get(A).distanceFrom(vertices.get(B));
                maxDist = Math.max(dist, maxDist);
            }
        }
        return maxDist;
    }

    /**
     * Determine whether the geometry of this structure is sound.
     * @return Answer
     */
    public boolean geometryIsValid() {
        for (BaseVoronoiFace face : Faces) {
            if (!face.isClosed()) {
                return false;
            }
            if (!Faces.containsAll(face.getNeighboringFaces())) {
                return false;
            }
        }
        return true;
    }

}
