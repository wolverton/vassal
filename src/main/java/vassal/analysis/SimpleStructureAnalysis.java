package vassal.analysis;

import vassal.data.Cell;

/**
 * Computes simple measures about a simulation cell.
 *
 * <p>Currently-implemented measurements:
 *
 * <ol>
 * <li>Volume per atom
 * </ol>
 *
 * @author Logan Ward
 */
public class SimpleStructureAnalysis extends BaseAnalysis {

    @Override
    protected void precompute() {
        // Nothing to do
    }

    /**
     * Compute volume per atom
     * @return Volume per atom
     */
    public double volumePerAtom() {
        return Structure.volume() / (double) Structure.nAtoms();
    }
}
