package vassal.analysis;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.util.VectorCombinationComputer;

import java.util.ArrayList;
import java.util.List;

/**
 * Compute the distances between each pairs of atom. Determines the distance between
 * each pair of atoms (and any periodic image within a certain distance).
 *
 * <p>Current operations
 * <ul>
 * </ul>
 * @author Logan Ward
 */
public class PairDistanceAnalysis extends BaseAnalysis {
    /** Cutoff distance. No pairs farther than this value are considered */
    private double CutoffDistance = 6.5;
    /** Periodic images to consider when searching for neighbors */
    private List<int[]> Supercells = null;
    /** Lattice vectors corresponding to each image */
    private List<Vector3D> LatticeVectors = null;

    @Override
    protected void precompute() {
        Vector3D[] latVectors = Structure.getLatticeVectors();
        // Goal: Compute all combinations of lattice vectors that are less than
        //       the cutoff length plus the length of the longest vector

        // Find the largest distance between an atom and one of the vertices
        //  of the unit cell. Computed using the Voronoi tessellation. This point
        //  is the vertex corresponding to the faces of (origin, {a & b & c})
        double maxImageDist = Plane.intersection(
                new Plane(latVectors[0].scalarMultiply(0.5), latVectors[0], 1e-6),
                new Plane(latVectors[1].scalarMultiply(0.5), latVectors[1], 1e-6),
                new Plane(latVectors[2].scalarMultiply(0.5), latVectors[2], 1e-6)).getNorm();

        // In order to create a list of vectors such the set of displacement vectors
        //  from any atom in the structure to every image of another atom is
        //  within the cutoff radius is a subset of the set of displacement vectors
        //  created by adding the displacement vector of the closest image of the
        //  second atom from the first atom to each atom in the list of vectors,
        //  we need to find all vectors whose length is shorter than maxDist + CutoffDistance
        //
        // Rationale:
        //   1. The maximum distance between an atom and the closet neighbor
        //      is no more than maxDist (defined above). Otherwise, the image
        //      is not the closest image.
        //   2. If we are interested in finding all images of that second atom
        //      that are within the cutoff distance of the first atom, the
        //      distance between any of those images and the closest image
        //      of the second atom can't be greater than the cutoff distance
        //      plus the distance between that closest image and the first atom
        //   3. Those images of the second atoms are located at integer multiples
        //      of the lattice vectors. So, if we find all lattice vectors shorter
        //      than CutoffDistance + longest possible distance between an atom
        //      and the closest image of a second atom, we can find all images!
        double maxVectorDistance = CutoffDistance + maxImageDist;

        // Compute those vectors
        VectorCombinationComputer computer =
                new VectorCombinationComputer(latVectors, maxVectorDistance);
        Supercells = computer.getSupercellCoordinates();
        LatticeVectors = computer.getVectors();
    }

    /**
     * Get the cutoff distance
     *
     * @return Current cutoff distance
     */
    public double getCutoffDistance() {
        return CutoffDistance;
    }

    /**
     * Define the cutoff distance.
     * @param CutoffDistance Desired distance
     * @see #CutoffDistance
     */
    public void setCutoffDistance(double CutoffDistance) {
        this.CutoffDistance = CutoffDistance;
        if (Structure != null) {
            precompute();
        }
    }

    /**
     * Compute all neighbors of a certain atom
     * @param index Index of atom being considered
     * @return List of pairs: [Neighboring {@linkplain Atom}, Distance]
     */
    public List<Pair<AtomImage,Double>> getAllNeighborsOfAtom(int index) {
        List<Pair<AtomImage, Double>> output = new ArrayList<>();

        // Loop over all atoms
        for (Atom atom : Structure.getAtoms()) {
            output.addAll(findAllImages(index, atom.getID()));
        }

        return output;
    }

    /**
     * Compute the pair radial distribution function.
     *
     * @param nBin Number of bins in which to discretize PRDF (must be > 0)
     * @return Pair radial distribution function between each type. Bin i,j,k
     * is the density of bonds between types i and j that are between k *
     * cutoff / nBin and (k + 1) * cutoff / nBin.
     */
    public double[][][] computePRDF(int nBin) {
        if (nBin <= 0) {
            throw new Error("Number of bins must be > 0.");
        }

        // Initialize arrays
        double[][][] output = new double[Structure.nTypes()][Structure.nTypes()][nBin];

        // Find all pairs within the cutoff distance
        double[] nType = new double[Structure.nTypes()];
        for (int i=0; i<Structure.nAtoms(); i++) {
            for (int j=i; j<Structure.nAtoms(); j++) {
                // Find images
                List<Pair<AtomImage,Double>> images = findAllImages(i,j);
                int iType = Structure.getAtom(i).getType();
                nType[iType]++;
                int jType = Structure.getAtom(j).getType();

                // For each image, assign it to bin
                for (Pair<AtomImage,Double> img : images) {
                    int bin = (int) Math.floor(img.getValue() * nBin / CutoffDistance);
                    if (bin >= nBin) { // Happens if dist equals cutoff
                        continue;
                    }

                    output[iType][jType][bin]++;
                    output[jType][iType][bin]++;
                }
            }
        }

        // Normalize data
        double binSpacing = CutoffDistance / nBin;
        for (int bin=0; bin<nBin; bin++) {
            double vol = 4.0 / 3.0 * (Math.pow(bin + 1, 3) - Math.pow(bin, 3))
                    * Math.pow(binSpacing, 3) * Math.PI;
            for (int i=0; i<output.length; i++) {
                for (int j=0; j<output.length; j++) {
                    output[i][j][bin] /= vol * nType[i];
                }
            }
        }

        return output;
    }

    /**
     * Find all images of one atom that are within the cutoff distance of another atom.
     * @param centerAtom Atom at the center
     * @param neighborAtom Neighboring atom
     * @return All images of nieghborAtom that are within a cutoff distance of
     * centerAtom, and their respective distances
     */
    public List<Pair<AtomImage,Double>> findAllImages(int centerAtom, int neighborAtom) {
        // Get the two atoms
        double[] centerPos = Structure.getAtom(centerAtom).getPositionCartesian();

        // Find all images
        List<Pair<AtomImage, Double>> output = new ArrayList<>();
        AtomImage closestImage = Structure.getClosestImage(centerAtom, neighborAtom);

        double[] BsubA = closestImage.getPosition().toArray();
        for (int i=0; i<3; i++) {
            BsubA[i] -= centerPos[i];
        }
        int[] closestSuperCell = closestImage.getSupercell();

        // Loop over each periodic image to find those within within range
        double cutoffDistanceSq = CutoffDistance * CutoffDistance;
        for (int img = 0; img < Supercells.size(); img++) {
            Vector3D supercellVec = LatticeVectors.get(img);
            double[] newPos = BsubA.clone();
            newPos[0] += supercellVec.getX();
            newPos[1] += supercellVec.getY();
            newPos[2] += supercellVec.getZ();
            double dist = newPos[0] * newPos[0]
                    + newPos[1] * newPos[1]
                    + newPos[2] * newPos[2];
            if (dist < cutoffDistanceSq && dist > 1e-8) {
                int[] ss = Supercells.get(img).clone();
                for (int i = 0; i < 3; i++) {
                    ss[i] += closestSuperCell[i];
                }
                output.add(new ImmutablePair<>(new AtomImage(closestImage.getAtom(), ss), Math.sqrt(dist)));
            }
        }

        return output;
    }
}
