package vassal.analysis;

import vassal.data.Cell;

import java.io.Serializable;

/**
 * Base class for analyzing a structure.
 *
 * <h2>How to Use a Base Analysis Class</h2>
 *
 * <p>First, call the {@linkplain #analyzeStructure(vassal.data.Cell)} operation to
 * perform the analysis of the structure. Once this operation completes, individual measures
 * can be retrieved with operations specific to the implementing class.</p>
 * @author Logan Ward
 */
abstract public class BaseAnalysis implements Serializable {
    /** Link to structure being evaluated */
    protected Cell Structure;

    /**
     * Analyze a specific structure. Once this completes, it is possible to retrieve
     * results out of this object.
     * @param strc Structure to be analyzed.
     * @throws Exception If computation fails
     */
    public void analyzeStructure(Cell strc) throws Exception {
        Structure = strc;
        precompute();
    }

    /**
     * Perform any kind of computations that should only be performed once.
     * @throws java.lang.Exception If computation fails
     */
    abstract protected void precompute() throws Exception ;

    /**
     * Recalculate structural information.
     * @throws Exception If computation fails
     */
    public void recompute() throws Exception {
        precompute();
    }
}
