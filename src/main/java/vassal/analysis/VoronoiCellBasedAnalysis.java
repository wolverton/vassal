package vassal.analysis;

import java.util.*;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.linear.RealMatrix;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.analysis.voronoi.*;
import org.apache.commons.math3.stat.StatUtils;
import vassal.data.Cell;

/**
 * Structure analysis based on the Voronoi tessellation method.
 *
 * <p>
 * <b>Some Papers You Might Want To Read</b>
 *
 * <p>
 * TBD: Logan Ward's fault there is nothing here.
 *
 * @author Logan Ward
 * @see VoronoiTessellationCalculator#compute(jstrc.data.Cell, boolean)
 */
public class VoronoiCellBasedAnalysis extends BaseAnalysis {

    /**
     * Voronoi cells for each atom in structure
     */
    private List<BaseVoronoiCell> Cells = null;
    /**
     * Whether to use a radical Voronoi tessellation
     */
    final private boolean Radical;

    /**
     * Initialize a Voronoi cell analyzer.
     *
     * @param radical Whether to use the radical-plane Voronoi method
     * @see VoronoiTessellationCalculator
     */
    public VoronoiCellBasedAnalysis(boolean radical) {
        this.Radical = radical;
    }

    /**
     * Create a new instance of the analysis toolkit based on a structure that
     * has an identical tessellation. This occurs when the two structures have
     * identical lattice parameters and atomic positions, but different
     * identities of elements on those sites.
     *
     * @param oldTessellation Tessellation of original structure
     * @param newStructure New structure
     * @throws java.lang.Exception If structures are too different
     */
    public VoronoiCellBasedAnalysis(VoronoiCellBasedAnalysis oldTessellation,
            Cell newStructure) throws Exception {
        // Check whether the structures are similar
        Cell oldStructure = oldTessellation.Structure;
        if (oldStructure.nAtoms() != newStructure.nAtoms()) {
            throw new Exception("Structures have different numbers of atoms");
        }
        RealMatrix oldBasis = oldStructure.getBasisMatrix(),
                newBasis = newStructure.getBasisMatrix();
        if (oldBasis.subtract(newBasis).getFrobeniusNorm() > 1e-6) {
            throw new Exception("Basis vectors of new structure are different");
        }
        for (int a = 0; a < oldStructure.nAtoms(); a++) {
            double[] oldAtomPos = oldStructure.getAtom(a).getPosition(),
                    newAtomPos = newStructure.getAtom(a).getPosition();
            for (int d = 0; d < 3; d++) {
                if (Math.abs(oldAtomPos[d] - newAtomPos[d]) > 1e-6) {
                    throw new Exception("Atom #" + a + " position is different");
                }
            }
        }

        // Create new instance
        Cells = oldTessellation.Cells;
        Structure = newStructure;
        Radical = oldTessellation.Radical;
    }

    @Override
    protected void precompute() throws Exception {
        Cells = null;
        Cells = VoronoiTessellationCalculator.compute(Structure, Radical);
        for (BaseVoronoiCell cell : Cells) {
            if (!cell.geometryIsValid()) {
                Cells = null;
                throw new Exception("Invalid geometry");
            }
        }
    }

    /**
     * @return Whether the tessellation of this structure was successful
     */
    public boolean tessellationIsConverged() {
        return Cells != null;
    }

    /**
     * Get the effective coordination number. Defined as:
     * <center><i>N<sub>eff</sub></i> = 1 / sum[(<i>f<sub>i</sub></i>
     * / <i>SA<sub>i</sub></i>)<sup>2</sup>]</center>
     *
     * where <i>f<sub>i</sub></i> is the area of face <i>i</i>.
     *
     * @return Effective coordination number for each atom
     */
    public double[] getEffectiveCoordinationNumbers() {
        double[] output = new double[Structure.nAtoms()];

        // Compute the coordination number
        int pos = 0;
        for (BaseVoronoiCell cell : Cells) {
            // Get surface area
            double sa = cell.getSurfaceArea();

            // Sum over all faces
            double sum = 0;
            double temp;
            for (BaseVoronoiFace face : cell.getFaces()) {
                temp = face.getArea() / sa;
                sum += temp * temp;
            }

            // Store N_eff and increment
            output[pos++] = 1.0 / sum;
        }

        return output;
    }

    /**
     * Get the average number of faces on all cells
     *
     * @return Average number
     */
    public double faceCountAverage() {
        long tally = 0;
        for (BaseVoronoiCell cell : Cells) {
            tally += cell.nFaces();
        }
        return tally / Cells.size();
    }

    /**
     * Get the variance in face count
     *
     * @return Mean absolute deviation in coordination number
     */
    public double faceCountVariance() {
        double average = faceCountAverage();
        double tally = 0;
        for (BaseVoronoiCell cell : Cells) {
            tally += Math.abs(cell.nFaces() - average);
        }
        return tally / Cells.size();
    }

    /**
     * Get the minimum face count of all cells
     *
     * @return Minimum
     */
    public double faceCountMinimum() {
        double output = Double.MAX_VALUE;
        for (BaseVoronoiCell cell : Cells) {
            if (cell.nFaces() < output) {
                output = cell.nFaces();
            }
        }
        return output;
    }

    /**
     * Get the maximum face count of all cells
     *
     * @return Maximum
     */
    public double faceCountMaximum() {
        double output = Double.MIN_VALUE;
        for (BaseVoronoiCell cell : Cells) {
            if (cell.nFaces() > output) {
                output = cell.nFaces();
            }
        }
        return output;
    }

    /**
     * Get a list of all unique polyhedron shapes.
     *
     * @return Set of polyhedron shapes
     * @see BaseVoronoiCell#getNeighborTypes()
     */
    public Set<Map<Integer, Integer>> getUniquePolyhedronShapes() {
        Set<Map<Integer, Integer>> output = new HashSet<>();
        for (BaseVoronoiCell cell : Cells) {
            output.add(cell.getPolyhedronShape());
        }
        return output;
    }

    /**
     * Computes the mean absolute deviation in the volume of each cell.
     *
     * @return Variance of cell volume fraction of all cells
     */
    public double volumeVariance() {
        double[] volumeDeviation = new double[Structure.nAtoms()];
        double meanVolume = Structure.volume() / Structure.nAtoms();

        // Compute the deviation of each cell from the mean volume
        for (int i = 0; i < Structure.nAtoms(); i++) {
            volumeDeviation[i] = Math.abs(Cells.get(i).getVolume() - meanVolume);
        }

        // Return the mean
        return StatUtils.mean(volumeDeviation);
    }

    /**
     * Computes the fraction of cell volume occupied by the smallest Voronoi
     * cell.
     *
     * @return Volume fraction of smallest cell
     */
    public double volumeFractionMinimum() {
        double minVol = Double.MAX_VALUE;
        for (BaseVoronoiCell cell : Cells) {
            double vol = cell.getVolume();
            if (vol < minVol) {
                minVol = vol;
            }
        }
        return minVol / Structure.volume();
    }

    /**
     * Computes the fraction of cell volume occupied by the largest Voronoi
     * cell.
     *
     * @return Volume fraction of largest cell
     */
    public double volumeFractionMaximum() {
        double maxVol = 0;
        for (BaseVoronoiCell cell : Cells) {
            double vol = cell.getVolume();
            if (vol > maxVol) {
                maxVol = vol;
            }
        }
        return maxVol / Structure.volume();
    }

    /**
     * Compute maximum packing efficiency assuming atoms are hard spheres.
     *
     * <p>
     * Algorithm:
     * <ol>
     * <li>For each cell in the Voronoi tessellation of this cell, determine the
     * minimum distance from the cell center to a face. This marks the maximum
     * atomic radius
     * <li>Compute the total volume represented by those maximally-sized atoms
     * <li>Packing efficiency is atom volume divided by cell volume.
     * </ol>
     *
     * @return Maximum packing efficiency
     */
    public double maxPackingEfficiency() {
        double atomVol = 0;
        for (BaseVoronoiCell cell : Cells) {
            // Get minimum distance
            double minDistance = Double.MAX_VALUE;
            for (BaseVoronoiFace face : cell.getFaces()) {
                double fDist = face.getFaceDistance();
                if (fDist < minDistance) {
                    minDistance = fDist;
                }
            }

            // Atom atomic volume
            atomVol += 4.0 / 3.0 * Math.PI * Math.pow(minDistance, 3);
        }
        return atomVol / Structure.volume();
    }

    /**
     * Compute the Warren-Cowley short range ordering parameters for each atom.
     *
     * <p>
     * &alpha;<sub>s,i</sub> = 1 - n<sub><i>A,s</i></sub> /
     * (x<sub><i>A</i></sub> * n<sub><i>s</i></sub>)
     *
     * <p>
     * where n<sub><i>A,s</i></sub> is the number of atoms of type A in shell s,
     * x<sub><i>A</i></sub> is the composition of atom A, and
     * n<sub><i>s</i></sub>
     * is the number of atoms in shell s.
     *
     * <p>
     * <b>Citation</b>: <a
     * href="http://journals.aps.org/pr/abstract/10.1103/PhysRev.77.669">
     * Cowley, J. <i>Physical Review Letters</i>. 77 (1950), 669</a>
     *
     * <p>
     * Optionally, one can weight the contributions of each neighbor based on
     * the path weights. See {@link BaseVoronoiCell#getNeighborsByWalks(java.util.List, int) }
     * for further discussion
     *
     * @param shell Index of nearest neighbor shell
     * @param weighted Whether to compute the weighted ordering parameters
     * @return Ordering parameters each atom in cell
     */
    public double[][] getNeighborOrderingParameters(int shell, boolean weighted) {
        double[][] output = new double[Structure.nAtoms()][Structure.nTypes()];

        // Compute the composition
        double[] x = new double[Structure.nTypes()];
        for (Atom atom : Structure.getAtoms()) {
            x[atom.getType()]++;
        }
        for (int i = 0; i < x.length; i++) {
            x[i] /= Structure.nAtoms();
        }

        // Compute the WC parameters for each atom
        for (int a = 0; a < Structure.nAtoms(); a++) {
            if (weighted) {
                // Get the neighbors and weights
                Map<AtomImage,Double> neighborShell = Cells.get(a).getNeighborsByWalks(Cells, shell);

                // Compute the actual weight of each type
                double[] nA = new double[Structure.nTypes()];
                for (Map.Entry<AtomImage,Double> neighbor : neighborShell.entrySet()) {
                    nA[neighbor.getKey().getAtom().getType()]
                            += neighbor.getValue();
                }

                // Compute the WC parameters
                for (int t = 0; t < Structure.nTypes(); t++) {
                    if (x[t] == 0) {
                        x[t] = 0;
                    } else {
                        output[a][t] = 1 - nA[t] / x[t];
                    }
                }
            } else {
                // Get the neighboring atoms
                Set<AtomImage> neighbors = Cells.get(a).getNeighborShell(Cells, shell);

                // Compute the actual number of each type
                double[] nA = new double[Structure.nTypes()];
                for (AtomImage neighbor : neighbors) {
                    nA[neighbor.getAtom().getType()]++;
                }

                // Compute the WC parameters
                for (int t = 0; t < Structure.nTypes(); t++) {
                    if (x[t] == 0) {
                        x[t] = 0;
                    } else {
                        output[a][t] = 1 - nA[t] / neighbors.size() / x[t];
                    }
                }
            }
        }

        return output;
    }

    /**
     * Compute the mean deviation in the Warren-Cowley parameter for each type
     * for each site from 0. Consider this a measure of how "Ordered" a
     * structure is.
     *
     * <p>
     * Computed as the average of the average the absolute values of the WC
     * parameters for each type for each site:
     *
     * <p>
     * Sum<sub>i,j</sub>1 / [ Number of atoms in structure ] / [ Number of types
     * ] * | [ WC parameter for type i about atom j ] |
     *
     * @param shell Index of neighbor shell (e.g. 1st shell = 1)
     * @param weighted Whether to weigh ordering parameters by face area
     * @return Computed value
     * @see VoronoiCellBasedAnalysis#getNeighborOrderingParameters(int, boolean)
     */
    public double warrenCowleyOrderingMagnituide(int shell, boolean weighted) {
        // Get the WC ordering parameters
        double[][] WC = getNeighborOrderingParameters(shell, weighted);

        // Compute average
        double output = 0;
        for (double[] a : WC) {
            for (double x : a) {
                output += Math.abs(x);
            }
        }
        return output / Structure.nAtoms() / Structure.nTypes();
    }

    /**
     * Compute the similarity of the shape of a cell to a reference. Computed as
     * the difference between the number of faces with a certain number of edges
     * for between a shape and the reference for each type of face (defined by
     * the number of edges) divided by the total number of faces in the
     * reference shape.
     *
     * <p>
     * <b>Example</b>: Reference shape has 12 square faces, this shape has 11
     * square faces and two triangular faces. There are three different faces
     * (i.e. one missing square face and two extraneous triangular faces).
     * Therefore the dissimilarity is: 3 / 12 = 25%.
     *
     * @param thisShape Shape to be compared
     * @param referenceShape Shape to compare against
     * @return Dissimilarity figure (0 means identical, can be > 1)
     */
    static public double computeShapeDissimilarity(Map<Integer, Integer> thisShape,
            Map<Integer, Integer> referenceShape) {
        int nDifferences = 0;
        int nFaces = 0;

        // For all shapes that are in reference pattern
        for (Map.Entry<Integer, Integer> entrySet : referenceShape.entrySet()) {
            Integer type = entrySet.getKey();
            Integer count = entrySet.getValue();
            nFaces += count;
            if (thisShape.containsKey(type)) {
                nDifferences += Math.abs(count - thisShape.get(type));
            } else {
                nDifferences += count;
            }
        }

        // For all types in this shape, but not reference pattern
        Set<Integer> typesToEval = thisShape.keySet();
        typesToEval.removeAll(referenceShape.keySet());
        for (Integer type : typesToEval) {
            nDifferences += thisShape.get(type);
        }

        return (double) nDifferences / nFaces;
    }

    /**
     * Get how dissimilar, on average, coordination polyhedra are from SC.
     *
     * @return Average shape dissimilarity from SC
     */
    public double meanSCDissimilarity() {
        // Make reference
        Map<Integer, Integer> ref = new HashMap<>();
        ref.put(4, 6);

        // Compute dissimilarity for each entry
        double[] dissimilarity = new double[Structure.nAtoms()];
        for (int i = 0; i < dissimilarity.length; i++) {
            dissimilarity[i] = computeShapeDissimilarity(
                    Cells.get(i).getPolyhedronShape(), ref);
        }

        return StatUtils.mean(dissimilarity);
    }

    /**
     * Get how dissimilar, on average, coordination polyhedra are from BCC.
     *
     * @return Average shape dissimilarity from BCC
     */
    public double meanBCCDissimilarity() {
        // Make reference
        Map<Integer, Integer> ref = new HashMap<>();
        ref.put(6, 8);
        ref.put(4, 6);

        // Compute dissimilarity for each entry
        double[] dissimilarity = new double[Structure.nAtoms()];
        for (int i = 0; i < dissimilarity.length; i++) {
            dissimilarity[i] = computeShapeDissimilarity(
                    Cells.get(i).getPolyhedronShape(), ref);
        }

        return StatUtils.mean(dissimilarity);
    }

    /**
     * Get how dissimilar, on average, coordination polyhedra are from FCC.
     *
     * @return Average shape dissimilarity from FCC
     */
    public double meanFCCDissimilarity() {
        // Make reference
        Map<Integer, Integer> ref = new HashMap<>();
        ref.put(4, 12);

        // Compute dissimilarity for each entry
        double[] dissimilarity = new double[Structure.nAtoms()];
        for (int i = 0; i < dissimilarity.length; i++) {
            dissimilarity[i] = computeShapeDissimilarity(
                    Cells.get(i).getPolyhedronShape(), ref);
        }

        return StatUtils.mean(dissimilarity);
    }

    /**
     * Compute the face-size-weighted difference between properties of an atom
     * and its neighbors. Computed as:
     *
     * <p>
     * sum<sub><i</sub>[ Size of face between atom and neighbor i ] * |
     * [property of atom] - [property neighbor] | / [ surface area ]
     *
     * <p>
     * For neighbors in the 2nd or greater shell, {@linkplain BaseVoronoiCell#getExtendedFaces(java.util.List, int) )
     * is used to find the unique faces between the atoms in the N - 1 shell
     * and the Nth shell. So, each Nth neighbor atom might have multiple
     * faces and its total "size of face" will be defined as the sum of the
     * area of these faces.
     *
     * @param property List of property for each atom type
     * @param shell Shell to be considered (1 == 1st nearest neighbor shell)
     * @return Property difference value for each neighbor
     */
    public double[] neighborPropertyDifferences(double[] property, int shell) {
        if (property.length != Structure.nTypes()) {
            throw new IllegalArgumentException("Wrong number of property values provided");
        }

        // Get lookup table of face areas and types
        Pair<int[][], double[][]> neighbors = getNeighborShellWeights(shell);
        double[][] faceAreas = neighbors.getRight();
        int[][] faceTypes = neighbors.getLeft();

        // Compute neighbor property differences
        double[] output = neighborPropertyDifferences(property, faceAreas, faceTypes);

        return output;
    }

    /**
     * Get the types and weights on neighbors in each shell. Uses the
     * path weight argument described in {@linkplain BaseVoronoiCell#getNeighborsByWalks(java.util.List, int) }
     *
     * @param shell Shell being considered. 1 corresponds to the 1st NN,
     * 2 corresponds to the polyhedron formed by an atom and its 1st shell
     * neighbors, etc
     * @return Pair of neighbor types and weights
     * @see BaseVoronoiCell#getExtendedFaces(java.util.List, int)
     */
    public Pair<int[][], double[][]> getNeighborShellWeights(int shell) {
        int[][] types = new int[Structure.nAtoms()][];
        double[][] weights = new double[Structure.nAtoms()][];

        for (int a = 0; a < Structure.nAtoms(); a++) {
            // Get extended faces
            Map<AtomImage,Double> neighbors = Cells.get(a).getNeighborsByWalks(Cells, shell);

            // Store their areas
            weights[a] = new double[neighbors.size()];
            types[a] = new int[neighbors.size()];
            int f = 0;
            for (Map.Entry<AtomImage,Double> neighbor : neighbors.entrySet()) {
                weights[a][f] = neighbor.getValue();
                types[a][f] = neighbor.getKey().getAtom().getType();
                f++;
            }
        }

        return new ImmutablePair<>(types, weights);
    }

    /**
     * Provided a pre-computed list of neighbor types and weights, compute the
     * neighbor property differences.
     *
     * @param property List of property for each type
     * @param weights Weight for each neighbor
     * @param faceTypes Type of each neighbor
     * appropriate cell
     * @return Property difference value for each neighbor
     * @see #neighborPropertyDifferences(double[], int)
     */
    public double[] neighborPropertyDifferences(double[] property, double[][] weights,
            int[][] faceTypes) {
        // Compute average difference for each cell
        double[] output = new double[Structure.nAtoms()];
        for (int a = 0; a < Structure.nAtoms(); a++) {
            double tally = 0;

            // Get the property of this atom
            double myValue = property[Structure.getAtom(a).getType()];

            // Compute the variance
            for (int f = 0; f < weights[a].length; f++) {
                double theirValue = property[faceTypes[a][f]];
                tally += weights[a][f] * Math.abs(myValue - theirValue);
            }

            // Store the normalized result
            output[a] = tally;
        }
        return output;
    }

    /**
     * Get the face-size-weighted variance between the properties of each atom's
     * neighbors.
     *
     * @param property List of property for each atom type
     * @param shell Shell to be considered (1 == 1st nearest neighbor shell)
     * @return Variance in property in property of atom for each neighbor
     * @see #neighborPropertyDifferences(double[], int)
     */
    public double[] neighborPropertyVariances(double[] property, int shell) {
        if (property.length != Structure.nTypes()) {
            throw new IllegalArgumentException("Wrong number of property values provided");
        }

        // Get lookup table of face areas and types
        Pair<int[][], double[][]> nieghbors = getNeighborShellWeights(shell);
        double[][] weights = nieghbors.getRight();
        int[][] types = nieghbors.getLeft();

        // Compute variances
        double[] output = neighborPropertyVariances(property, weights, types);

        return output;
    }

    /**
     * Get the weighted variance between the properties of each atom's
     * neighbors.
     *
     * @param property List of property for each type
     * @param weights Weight for each neighbor
     * @param types Types of each neighbors
     * @return Variance in property in property of atom for each neighbor
     * @throws MathIllegalArgumentException
     */
    public double[] neighborPropertyVariances(double[] property, double[][] weights,
            int[][] types) {
        // Compute average difference for each cell
        double[] output = new double[Structure.nAtoms()];
        for (int a = 0; a < Structure.nAtoms(); a++) {

            // Get the property for each neighbor
            double[] faceProps = new double[types[a].length];
            for (int f = 0; f < faceProps.length; f++) {
                faceProps[f] = property[types[a][f]];
            }

            // Compute the face-weighted mean
            double mean = 0;
            for (int f = 0; f < faceProps.length; f++) {
                mean += weights[a][f] * faceProps[f];
            }

            // Compute the face-weighted variance
            output[a] = 0;
            double x;
            for (int f = 0; f < faceProps.length; f++) {
                x = faceProps[f] - mean;
                output[a] += weights[a][f] * x * x;
            }
        }
        return output;
    }

    /**
     * Get the bond lengths for each atom
     *
     * @return Bond lengths for each atom in Cartesian units
     */
    public double[][] bondLengths() {
        double[][] output = new double[Structure.nAtoms()][];
        for (int a = 0; a < Structure.nAtoms(); a++) {
            output[a] = Cells.get(a).getNeighborDistances();
        }
        return output;
    }

    /**
     * Compute mean bond length for each cell. Bond length for each neighbor is
     * weighted by face size
     *
     * @return Mean bond lengths
     */
    public double[] meanBondLengths() {
        double[] output = new double[Structure.nAtoms()];
        for (int a = 0; a < Structure.nAtoms(); a++) {
            double dist = 0.0;
            for (BaseVoronoiFace face : Cells.get(a).getFaces()) {
                dist += face.getArea() * face.getNeighborDistance();
            }
            output[a] = dist / Cells.get(a).getSurfaceArea();
        }
        return output;
    }

    /**
     * Compute variance in bond length for each face. Computed as the mean
     * absolute deviation of each neighbor distance from the mean neighbor
     * distance, weighted by face area.
     *
     * @param meanLengths Mean bond length for each cell.
     * @return Bond length variance for each cell
     */
    public double[] bondLengthVariance(double[] meanLengths) {
        double[] output = new double[Structure.nAtoms()];
        for (int a = 0; a < Structure.nAtoms(); a++) {
            double dist = 0.0;
            for (BaseVoronoiFace face : Cells.get(a).getFaces()) {
                dist += face.getArea() * Math.abs(face.getNeighborDistance()
                        - meanLengths[a]);
            }
            output[a] = dist / Cells.get(a).getSurfaceArea();
        }
        return output;
    }
}
