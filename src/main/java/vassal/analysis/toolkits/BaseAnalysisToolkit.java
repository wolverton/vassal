package vassal.analysis.toolkits;

import vassal.data.Cell;
import vassal.util.Options;

/**
 * Class designed to perform simple structure analyses from the command line.
 * Purpose is simple: Analyze a structure and prepare a user friendly report
 * as a string.
 *
 * <p><b>How to Use These Tools</b>
 * <ol>
 * <li>Create a new instances of this class and
 * <li>Call the {@linkplain #analyzeStructure(vassal.data.Cell) }
 * operation with the cell of interest
 * </ol>
 *
 * <p><b>Implementation Guide</b>
 * <ol>
 * <li>Define list of available options, and create {@linkplain #setOptions(java.util.List) }
 * and {@linkplain #printUsage() }
 * <li>Implement {@linkplain #analyzeStructure(vassal.data.Cell) }
 * </ol>
 *
 * @author Logan Ward
 */
abstract public class BaseAnalysisToolkit implements Options {

    /**
     * Analyze structure and prepare report of results.
     * @param cell Structure to be analyzed
     * @return Report of findings
     * @throws Exception
     */
    abstract public String analyzeStructure(Cell cell) throws Exception;
}
