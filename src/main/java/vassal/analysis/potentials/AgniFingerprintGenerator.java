package vassal.analysis.potentials;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import vassal.analysis.BaseAnalysis;
import vassal.analysis.PairDistanceAnalysis;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.util.MathUtils;

import java.util.*;

/**
 * Implementation of the basis functions introduced by <a href="http://onlinelibrary.wiley.com/doi/10.1002/qua.24836/abstract">
 *     Botu <i>et al.</i></a>.
 * @author Logan Ward
 */
public class AgniFingerprintGenerator extends BaseAnalysis {
    /** Tool used to find and compute distances to neighboring atoms */
    final protected PairDistanceAnalysis NeighborFinder = new PairDistanceAnalysis();
    /** &eta; values used when evaluating fingerprints */
    final protected SortedSet<Double> Etas = new TreeSet<>();

    @Override
    protected void precompute() throws Exception {
        NeighborFinder.analyzeStructure(Structure);
    }

    /**
     * Clear the list of &eta; used when computing the fingerprints
     */
    public void clearEtas() {
        Etas.clear();
    }

    /**
     * Add an eta to the fingerprint vector
     * @param eta Value to add
     */
    public void addEta(double eta) {
        Etas.add(eta);
    }

    /**
     * Set a logarithmic spacing for the etas. Controls the size of the fingerprint vector.
     *
     * <p>If size == 1, only generates the mimimum value</p>
     *
     * @param min Minimum value. 10<sup>-1</sup> in original paper
     * @param max Maximum value. 10<sup>2</sup> in original paper
     * @param number Number of values (must be >=1)
     */
    public void setEtasLogarithmic(double min, double max, int number) {
        // Generate the output
        double[] output = MathUtils.logspace(min, max, number);

        // Set the etas
        clearEtas();
        for (double n : output) {
            addEta(n);
        }
    }

    /**
     * Get the list of &eta;s used when computing the fingerprint
     * @return List of &eta;s sorted from minimum to maximum
     */
    public List<Double> getEtas() {
        return new ArrayList<>(Etas);
    }

    /**
     * Compute the fingerprints for each atom in each direction
     * @return Fingerprints, where <code>x<sub>i</sub><sub>j</sub></code> is the fingerprint vector for atom <code>i</code> in
     * direction <code>j</code> (where 0=x, 1=y, 2=z)
     */
    public double[][][] computeAllFingerprints() {
        double[][][] output = new double[Structure.nAtoms()][3][];
        for (int a = 0; a < Structure.nAtoms(); a++) {
            output[a] = computeFingerprint(a);
        }
        return output;
    }

    /**
     * Get the cutoff distance for this model
     * @return Cutoff distance
     */
    public double getCutoffDistance() {
        return NeighborFinder.getCutoffDistance();
    }

    /**
     * Set the cutoff distance for the fingerprints
     *
     * @param distance Cutoff distance
     */
    public void setCutoffDistance(double distance) {
        NeighborFinder.setCutoffDistance(distance);
    }

    /**
     * Compute the fingerprint for a single atom in a single direction
     * @param atomID Index of atom
     * @return Fingerprints for <code>atomID</code> in the x, y, and z directions where x[d][e] is the fingerprint #e
     * in direction d (0->x, 1->y, 2->z)
     */
    public double[][] computeFingerprint(int atomID) {
        // Check the inputs
        if (atomID < 0 || atomID >= Structure.nAtoms()) {
            throw new IllegalArgumentException("atomID must be <= 0 and < number of atoms: " + Structure.nAtoms());
        }

        // Get the nearest neighbors
        List<Pair<AtomImage, Double>> neighbors = NeighborFinder.getAllNeighborsOfAtom(atomID);

        // Get the distances as an array, for conveniences
        double[] r = new double[neighbors.size()];
        for (int n=0; n<r.length; n++) {
            r[n] = neighbors.get(n).getValue();
        }

        // Compute the contribution to the cutoff function for each neighbor
        double[] cutoffFunction = computeCutoffContribution(r);

        // Sum their contributions to the fingerprint
        double[][] output = new double[3][];
        for (int dir = 0; dir < 3; dir++) {
            // Compute the projection of the distance vector in direction u
            double[] projectedR = computeProjectedR(atomID, dir, neighbors);

            // Compute the projected fingerprint by multiplying the "projectedR" by the
            output[dir] = new double[Etas.size()];
            Arrays.fill(output[dir], 0);
            int e = 0;
            for (double eta : Etas) {
                for (int n = 0; n < cutoffFunction.length; n++) {
                    output[dir][e] += projectedR[n] / r[n] * Math.exp(-1 * r[n] * r[n] / eta) * cutoffFunction[n];
                }
                e++;
            }
        }

        return output;
    }

    /**
     * Compute the distance between our atom and each of its neighbors projected along a certain direction.
     * @param atomID ID of atom being assessed
     * @param direction Projection direction (x == 0, y == 1, z == 2)
     * @param neighbors All neighbors of atom within cutoff radius
     * @return Projection of the distance vector
     */
    protected double[] computeProjectedR(int atomID, int direction, List<Pair<AtomImage, Double>> neighbors) {
        double[] projectedR = new double[neighbors.size()];
        Atom myAtom = Structure.getAtom(atomID);
        Vector3D myPosition = new Vector3D(myAtom.getPositionCartesian());
        Vector3D projVector = new Vector3D(direction == 0 ? 1 : 0, direction == 1 ? 1 : 0, direction == 2 ? 1 : 0);
        for (int i=0; i<projectedR.length; i++) {
            projectedR[i] = neighbors.get(i).getKey().getPosition().subtract(myPosition).dotProduct(projVector);
        }
        return projectedR;
    }

    /**
     * Compute the contribution of the cutoff function to this data. This is defined by the function,
     *
     * <p>
     *     <code>f(r) = 0.5 * (cos(&pi;r/R<sub>c</sub>) + 1) </code>
     * </p>
     *
     * @param distances List of distances to each neighbor
     * @return Value of the cutoff function for each neighbor
     */
    protected double[] computeCutoffContribution(double[] distances) {
        double[] cutoffContribution = new double[distances.length];
        double Rc = NeighborFinder.getCutoffDistance();
        double r;
        for (int i = 0; i<cutoffContribution.length; i++) {
            r = distances[i];
            cutoffContribution[i] = r < Rc ? 0.5 * (Math.cos(Math.PI * r / Rc) + 1) : 0;
        }
        return cutoffContribution;
    }
}
