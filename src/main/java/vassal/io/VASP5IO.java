package vassal.io;

import java.util.*;
import vassal.data.Atom;
import vassal.data.Cell;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Read/write operations for VASP 5 format. Note that this version expects that
 *  the sixth line to contain the names of elements.
 * @author Logan Ward
 */
public class VASP5IO extends StructureIO {

	@Override
	public List<String> convertStructureToString(Cell strc) throws Exception {
		List<String> output = new LinkedList<>();

		// Write header
		output.add("Automatically-generated POSCAR");
		output.add("1.0");

		// Write lattice vectors
		Vector3D[] latVecs = strc.getLatticeVectors();
		for (int i=0; i < 3; i++) {
			output.add(String.format("  %.10f %.10f %.10f", latVecs[i].getX(),
				latVecs[i].getY() , latVecs[i].getZ()));
		}

		// Gather atoms
		NavigableMap<Integer, String> names = new TreeMap<>();
		NavigableMap<Integer, List<Atom>> atoms = new TreeMap<>();
		for (Atom atom : strc.getAtoms()) {
			int type = atom.getType();
			if (names.containsKey(type)) {
				atoms.get(type).add(atom);
			} else {
				names.put(type, strc.getTypeName(type));
				List<Atom> temp = new LinkedList<>();
				temp.add(atom);
				atoms.put(type, temp);
			}
		}

		// Print atom types
		String nameline = "", countLine = "";
		for (Integer key : names.navigableKeySet()) {
			nameline += " " + names.get(key);
			countLine += " " + atoms.get(key).size();
		}
		output.add(nameline);
		output.add(countLine);

		output.add("Direct");

		// Print atoms
		for (Integer key : atoms.navigableKeySet()) {
			for (Atom atom : atoms.get(key)) {
				double[] x = atom.getPosition();
				output.add(String.format("  %.10f %.10f %.10f", x[0], x[1], x[2]));
			}
		}
		return output;
	}

	@Override
	public Cell parseStructure(List<String> lines) throws Exception {
		// Get basis
		double[][] basis;
		try {
			basis = new double[3][3];
			double factor = Double.parseDouble(lines.get(1));
			for (int i=0; i<3; i++) {
				String[] words = lines.get(2+i).trim().split("\\s+");
				for (int j=0; j<3; j++) {
					// VASP's basis is tranposed so that each row is a lattice vector
					// See: http://cms.mpi.univie.ac.at/vasp/guide/node59.html)
					basis[j][i] = Double.parseDouble(words[j]) * factor;
				}
			}
		} catch (Exception e) {
			throw new Exception("Error parsing basis");
		}

		// Read atom types
		String[] types; int[] typeCount;
		try {
			types = lines.get(5).trim().split("\\s+");
			String[] words = lines.get(6).trim().split("\\s+");
			typeCount = new int[types.length];
			for (int i=0; i<typeCount.length; i++) {
				typeCount[i] = Integer.parseInt(words[i]);
			}
		} catch (Exception e) {
			throw new Exception("Error parsing atom types");
		}

		// Read middle section
		int atomStart; boolean cartesian;
		try {
			// Check whether SD is on to see where atom positions start
			atomStart = lines.get(7).toLowerCase().startsWith("sele") ? 7 : 8;

			// See whether coordinates are in direct or cartesian units
			cartesian = lines.get(atomStart-1).toLowerCase().startsWith("c");
		} catch (Exception e) {
			throw new Exception("Error determining whether atoms are in cartesian units");
		}

		// Make the cell
		Cell strc = new Cell();
		strc.setBasis(basis);

		// Get atom positions
		try {
			for (int t=0; t<types.length; t++) {
				for (int ti=0; ti<typeCount[t]; ti++) {
					// Read position
					double[] x = new double[3];
					String[] words = lines.get(atomStart++).trim().split("\\s+");
					for (int wi=0; wi<3; wi++) {
						x[wi] = Double.parseDouble(words[wi]);
					}
					if (cartesian) {
						x = strc.convertCartesianToFractional(x);
					}
					Atom atom = new Atom(x, t);
					strc.addAtom(atom);
				}
				strc.setTypeName(t, types[t]);
			}
		} catch (Exception e) {
			throw new Exception("Error parsing atoms");
		}

		return strc;
	}
}
