package vassal.io;

import vassal.data.Cell;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Handles reading and writing structure files.
 *
 * @author Logan Ward
 */
abstract public class StructureIO implements Serializable {

	/**
	 * Read a structure from file
	 * @param filename Path to file
	 * @return Structure
     * @throws java.lang.Exception
	 */
	public Cell parseFile(String filename) throws Exception {
		// Open file
		BufferedReader fp = new BufferedReader(new FileReader(filename));

		// Read in entire file
		List<String> file = new LinkedList<>();
		while (true) {
			String line = fp.readLine();
			if (line == null) break;
			file.add(line);
		}
		fp.close();

		return parseStructure(file);
	}

	/**
	 * Given lines of a file that describes a structure, generate structure.
	 * @param lines Lines describing structure
	 * @return Structure
	 * @throws java.lang.Exception If structure is unparseable
	 */
	abstract public Cell parseStructure(List<String> lines) throws Exception;

    /**
     * Parse a structure given the contents as a string
     *
     * @param fileContents File contents
     * @return Cell represententing object
     */
    public Cell parseStructure(String fileContents) throws Exception {
        String[] lines = fileContents.split("\n");
        return parseStructure(Arrays.asList(lines));
    }

	/**
	 * Write structure to file
	 * @param strc Structure to output
	 * @param filename Path to file
	 * @throws Exception
	 */
	public void writeStructureToFile(Cell strc, String filename) throws Exception {
		// Generate description
		List<String> toWrite = convertStructureToString(strc);

		// Write to file
		PrintWriter fp = new PrintWriter(filename);
		for (String line : toWrite) {
			fp.println(line);
		}
		fp.close();
	}

	/**
	 * Convert structure to string representation
	 * @param strc Structure to convert
	 * @return List of lines, where each line would be a line in a file
	 * @throws java.lang.Exception
	 */
	abstract public List<String> convertStructureToString(Cell strc) throws Exception;

    /**
     * Print the structure as a string
     *
     * @param strc Structure to be printed
     * @return String describing the structure in this format
     * @throws Exception
     */
    public String printStructure(Cell strc) throws Exception {
        // Get the structure as a list of strings
        List<String> lines = convertStructureToString(strc);
        
        // Combine lines
        Iterator<String> iter = lines.iterator();
        if (! iter.hasNext()) {
            return "";
        }
        StringBuilder output = new StringBuilder();
        output.append(iter.next());
        while (iter.hasNext()) {
            output.append("\n");
            output.append(iter.next());
        }
        return output.toString();
    }

}
