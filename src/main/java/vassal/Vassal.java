package vassal;

import java.io.File;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import vassal.analysis.toolkits.BaseAnalysisToolkit;
import vassal.data.Cell;
import vassal.io.StructureIO;
import vassal.util.Options;

/**
 * Main class for Vassal. Allows users to perform structural analysis methods
 * from the command line.
 *
 * <p><b>Usage</b>: &lt;Import Method&gt; &lt;File&gt; &lt;Analysis Tool&gt;
 * &lt;Options...&gt;
 * <br><i>Import Method</i>: Name of {@linkplain StructureIO} method
 * <br><i>File</i>: Path to file to be analyzed
 * <br><i>Analysis Tool</i>: Name of a {@linkplain BaseAnalysisToolkit} class to do the analysis
 * <br><i>Options...</i>: Options for the analysis
 *
 * @author Logan Ward
 */
public class Vassal {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		if (args.length < 3) {
            System.out.println("Usage: <Import Method> <Path to Structure> <Analysis Method>");
            return;
        }

        // Check that file exists
        File file = new File(args[1]);
        if (! file.isFile()) {
            System.out.println("No file found at: " + file.getPath());
            return;
        }

        try {
            // Load the IO and analysis tools
            StructureIO io = (StructureIO) instantiateClass("io." + args[0], null);
            BaseAnalysisToolkit tool = (BaseAnalysisToolkit)
                    instantiateClass("analysis.toolkits." + args[2],
                            Arrays.asList(args).subList(3, args.length));

            // Run it
            Cell strc = io.parseFile(file.getPath());
            String result = tool.analyzeStructure(strc);

            // Print it
            System.out.println(result);
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
	}

    /**
     * Create new instance of a class given type and options. Class name must be fully
     *  specified within the Vassal package (i.e. "io.Vasp5IO")
     *
     * @param ClassType Full name of class to create minus "vassal"
     * @param Options Array specifying option commands (can be empty/null)
     * @return Newly instantiated class
     * @throws Exception For various reasons
     */
    static public Object instantiateClass(String ClassType, List<String> Options) throws Exception {
        Object NewObj;
        try {
            Class x = Class.forName("vassal." + ClassType);
            NewObj = x.newInstance();
        } catch (ClassNotFoundException e) {
            throw new Exception("ERROR: Class " + ClassType + " not found");
        } catch (Exception e) {
            throw new Error("FATAL ERROR: Something wrong with " + ClassType + "'s implementation\n" + e);
        }
        if (NewObj instanceof Options) {
            if (Options == null)
                Options = new LinkedList<String>();
            Options Ptr = (Options) NewObj;
            try {
                Ptr.setOptions(Options);
            } catch (Exception e) {
                throw new Exception("Error when setting options of " +
                        ClassType + " - " + e.getLocalizedMessage());
            }
        }
        return NewObj;
    }
}
