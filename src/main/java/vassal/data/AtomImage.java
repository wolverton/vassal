
package vassal.data;
import org.apache.commons.math3.geometry.euclidean.threed.*;
import java.util.*;

/**
 * Uniquely identifies an atom image. Key is atom ID, value is periodic image.
 * @author Logan Ward
 */
public class AtomImage implements Comparable<AtomImage> {
    /** Pre-computed Cartesian coordinates of this image */
    protected Vector3D Position;
    /** Atom which this image is associated with */
    protected Atom Atom;
    /** Supercell in which this image is located */
    protected int[] Supercell;

    /**
     * Create a an atom image object
     * @param atom Link to atom that this
     * @param image Supercell position (i.e., which image it is in)
     */
    public AtomImage(Atom atom, int[] image) {
        Supercell = image.clone();
        Atom = atom;
        computePosition();
    }

    @Override
    public int compareTo(AtomImage other) {
        if (Atom.equals(other.Atom)) {
            int[] otherRight = other.Supercell;
            if (Supercell[0] != otherRight[0]) {
                return Integer.compare(Supercell[0], otherRight[0]);
            } else if (Supercell[1] != otherRight[1]) {
                return Integer.compare(Supercell[1], otherRight[1]);
            } else {
                return Integer.compare(Supercell[2], otherRight[2]);
            }
        } else {
            return Integer.compare(Atom.getID(), other.getAtomID());
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AtomImage) {
            AtomImage other = (AtomImage) obj;
            return Atom.getID() == other.getAtomID()
                    && Arrays.equals(Supercell, other.Supercell);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.Atom);
        hash = 43 * hash + Arrays.hashCode(this.Supercell);
        return hash;
    }

    /**
     * Compute (or re-compute) position of this image
     */
    public final void computePosition() {
        Position = new Vector3D(Atom.getCell().getPeriodicImage(Atom.getPositionCartesian(),
                Supercell[0], Supercell[1], Supercell[2]));
    }

    /**
     * Get the atom at the center of this
     * @return Link to atom at the center
     */
    public Atom getAtom() {
        return Atom;
    }

    /**
     * Get the ID of the atom associated with this image
     * @return Atom ID
     */
    public int getAtomID() {
        return Atom.getID();
    }

    /**
     * Get which supercell this image is located in
     * @return Supercell coordinates
     */
    public int[] getSupercell() {
        return Supercell.clone();
    }

    /**
     * Get the position (in Cartesian coordinates) of this image.
     * @return Position of atom
     */
    public Vector3D getPosition() {
        return Position;
    }

    @Override
    public String toString() {
        String output = Integer.toString(Atom.getID());
        int[] image = getSupercell();
        if (image[0] == 0 && image[1] == 0 && image[2] == 0) {
            return output;
        } else {
            output += "(";
            for (int i=0; i<3; i++) {
                if (image[i] != 0) {
                    output += String.format("%d%s", image[i],
                            String.valueOf(Character.toChars(97 + i)));
                }
            }
            return output + ")";
        }
    }
}
