package vassal.util;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.*;

/**
 * Tool to find all combinations of 3 vectors that are shorter than a certain
 * length.
 *
 * @author Logan Ward
 */
public final class VectorCombinationComputer {
    /**
     * Vectors to be added
     */
    final private Vector3D[] InputVectors;

    /**
     * Square of cutoff distance
     */
    final private double CutoffDistanceSq;

    /**
     * x,y,z coordinates of each vector shorter than cutoff. Same order as {@linkplain #Vectors}
     */
    private final List<int[]> Supercells = new LinkedList<>();

    /**
     * All vectors shorter than cutoff. Same order as {@linkplain #Supercells}
     */
    private final List<Vector3D> Vectors = new LinkedList<>();

    /**
     * Whether to include the zero vector in the list.
     */
    private final boolean IncludeZero;

    /**
     * Compute all combinations of input vectors shorter than cutoff distance.
     * Includes the zero vector.
     * @param inputVectors Vectors to be combined. Must be exactly 3 linearly-independent vectors
     * @param cutoffDistance Cutoff distance
     */
    public VectorCombinationComputer(Vector3D[] inputVectors, double cutoffDistance) {
        if (inputVectors.length != 3) {
            throw new Error("Expecting exactly three vectors");
        }
        this.InputVectors = inputVectors.clone();
        this.CutoffDistanceSq = cutoffDistance * cutoffDistance;
        this.IncludeZero = true;

        getAllVectors();
    }

    /**
     * Compute all combinations of input vectors shorter than cutoff distance
     * @param inputVectors Vectors to be combined. Must be exactly 3 linearly-independent vectors
     * @param cutoffDistance Cutoff distance
     * @param includeZero Whether to include the zero vector
     */
    public VectorCombinationComputer(Vector3D[] inputVectors, double cutoffDistance,
            boolean includeZero) {
        if (inputVectors.length != 3) {
            throw new Error("Expecting exactly three vectors");
        }
        this.InputVectors = inputVectors.clone();
        this.CutoffDistanceSq = cutoffDistance * cutoffDistance;
        this.IncludeZero = includeZero;

        getAllVectors();
    }

    /**
     * Compute all vectors that are shorter than cutoff distance.
     */
    protected void getAllVectors() {
        // Create a matrix of basis vectors
        RealMatrix basis = new Array2DRowRealMatrix(3,3);
        for (int i=0; i<3; i++) {
            basis.setColumn(i, InputVectors[i].toArray());
        }

        // Create ability to invert it
        LUDecomposition inverter = new LUDecomposition(basis);
        if (inverter.getDeterminant() == 0) {
            throw new Error("Vectors are not linearly-independant");
        }
        DecompositionSolver solver = inverter.getSolver();

        // Compute range of each variable
        double cutoffDistance = Math.sqrt(CutoffDistanceSq);
        int[] stepRange = new int[3];
        for (int i=0; i<3; i++) {
            double maxDisp = 0.0;
            for (int j=0; j<3; j++) {
                maxDisp += InputVectors[i].dotProduct(InputVectors[j]) / InputVectors[i].getNorm();
            }
            stepRange[i] = (int) Math.ceil(maxDisp / cutoffDistance) + 1;
        }

        // Ensure that we have sufficient range to get the cutoff distance away from
        //  the origin by checking that we have large enough range to access
        //  a point cutoff distance away along the direction of the xy, xz, and yz
        //  cross products
        for (int dir=0; dir<3; dir++) {
            Vector3D point = InputVectors[dir].crossProduct(InputVectors[(dir + 1) % 3])
                    .normalize().scalarMultiply(cutoffDistance);
            RealVector sln = solver.solve(new ArrayRealVector(point.toArray()));
            for (int i=0; i<3; i++) {
                stepRange[i] = Math.max(stepRange[i],
                        (int) Math.ceil(Math.abs(sln.getEntry(i))));
            }
        }

        // Create the initial vector
        for (int x = -stepRange[0]; x <= stepRange[0]; x++) {
            for (int y = -stepRange[1]; y <= stepRange[1]; y++) {
                for (int z = -stepRange[2]; z <= stepRange[2]; z++) {
                    double[] l = computeVector(x, y, z);
                    double distSq = l[0] * l[0] + l[1] * l[1]
                            + l[2] * l[2];
                    if (distSq <= CutoffDistanceSq) {
                        if (!IncludeZero && x == 0 && y == 0 && z == 0) {
                            continue;
                        }
                        Supercells.add(new int[]{x, y, z});
                        Vectors.add(new Vector3D(l));
                    }
                }
            }
        }
    }

    /**
     * Compute a certain combination of the vectors stored in this array.
     * @param x X coordinate
     * @param y Y coordinate
     * @param z Z coordinate
     * @return Combination
     */
    public double[] computeVector(int x, int y, int z) {
        double[] output = new double[3];
        output[0] = x * InputVectors[0].getX() + y * InputVectors[1].getX()
                + z * InputVectors[2].getX();
        output[1] = x * InputVectors[0].getY() + y * InputVectors[1].getY()
                + z * InputVectors[2].getY();
        output[2] = x * InputVectors[0].getZ() + y * InputVectors[1].getZ()
                + z * InputVectors[2].getZ();
        return output;
    }

    /**
     * Get list of all vectors shorter than the cutoff
     * @return List of vectors
     */
    public List<Vector3D> getVectors() {
        return new ArrayList<>(Vectors);
    }

    /**
     * Get list of all image coordinates of vectors
     * @return List of supercell coordinates
     */
    public List<int[]> getSupercellCoordinates() {
        return new ArrayList<>(Supercells);
    }
}
