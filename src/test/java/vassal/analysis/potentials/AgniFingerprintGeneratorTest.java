package vassal.analysis.potentials;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.junit.Test;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.data.Cell;

import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author Logan Ward
 */
public class AgniFingerprintGeneratorTest {
    @Test
    public void setCutoffDistance() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();
        gen.setCutoffDistance(1.0);
        assertEquals(1.0, gen.getCutoffDistance(), 1e-6);
    }

    @Test
    public void setEtasLogarithmic() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();
        gen.setEtasLogarithmic(1e-1, 1e2, 4);
        assertArrayEquals(new double[]{1e-1, 1e0, 1e1, 1e2},
                ArrayUtils.toPrimitive(gen.getEtas().toArray(new Double[0])),
                1e-6);
    }

    @Test
    public void computeFingerprintTrivial() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();
        gen.setEtasLogarithmic(1e-1, 1e2, 10);
        Cell cell = makeSimpleCubic();
        gen.analyzeStructure(cell);

        // Regardless of the cell size, the fingerprints should be equal to zero (symmetry)
        for (double cutoff : new double[]{1.1, 3.1, 5.1}) {
            gen.setCutoffDistance(cutoff);
            double[][] fingerprints = gen.computeFingerprint(0);
            for (int d=0; d<2; d++) {
                for (double f : fingerprints[d]) {
                    assertEquals(0, f, 1e-6);
                }
            }
        }
    }

    @Test
    public void computeFingerprintBinary() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();
        gen.setEtasLogarithmic(1e-1, 1e2, 4);

        // Make a cell with an atom (0.1, 0.2, 0.3) from the origin (will enumalate a binary pair of atoms)
        Cell cell = makeSimpleCubic();
        cell.addAtom(new Atom(new double[]{0.1,0.2,0.3}, 0));
        gen.analyzeStructure(cell);
        gen.setCutoffDistance(0.5);

        // Make sure we're only getting the one pair of atoms
        assertEquals(1, gen.NeighborFinder.getAllNeighborsOfAtom(0).size());

        // Compute all the fingerprints
        double[][][] fingerprint = gen.computeAllFingerprints();
        assertEquals(2, fingerprint.length);
        assertEquals(3, fingerprint[0].length);
        assertEquals(4, fingerprint[0][0].length);

        // Make sure that fingerprints for each atom add to zero between atoms
        //  This must be true for forces to balance
        for (int d=0; d<3; d++) {
            assertEquals(0,
                    new ArrayRealVector(fingerprint[0][d]).add(new ArrayRealVector(fingerprint[1][d])).getMaxValue(),
                    1e-6);
        }

        // Compute one of the vectors by hand to make sure we've got it right
        double R = Math.sqrt(0.1 * 0.1 + 0.2 * 0.2 + 0.3 * 0.3);
        assertEquals(0.1 / R * Math.exp(-1 * R * R / 0.1) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][0][0], 1e-6);
        assertEquals(0.2 / R * Math.exp(-1 * R * R / 0.1) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][1][0], 1e-6);
        assertEquals(0.3 / R * Math.exp(-1 * R * R / 0.1) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][2][0], 1e-6);
        assertEquals(0.1 / R * Math.exp(-1 * R * R / 1) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][0][1], 1e-6);
        assertEquals(0.1 / R * Math.exp(-1 * R * R / 10) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][0][2], 1e-6);
        assertEquals(0.1 / R * Math.exp(-1 * R * R / 100) * 0.5 * (Math.cos(Math.PI * R / 0.5) + 1),
                fingerprint[0][0][3], 1e-6);
    }

    @Test
    public void computeCutoffContribution() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();

        // Make a simple test cell
        Cell cell = makeSimpleCubic();

        // Call the analysis routine
        gen.analyzeStructure(cell);

        // Get the list of 1st NNs
        gen.setCutoffDistance(1.1);
        List<Pair<AtomImage, Double>> neighbors = gen.NeighborFinder.getAllNeighborsOfAtom(0);
        assertEquals(6, neighbors.size()); // Should be 6, 1 Angstrom away

        double[] neighborDists = new double[neighbors.size()];
        for (int i = 0; i < neighborDists.length; i++) {
            neighborDists[i] = neighbors.get(i).getRight();
        }

        double[] results = gen.computeCutoffContribution(neighborDists);
        for (double result : results) {
            assertEquals(0.5 * (Math.cos(Math.PI * 1 / 1.1) + 1), result, 1e-6);
        }

        // Update the cutoff distance to 1.5 to get the 2nd NN
        gen.setCutoffDistance(1.5);

        neighbors = gen.NeighborFinder.getAllNeighborsOfAtom(0);
        assertEquals(6 + 12, neighbors.size()); // Should be 6, 1 Angstrom away
        neighborDists = new double[neighbors.size()];
        for (int i = 0; i < neighborDists.length; i++) {
            neighborDists[i] = neighbors.get(i).getRight();
        }

        results = gen.computeCutoffContribution(neighborDists);
        for (int n=0; n<neighbors.size(); n++) {
            Pair<AtomImage, Double> neighbor = neighbors.get(n);
            assertEquals(0.5 * (Math.cos(Math.PI * neighbor.getRight() / 1.5) + 1), results[n], 1e-6);
        }
    }

    @Test
    public void testProjectedR() throws Exception {
        AgniFingerprintGenerator gen = new AgniFingerprintGenerator();
        Cell cell = makeSimpleCubic();

        // Test this for a large cutoff
        gen.setCutoffDistance(3.4);
        gen.analyzeStructure(cell);
        List<Pair<AtomImage, Double>> neighbors = gen.NeighborFinder.getAllNeighborsOfAtom(0);
        for (int d=0; d<3; d++) {
            double[] projectR = gen.computeProjectedR(0, d, neighbors);
            assertEquals(neighbors.size(), projectR.length);
            for (int n=0; n<projectR.length; n++) {
                // The distance in the d direction is equal to the number of supercell steps in that direction
                assertEquals(projectR[n], neighbors.get(n).getLeft().getSupercell()[d], 1e-6);
            }
        }
    }

    /**
     * Make a simple cubic unit cell with <code>a=b=c=1</code>
     * @return Simple cubic cell
     */
    private Cell makeSimpleCubic() {
        // Make a sample cell
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));
        return cell;
    }

}