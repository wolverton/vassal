package vassal.analysis;

import vassal.data.Atom;
import vassal.data.Cell;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Logan Ward
 */
public class SimpleMeasuresTest {
    /** Structure being analyzed */
    final private Cell Structure;
    /** Analysis tool */
    final private SimpleStructureAnalysis Tool;

    public SimpleMeasuresTest() throws Exception {
        // Create the cell
        Structure = new Cell();
        Structure.addAtom(new Atom(new double[]{0,0,0}, 0));
        // Start computation
        Tool = new SimpleStructureAnalysis();
        Tool.analyzeStructure(Structure);
    }

    /**
     * Test of volumePerAtom method, of class SimpleStructureAnalysis.
     */
    @Test
    public void testVolumePerAtom() throws Exception {
        // Basic cell
        assertEquals(1.0, Tool.volumePerAtom(), 1e-6);
        // Have all the lattice parameters
    }

}
