package vassal.analysis.voronoi;

import java.util.*;
import org.apache.commons.lang3.ArrayUtils;
import vassal.analysis.voronoi.output.EdgeWriter;
import vassal.data.Atom;
import vassal.data.Cell;
import vassal.io.VASP5IO;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.data.AtomImage;

/**
 *
 * @author Logan Ward
 */
public class VoronoiTessellationCalculatorTest {

    static boolean WriteResult = false;

    @Test
    public void testSimpleCubic() throws Exception {
        // Create the simulation cell
        Cell Structure = new Cell();
        Atom atom = new Atom(new double[]{0, 0, 0}, 0);
        Structure.addAtom(atom);

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(Structure, false);

        // Evaluate results
        assertEquals(Structure.nAtoms(), result.size());
        assertEquals(6, result.get(0).getFaces().size());
        assertEquals(Structure.volume(), result.get(0).getVolume(), 1e-6);
        Map<Integer, Integer> polyIndex = result.get(0).getPolyhedronShape();
        assertEquals(Integer.valueOf(6), polyIndex.get(4));
        polyIndex = result.get(0).getCoordinationShellShape(result);
        assertEquals(6, (int) polyIndex.get(0));

        // Test out the nearest neighbor shells
        // 1st NN shell
        Set<AtomImage> nns = result.get(0).getNeighborShell(result, 1);
        assertEquals(6, nns.size());

        // 2nd NN shell
        nns = result.get(0).getNeighborShell(result, 2);
        assertEquals(18, nns.size());

        // 3rd-5th NN shell
        for (int s = 3; s <= 5; s++) {
            nns = result.get(0).getNeighborShell(result, s);
            for (AtomImage image : nns) {
                int[] cell = image.getSupercell();
                assertEquals(s, Math.abs(cell[0]) + Math.abs(cell[1])
                        + Math.abs(cell[2]), 1e-6);
            }
        }

        // Test path NNs

        //  0th NN
        Map<AtomImage,Double> paths = result.get(0).getNeighborsByWalks(result, 0);
        assertEquals(1, paths.size());
        double totalWeight = 0.0;
        for (Double weight : paths.values()) {
            totalWeight += weight;
        }
        assertEquals(1.0, totalWeight, 1e-6);

        // 1st NN
        paths = result.get(0).getNeighborsByWalks(result, 1);
        assertEquals(6, paths.size());
        totalWeight = 0.0;
        for (Map.Entry<AtomImage,Double> entry : paths.entrySet()) {
            assertEquals(1f/6, entry.getValue(), 1e-6);
        }
        for (Double weight : paths.values()) {
            totalWeight += weight;
        }
        assertEquals(1.0, totalWeight, 1e-6);

        // 2nd NN
        paths = result.get(0).getNeighborsByWalks(result, 2);
        assertEquals(18, paths.size());
        totalWeight = 0.0;
        for (Map.Entry<AtomImage,Double> entry : paths.entrySet()) {
            if (ArrayUtils.contains(entry.getKey().getSupercell(), 2) ||
                    ArrayUtils.contains(entry.getKey().getSupercell(), -2)) {
                assertEquals(1f/30, entry.getValue(), 1e-6);
            } else {
                assertEquals(2f/30, entry.getValue(), 1e-6);
            }
        }
        for (Double weight : paths.values()) {
            totalWeight += weight;
        }
        assertEquals(1.0, totalWeight, 1e-6);
    }

    @Test
    public void testBCC() throws Exception {
        // Create the simulation cell
        Cell Structure = new Cell();
        Atom atom = new Atom(new double[]{0, 0, 0}, 0);
        Structure.addAtom(atom);
        atom = new Atom(new double[]{0.5, 0.5, 0.5}, 0);
        Structure.addAtom(atom);

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(Structure, false);

        // Write output
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/bcc.edges");
        }

        // Evaluate results
        assertEquals(Structure.nAtoms(), result.size());
        for (BaseVoronoiCell cell : result) {
            assertTrue(cell.geometryIsValid());
            assertEquals(14, cell.getFaces().size());
            assertEquals(0.5, cell.getVolume(), 1e-6);
            Map<Integer, Integer> polyIndex = cell.getPolyhedronShape();
            assertEquals(Integer.valueOf(8), polyIndex.get(6));
            polyIndex = result.get(0).getCoordinationShellShape(result);
            assertEquals(6, (int) polyIndex.get(4));
            assertEquals(8, (int) polyIndex.get(6));
        }
    }

    @Test
    public void testFCC() throws Exception {
        // Create the simulation cell
        Cell Structure = new Cell();
        Structure.addAtom(new Atom(new double[]{0, 0, 0}, 0));
        Structure.addAtom(new Atom(new double[]{0.5, 0.5, 0}, 0));
        Structure.addAtom(new Atom(new double[]{0.5, 0, 0.5}, 0));
        Structure.addAtom(new Atom(new double[]{0, 0.5, 0.5}, 0));

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(
                Structure, false);

        // Write output
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/fcc.edges");
        }

        // Evaluate results
        assertEquals(Structure.nAtoms(), result.size());
        for (BaseVoronoiCell cell : result) {
            assertTrue(cell.geometryIsValid());
            assertEquals(0.25, cell.getVolume(), 1e-6);
            assertEquals(12, cell.getFaces().size());
            Map<Integer, Integer> polyIndex = cell.getPolyhedronShape();
            assertEquals(Integer.valueOf(12), polyIndex.get(4));
            polyIndex = result.get(0).getCoordinationShellShape(result);
            assertEquals(12, (int) polyIndex.get(4));
        }
    }

    @Test
    public void testFCCPrimitive() throws Exception {
        // Create the simulation cell
        Cell Structure = new Cell();
        Structure.setBasis(new double[]{0.70710678118655, 0.70710678118655, 1.0},
                new double[]{45, 90, 60});
        Atom atom = new Atom(new double[]{0, 0, 0}, 0);
        Structure.addAtom(atom);

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(Structure, false);

        // Write output
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/fcc-prim.edges");
        }

        // Evaluate results
        assertTrue(result.get(0).geometryIsValid());
        assertEquals(Structure.nAtoms(), result.size());
        assertEquals(12, result.get(0).getFaces().size());
        Map<Integer, Integer> polyIndex = result.get(0).getPolyhedronShape();
        assertEquals(Integer.valueOf(12), polyIndex.get(4));
        polyIndex = result.get(0).getCoordinationShellShape(result);
        assertEquals(12, (int) polyIndex.get(4));
    }

    @Test
    public void testTa() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/393-Ta1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/ta.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            double thisVol = cell.getVolume();
            totalVol += thisVol;
            assertTrue(cell.geometryIsValid());
        }
        assertEquals(strc.volume(), totalVol, totalVol * 0.05);
    }

    @Test
    public void testZr() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/1214-Zr1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Zr.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        assertEquals(strc.volume(), totalVol, totalVol * 0.01);
    }

    @Test
    public void testC() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/1004-C1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/C.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        assertEquals(strc.volume(), totalVol, totalVol * 0.01);
    }

    @Test
    public void testC2() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/846-C1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/C2.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testHo() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/592-Ho1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Ho.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
            //assertEquals(12, cell.nFaces());
            //assertEquals(12, (int) cell.getPolyhedronShape().get(4));
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testSi() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/399-Si1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Si.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testLi() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/478-Li1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Li.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testB() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/673-B1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/B.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        assertEquals(strc.volume(), totalVol, totalVol * 0.01);
    }

    @Test
    public void icsdExamples() throws Exception {
        for (String example : new String[]{"3315-Ge2Os2Th1", "1001-N1Y1", "11375-C2N1",
            "12012-Ge2Ru2Tb1", "3778-Sr1Zn2", "4746-Cd1Cu4Er1",}) {
            Cell strc = new VASP5IO().parseFile("test-files/" + example + ".vasp");

            // Run tessellation
            List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

            // Write result
            if (WriteResult) {
                new EdgeWriter().writeToFile(result, "debug/" + example + ".edges");
            }

            // Test
            double totalVol = 0;
            for (BaseVoronoiCell cell : result) {
                totalVol += cell.getVolume();
                assertTrue(cell.geometryIsValid());
            }
            double volError = (totalVol - strc.volume()) / strc.volume();
            assertEquals(0.0, volError, 1e-2);
        }
    }

    @Test
    public void testHg2K() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/7823-Hg2K1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Hg2K1.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testAg2Pr1Si2() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/8379-Ag2Pr1Si2.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/8379-Ag2Pr1Si2.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void testSc() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/1565-Sc1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/Sc.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void bigTest() throws Exception {
        // Number of atoms in each direction
        int nAtom = 10;
        Cell strc = new Cell();
        strc.setBasis(new double[]{2 * nAtom, 2 * nAtom, 2 * nAtom},
                new double[]{90, 90, 90});

        // Add a bunch of atoms
        double stepSize = 1.0 / (double) nAtom;
        for (int x = 0; x < nAtom; x++) {
            for (int y = 0; y < nAtom; y++) {
                for (int z = 0; z < nAtom; z++) {
                    double[] newPos = new double[]{(x + Math.random() / nAtom) * stepSize,
                        (y + Math.random() / nAtom) * stepSize,
                        (z + Math.random() / nAtom) * stepSize};
                    strc.addAtom(new Atom(newPos, 0));
                }
            }
        }

        // Compute the cells
        List<BaseVoronoiCell> cells = VoronoiTessellationCalculator.compute(strc, true);
        double totalVol = 0;
        for (BaseVoronoiCell cell : cells) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);

        if (WriteResult) {
            new EdgeWriter().writeToFile(cells, "debug/big.edges");
        }
    }

    @Test
    public void randomPackingTest() throws Exception {
        // Number of atoms in each direction
        int nAtom = 10;
        Cell strc = new Cell();
        strc.setBasis(new double[]{2 * nAtom, 2 * nAtom, 2 * nAtom},
                new double[]{90, 90, 90});

        // Add a bunch of atoms
        for (int x = 0; x < nAtom; x++) {
            for (int y = 0; y < nAtom; y++) {
                for (int z = 0; z < nAtom; z++) {
                    double[] newPos
                            = new double[]{Math.random(), Math.random(), Math.random()};
                    strc.addAtom(new Atom(newPos, 0));
                }
            }
        }

        // Compute the cells
        List<BaseVoronoiCell> cells = VoronoiTessellationCalculator.compute(strc, true);
        double totalVol = 0;
        for (BaseVoronoiCell cell : cells) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }

    @Test
    public void scalingTest() throws Exception {
        System.out.println("size\ttime\tvol_error");
        for (int nAtom : new int[]{1, 2, 3, 4, 5, 6, 7, 8, 10, 20}) {
            // Number of atoms in each direction
            Cell strc = new Cell();
            strc.setBasis(new double[]{2 * nAtom, 2 * nAtom, 2 * nAtom},
                    new double[]{90, 90, 90});

            // Add a bunch of atoms
            double stepSize = 1.0 / (double) nAtom;
            for (int x = 0; x < nAtom; x++) {
                for (int y = 0; y < nAtom; y++) {
                    for (int z = 0; z < nAtom; z++) {
                        double[] newPos = new double[]{(x + Math.random() / nAtom) * stepSize,
                            (y + Math.random() / nAtom) * stepSize,
                            (z + Math.random() / nAtom) * stepSize};
                        strc.addAtom(new Atom(newPos, 0));
                    }
                }
            }

            // Compute the cells
            long startTime = System.currentTimeMillis();
            List<BaseVoronoiCell> cells = VoronoiTessellationCalculator.compute(strc, true);
            long runTime = System.currentTimeMillis() - startTime;

            double totalVol = 0;
            for (BaseVoronoiCell cell : cells) {
                totalVol += cell.getVolume();
                assertTrue(cell.geometryIsValid());
            }
            double volError = (totalVol - strc.volume()) / strc.volume();
            assertEquals(0.0, volError, 1e-2);
            System.out.format("%d\t%.3f\t%.2f%%\n", strc.nAtoms(),
                    runTime / 1000.0, volError * 100);
        }
    }

    @Test
    public void layeredCompoundTest() throws Exception {
        Cell strc = new VASP5IO().parseFile("test-files/16234-O2Si1.vasp");

        // Run tessellation
        List<BaseVoronoiCell> result = VoronoiTessellationCalculator.compute(strc, false);

        // Write result
        if (WriteResult) {
            new EdgeWriter().writeToFile(result, "debug/16234-O2Si1.edges");
        }

        // Test
        double totalVol = 0;
        for (BaseVoronoiCell cell : result) {
            totalVol += cell.getVolume();
            assertTrue(cell.geometryIsValid());
        }
        double volError = (totalVol - strc.volume()) / strc.volume();
        assertEquals(0.0, volError, 1e-2);
    }
}
