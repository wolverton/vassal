package vassal.analysis.voronoi.voroplusplus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.analysis.voronoi.BaseVoronoiCell;
import vassal.data.Atom;
import vassal.data.AtomImage;
import vassal.data.Cell;

/**
 *
 * @author Logan Ward
 */
public class VoroPlusPlusWrapperTest {

    @Test
    public void testRun() throws Exception {
        Cell strc = makeStructure();
        VoroPlusPlusWrapper.DebugExeSearch = true;

        // Run tessellation
        List<BaseVoronoiCell> output = VoroPlusPlusWrapper.runVoroPlusPlus(strc, true);

        // Check results
        assertEquals(2, output.size());
        assertEquals(14, output.get(0).nFaces());
    }

    @Test
    public void testRunUnaligned() throws Exception {
        // Make the structure
        Cell strc = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0,2,2};
        basis[1] = new double[]{2,0,2};
        basis[2] = new double[]{2,2,0};
        strc.setBasis(basis);
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Run tessellation
        List<BaseVoronoiCell> output = VoroPlusPlusWrapper.runVoroPlusPlus(strc, true);
        assertEquals(1, output.size());
        assertEquals(12, output.get(0).nFaces());
    }

    @Test
    public void testSmallFace() throws Exception {
        // Make an FCC structure with a slight defect
        Cell strc = new Cell();
        strc.setBasis(new double[]{2.88,2.88,2.88}, new double[]{90,90,90});
        strc.addAtom(new Atom(new double[]{0.001,0,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0,0.5}, 0));
        strc.addAtom(new Atom(new double[]{0,0.5,0.5}, 0));

        // Run tessellation
        List<BaseVoronoiCell> output = VoroPlusPlusWrapper.runVoroPlusPlus(strc, true);
        assertEquals(4, output.size());
    }

    @Test
    public void testObliqueCell() throws Exception {
        // Make an small structure that will lead to differences in
        //   atomic position between Vassal and Voro++
        Cell strc = new Cell();
        strc.setBasis(new double[]{2.88,2.88,2.88}, new double[]{60,60,60});
        strc.addAtom(new Atom(new double[]{0.9,0.9,0.9}, 0));

        // Run tessellation
        List<BaseVoronoiCell> output = VoroPlusPlusWrapper.runVoroPlusPlus(strc, true);
    }

    @Test
    public void testWrite() throws Exception {
        Cell strc = makeStructure();

        // Print structure to screen
        VoroPlusPlusWrapper.writeStructure(System.out, strc, true);
    }

    /**
     * Make a sample structure
     * @return BCC structure
     * @throws Exception
     */
    protected Cell makeStructure() throws Exception {
        // Make structure equivalent to test case
        Cell strc = new Cell();
        strc.setBasis(new double[]{2.88,2.88,2.88}, new double[]{90,90,90});
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));
        strc.setTypeRadius(0, 1);
        strc.setTypeRadius(1, 1);
        return strc;
    }

    @Test
    public void testParse() throws Exception {
        Cell strc = makeStructure();

        // Parse the output
        BufferedReader fp = new BufferedReader(new FileReader(new File("voro++", "test.out")));
        VoroPlusPlusWrapper.parseResults(strc, true, fp);
        fp.close();
    }

    @Test
    public void matchPlane() {
        // Make structure
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));
        strc.setTypeRadius(0, 0.2);
        strc.setTypeRadius(1, 0.4);

        // Test the plane between the incell images
        Plane plane = new Plane(new Vector3D(0.25, 0.25, 0.25),
                new Vector3D(1, 1, 1), 1e-6);
        AtomImage image = VoroPlusPlusWrapper.determineImage(strc, strc, false,
                0, 1, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(1, image.getAtomID());
        assertArrayEquals(new int[]{0,0,0}, image.getSupercell());

        plane = new Plane(new Vector3D(0.21, 0.21, 0.21),
                new Vector3D(1, 1, 1), 1e-6);
        image = VoroPlusPlusWrapper.determineImage(strc, strc, true,
                0, 1, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(1, image.getAtomID());
        assertArrayEquals(new int[]{0,0,0}, image.getSupercell());

        // Test a few out of cell images
        plane = new Plane(new Vector3D(-0.21, -0.21, -0.21),
                new Vector3D(-1, -1, -1), 1e-6);
        image = VoroPlusPlusWrapper.determineImage(strc, strc, true,
                0, 1, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(1, image.getAtomID());
        assertArrayEquals(new int[]{-1,-1,-1}, image.getSupercell());

        plane = new Plane(new Vector3D(1, 0.5, 0),
                new Vector3D(2, 1, 0), 1e-6);
        image = VoroPlusPlusWrapper.determineImage(strc, strc, false,
                0, 0, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(0, image.getAtomID());
        assertArrayEquals(new int[]{2,1,0}, image.getSupercell());

        plane = new Plane(new Vector3D(1, -1.5, 3),
                new Vector3D(2, -3, 6), 1e-6);
        image = VoroPlusPlusWrapper.determineImage(strc, strc, false,
                0, 0, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(0, image.getAtomID());
        assertArrayEquals(new int[]{2,-3,6}, image.getSupercell());
    }

    @Test
    public void matchPlaneUnaligned() throws Exception {
        // Make the structure
        Cell strc = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0,2,2};
        basis[1] = new double[]{2,0,2};
        basis[2] = new double[]{2,2,0};
        strc.setBasis(basis);
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Get the aligned basis
        Cell strcAligned = strc.clone();
        strcAligned.setBasis(strc.getAlignedBasis());

        // Convert positions and normals to aligned basis
        double[] normal = strcAligned.convertFractionalToCartestian(new double[]{1,1,1});
        double[] point = strcAligned.convertFractionalToCartestian(new double[]{0.5,0.5,0.5});
        Plane plane = new Plane(new Vector3D(point), new Vector3D(normal), 1e-6);

        // Test ability to match atoms
        AtomImage image = VoroPlusPlusWrapper.determineImage(strc, strcAligned, false,
                0, 0, plane.getPointAt(Vector2D.ZERO, 0), plane.getNormal());
        assertEquals(0, image.getAtomID());
        assertArrayEquals(new int[]{1,1,1}, image.getSupercell());
    }

    @Test
    public void testStepDiv() {
        assertEquals(-2, VoroPlusPlusWrapper.stepDiv(-3, 2), 1e-6);
        assertEquals(-1, VoroPlusPlusWrapper.stepDiv(-2, 2), 1e-6);
        assertEquals(-1, VoroPlusPlusWrapper.stepDiv(-1, 2), 1e-6);
        assertEquals(0, VoroPlusPlusWrapper.stepDiv(0, 2), 1e-6);
        assertEquals(0, VoroPlusPlusWrapper.stepDiv(1, 2), 1e-6);
        assertEquals(1, VoroPlusPlusWrapper.stepDiv(2, 2), 1e-6);
    }

    @Test
    public void testVoroDisplacement() throws Exception {
        // Make a test cell
        Cell strc = new Cell();
        strc.setBasis(new double[]{5.0357,5.0357,5.0357},
                new double[]{58.057,58.057,58.057});
        strc.addAtom(new Atom(new double[]{0.989802065,0.989801861,0.346005871}, 0));

        // Get the difference between Voro++ atom coordinate and the Cartesian
        //   coordinates of each atom
        Vector3D disp = VoroPlusPlusWrapper.getVoroPlusPlusPositionDelta(strc, 0);
        assertTrue(0 < disp.getNorm());

        // Check that the coordinates line up
        Vector3D expectedPos = new Vector3D(0.843329, 0.468013, 1.387216);
        Vector3D actualPos = new Vector3D(strc.getAtom(0).getPositionCartesian())
                .add(disp);
        assertEquals(0, actualPos.distance(expectedPos), 1e-5);
    }
}
