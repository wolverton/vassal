package vassal.analysis.voronoi.java;

import org.junit.Test;
import java.util.*;
import static org.junit.Assert.*;
import vassal.analysis.voronoi.BaseVoronoiEdge;
import vassal.data.Atom;
import vassal.data.Cell;
import vassal.data.AtomImage;

/**
 *
 * @author Logan Ward
 */
public class VoronoiEdgeTest {

    @Test
    public void testInitialize() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edge
        VoronoiEdge edge = new VoronoiEdge(face1, face2);

        // Test properties
        assertArrayEquals(new double[]{-1,0,0},
                edge.getLine().getDirection().toArray(), 1e-6);
    }

    @Test
    public void testIsNext() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge1 = new VoronoiEdge(face1, face2);
        VoronoiEdge edge2 = new VoronoiEdge(face1, face3);

        // Compute proper ordering
        assertTrue(edge2.isCCW(edge1));
        assertFalse(edge1.isCCW(edge2));
        assertFalse(edge2.isCCW(edge2));
    }

    @Test
    public void testSimpleIntersection() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{2,0,0});
        VoronoiFace face4 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{-1,0,0});
        VoronoiFace face5 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge1 = new VoronoiEdge(face1, face2);
        VoronoiEdge edge2 = new VoronoiEdge(face1, face3);
        VoronoiEdge edge3 = new VoronoiEdge(face1, face4);
        VoronoiEdge edge4 = new VoronoiEdge(face1, face5);

        // Compute intersection
        assertTrue(VoronoiEdge.computeIntersection(edge1, edge3));
        assertTrue(VoronoiEdge.computeIntersection(edge1, edge2));
        assertTrue(VoronoiEdge.computeIntersection(edge1, edge4));

        // Test properties
        assertArrayEquals(new double[]{0.5,0.5,0.5},
                edge1.getStartVertex().getPosition().toArray(), 1e-6);
        assertArrayEquals(new double[]{0.5,0.5,0.5},
                edge2.getEndVertex().getPosition().toArray(), 1e-6);
        assertEquals(1.0, edge1.getLength(), 1e-6);

        // Compute intersection that shouldn't happen
        assertFalse(VoronoiEdge.computeIntersection(edge1, edge3));
    }

        @Test
    public void testJoin() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{2,0,0});
        VoronoiFace face4 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge1 = new VoronoiEdge(face1, face2);
        VoronoiEdge edge2 = new VoronoiEdge(face1, face3);
        VoronoiEdge edge3 = new VoronoiEdge(face1, face4);

        // Join edge1 & edge2
        VoronoiEdge.joinEdges(edge1, edge2);
        assertArrayEquals(new double[]{0.5,0.5,0.5},
                edge1.getStartVertex().getPosition().toArray(), 1e-6);
        assertEquals(edge2, edge1.getPreviousEdge());

        // Join edge2 & edge3
        VoronoiEdge.joinEdges(edge1, edge3);
        assertEquals(edge3, edge1.getPreviousEdge());
        assertArrayEquals(new double[]{1.0,0.5,0.5},
                edge1.getStartVertex().getPosition().toArray(), 1e-6);
    }


    @Test
    public void testCompare() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge1 = new VoronoiEdge(face1, face2);
        VoronoiEdge edge2 = new VoronoiEdge(face1, face3);

        // Tests
        assertEquals(0, edge1.compareTo(edge1));
        assertTrue(edge1.compareTo(edge2) != 0);
        assertTrue(edge1.compareTo(edge2) == -1 * edge2.compareTo(edge1));
        assertFalse(edge1.equals(edge2));
        assertTrue(edge1.equals(edge1));
    }

    @Test
    public void testNextEdge() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{-1,1,0});
        VoronoiFace face3 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{-1,0,0});
        VoronoiFace face4 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edges
        VoronoiEdge edge = new VoronoiEdge(face1, face2);
        VoronoiEdge edgea = new VoronoiEdge(face1, face3);
        VoronoiEdge edgeb = new VoronoiEdge(face1, face4);

        // Verify that edgea & edgeb intersect edge1 in the same place
        assertArrayEquals(edge.getLine().intersection(edgea.getLine()).toArray(),
                edge.getLine().intersection(edgeb.getLine()).toArray(), 1e-6);

        // Verify that when tasked with distinguishbing betwen the two, it
        //  recognizes that edgeb is the correct choice
        List<BaseVoronoiEdge> choices = new LinkedList<>();
        choices.add(edgea);
        choices.add(edgeb);
        assertTrue(edge.isCCW(edgea));
        assertEquals(edgeb, edge.findNextEdge(choices));

        // Add more confilicting choices
        image = new AtomImage(cell.getAtom(0), new int[]{-2,0,0});
        face4 = new VoronoiFace(cell.getAtom(0), image, true);
        choices.add(new VoronoiEdge(face1, face4)); // A farther edge
        image = new AtomImage(cell.getAtom(0), new int[]{1,0,0});
        face4 = new VoronoiFace(cell.getAtom(0), image, true);
        choices.add(new VoronoiEdge(face1, face4)); // A CW edge
        assertEquals(edgeb, edge.findNextEdge(choices));
    }

    @Test
    public void testPair() throws Exception {
        // Make a simple crystal
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));

        // Initialize faces
        AtomImage image = new AtomImage(cell.getAtom(0), new int[]{0,0,1});
        VoronoiFace face1 = new VoronoiFace(cell.getAtom(0), image, true);
        image = new AtomImage(cell.getAtom(0), new int[]{0,1,0});
        VoronoiFace face2 = new VoronoiFace(cell.getAtom(0), image, true);

        // Create edge
        VoronoiEdge edge = new VoronoiEdge(face1, face2);

        // Create pair
        VoronoiEdge pair = edge.generatePair();

        // Tests
        assertEquals(edge.getEdgeFace(), pair.getIntersectingFace());
        assertEquals(edge.getIntersectingFace(), pair.getEdgeFace());
        assertEquals(edge.getLine().getDirection(),
                pair.getLine().revert().getDirection());
    }
}
