package vassal.analysis.toolkits;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.data.*;

/**
 *
 * @author Logan Ward
 */
public class CoordinationPolyhedronToolkitTest {

    @Test
    public void test() throws Exception {
        // Create test subject
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{  0,  0,  0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 0));

        // Test printing the type of each atom
        CoordinationPolyhedronToolkit tool = new CoordinationPolyhedronToolkit();
        tool.setPrintCellStatistics(false);
        String output = tool.analyzeStructure(strc);
        assertEquals(3, output.split("\n").length);

        // Testing cell statistics
        tool.setPrintCellStatistics(true);
        output = tool.analyzeStructure(strc);
        assertEquals(2, output.split("\n").length);
    }

    @Test
    public void testOptions() throws Exception {
        // Test atom stats
        List<String> options = new ArrayList<>();
        options.add("atom");

        CoordinationPolyhedronToolkit tool = new CoordinationPolyhedronToolkit();
        tool.setOptions(options);

        assertFalse(tool.PrintCellStatistics);
        assertFalse(tool.ComputeRadical);
        assertEquals(0, tool.Radii.size());

        // Test cell/atom stats
        options.clear();
        options.add("cell");

        tool.setOptions(options);

        assertTrue(tool.PrintCellStatistics);
        assertFalse(tool.ComputeRadical);
        assertEquals(0, tool.Radii.size());

        // Test radii, no radical
        options.clear();
        options.add("cell");
        options.add("-radii");
        options.add("Fe");
        options.add("2.0");
        options.add("*");
        options.add("1.0");

        tool.setOptions(options);

        assertTrue(tool.PrintCellStatistics);
        assertFalse(tool.ComputeRadical);
        assertEquals(2, tool.Radii.size());
        assertEquals(2, tool.Radii.get("Fe"), 1e-6);
        assertEquals(1.0, tool.Radii.get("*"), 1e-6);

        // Test radii, yes radical
        options.clear();
        options.add("cell");
        options.add("-radical");
        options.add("-radii");
        options.add("Ni");
        options.add("2.0");
        options.add("Li");
        options.add("3.0");
        options.add("*");
        options.add("1.0");

        tool.setOptions(options);

        assertTrue(tool.PrintCellStatistics);
        assertTrue(tool.ComputeRadical);
        assertEquals(3, tool.Radii.size());
        assertEquals(2.0, tool.Radii.get("Ni"), 1e-6);
        assertEquals(3.0, tool.Radii.get("Li"), 1e-6);
        assertEquals(1.0, tool.Radii.get("*"), 1e-6);
    }

    @Test
    public void testRadical() throws Exception {
        // Structure of rocksalt
        Cell strc = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0.0,0.5,0.5};
        basis[1] = new double[]{0.5,0.0,0.5};
        basis[2] = new double[]{0.5,0.5,0.0};
        strc.setBasis(basis);
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        strc.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 1);
        strc.addAtom(atom);

        strc.setTypeName(0, "Na");
        strc.setTypeName(1, "Cl");

        // Create analysis tool, define settings
        CoordinationPolyhedronToolkit tool = new CoordinationPolyhedronToolkit();
        tool.addRadius("Na", 0.2);
        tool.addRadius("Cl", 0.3);
        tool.setComputeRadicalTessellation(true);
        tool.setPrintCellStatistics(false);

        // Run the analysis
        String output = tool.analyzeStructure(strc);
        System.out.println(output);

    }
}
