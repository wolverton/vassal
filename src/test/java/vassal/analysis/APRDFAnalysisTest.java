package vassal.analysis;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import vassal.data.Atom;
import vassal.data.Cell;

/**
 *
 * @author Logan Ward
 */
public class APRDFAnalysisTest {

    @Test
    public void testEvalDistance() throws Exception {
        // Make the analysis tool
        APRDFAnalysis tool = new APRDFAnalysis();

        // Define the settings
        tool.setNWindows(3);
        tool.setCutoffDistance(3.0);

        // Test result
        assertArrayEquals(new double[]{1,2,3}, tool.getEvaluationDistances(), 1e-6);
    }

    @Test
    public void testAPRDF() throws Exception {
        // Create a B2 structure with lattice parameter of 1
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));

        // Create the analysis tool
        APRDFAnalysis tool = new APRDFAnalysis();

        tool.setCutoffDistance(1.0);
        tool.setNWindows(2);
        tool.setSmoothingFactor(100);

        tool.analyzeStructure(strc);

        // Trivial: Properties == 0
        double[] apRDF = tool.computeAPRDF(new double[]{0,0});
        assertArrayEquals(new double[]{0.5,1}, tool.getEvaluationDistances(), 1e-6);
        assertArrayEquals(new double[]{0,0}, apRDF, 1e-6);

        // Actual case
        apRDF = tool.computeAPRDF(new double[]{1,2});

        //   Assemble known contributors
        //     [0] -> Number of neighbors * P_i * P_j
        //     [1] -> Bond distance
        List<double[]> contrs = new ArrayList<>();
        contrs.add(new double[]{2*8*2*1, Math.sqrt(3)/2}); // A-B 1st NN
        contrs.add(new double[]{6*1*1, 1}); // A-A 2nd NN
        contrs.add(new double[]{6*2*2, 1}); // B-B 2nd NN
        contrs.add(new double[]{8*1*1, Math.sqrt(3)}); // A-A 3rd NN
        contrs.add(new double[]{8*2*2, Math.sqrt(3)}); // B-B 3rd NN

        //    Compute expected values
        double[] evalDist = new double[]{0.5,1};
        double[] expectedAPRDF = new double[]{0,0};
        for (int r=0; r<evalDist.length; r++) {
            for (double[] contr : contrs) {
                expectedAPRDF[r] += contr[0] * Math.exp(-100 * Math.pow(contr[1] - evalDist[r],2));
            }
        }

        //      Normalize
        for (int r=0; r<expectedAPRDF.length; r++) {
            expectedAPRDF[r] /= 2;
        }

        assertArrayEquals(expectedAPRDF, apRDF, 1e-3);
    }

    /**
     * Make sure this property is not affected by unit cell choice
     * @throws Exception
     */
    @Test
    public void testUnitCellChoice() throws Exception {
        // Create a B2 structure with lattice parameter of 1
        Cell strc = new Cell();
        strc.addAtom(new Atom(new double[]{0,0,0}, 0));
        strc.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));

        // Create a 2x1x1 supercell
        Cell supercell = new Cell();
        supercell.setBasis(new double[]{2,1,1}, new double[]{90,90,90});
        supercell.addAtom(new Atom(new double[]{0,0,0}, 0));
        supercell.addAtom(new Atom(new double[]{0.5,0,0}, 0));
        supercell.addAtom(new Atom(new double[]{0.25,0.5,0.5}, 1));
        supercell.addAtom(new Atom(new double[]{0.75,0.5,0.5}, 1));

        // Make the PRDF tool
        APRDFAnalysis tool = new APRDFAnalysis();

        tool.setCutoffDistance(3.0);
        tool.setNWindows(10);
        tool.setSmoothingFactor(4);

        // Compute the primitive cell APRDF
        tool.analyzeStructure(strc);
        double[] strcRDF = tool.computeAPRDF(new double[]{1,2});

        // Compute the supercell APRDF
        tool.analyzeStructure(supercell);
        double[] superRDF = tool.computeAPRDF(new double[]{1,2});

        // Compare results
        assertArrayEquals(strcRDF, superRDF, 1e-6);

    }
}
