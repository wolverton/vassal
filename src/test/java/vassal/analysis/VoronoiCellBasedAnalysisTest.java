package vassal.analysis;

import vassal.data.Atom;
import vassal.data.Cell;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 * @author Logan Ward
 */
public class VoronoiCellBasedAnalysisTest {

    @Test
    public void testBCC() throws Exception {
        // Structure of BCC
        Cell Structure = new Cell();
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        Structure.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 0);
		Structure.addAtom(atom);

        // Prepare
        VoronoiCellBasedAnalysis tool = new VoronoiCellBasedAnalysis(true);
        tool.analyzeStructure(Structure);

        // Check results
        double N_eff = 11.95692194;
        assertArrayEquals(new double[]{N_eff, N_eff}, tool.getEffectiveCoordinationNumbers(), 1e-6);
        assertEquals(14.0, tool.faceCountAverage(), 1e-2);
        assertEquals(0.0, tool.faceCountVariance(), 1e-2);
        assertEquals(14.0, tool.faceCountMinimum(), 1e-2);
        assertEquals(14.0, tool.faceCountMaximum(), 1e-2);
        assertEquals(1, tool.getUniquePolyhedronShapes().size(), 1e-2);
        assertEquals(0, tool.volumeVariance(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMinimum(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMaximum(), 1e-2);
		assertEquals(0.68, tool.maxPackingEfficiency(), 1e-2);
        assertEquals(0, tool.meanBCCDissimilarity(), 1e-2);
        assertEquals(14f / 12f, tool.meanFCCDissimilarity(), 1e-2);
        assertEquals(8f / 6f, tool.meanSCDissimilarity(), 1e-2);
        double[][] bondLengths = tool.bondLengths();
        assertEquals(2, bondLengths.length);
        assertEquals(14, bondLengths[0].length);
        assertEquals(Math.sqrt(3)/2, bondLengths[0][0], 1e-6);
        assertEquals(1.0, bondLengths[0][12], 1e-6);
        double[] meanBondLengths = tool.meanBondLengths();
        double[] varBondLengths = tool.bondLengthVariance(meanBondLengths);
        assertTrue(varBondLengths[0] > 0);
    }

    @Test
    public void testB2() throws Exception {
        // Structure of BCC
        Cell Structure = new Cell();
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        Structure.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 1);
		Structure.addAtom(atom);

        // Prepare
        VoronoiCellBasedAnalysis tool = new VoronoiCellBasedAnalysis(true);
        tool.analyzeStructure(Structure);

        // Check results
        double N_eff = 11.95692194;
        assertArrayEquals(new double[]{N_eff, N_eff}, tool.getEffectiveCoordinationNumbers(), 1e-6);
        assertEquals(14.0, tool.faceCountAverage(), 1e-2);
        assertEquals(0.0, tool.faceCountVariance(), 1e-2);
        assertEquals(14.0, tool.faceCountMinimum(), 1e-2);
        assertEquals(14.0, tool.faceCountMaximum(), 1e-2);
        assertEquals(1, tool.getUniquePolyhedronShapes().size(), 1e-2);
        assertEquals(0, tool.volumeVariance(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMinimum(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMaximum(), 1e-2);
		assertEquals(0.68, tool.maxPackingEfficiency(), 1e-2);
        assertEquals(0, tool.meanBCCDissimilarity(), 1e-2);
        assertEquals(14f / 12f, tool.meanFCCDissimilarity(), 1e-2);
        assertEquals(8f / 6f, tool.meanSCDissimilarity(), 1e-2);
        double[][] bondLengths = tool.bondLengths();
        assertEquals(2, bondLengths.length);
        assertEquals(14, bondLengths[0].length);
        assertEquals(Math.sqrt(3)/2, bondLengths[0][0], 1e-6);
        assertEquals(1.0, bondLengths[0][12], 1e-6);
        double[] meanBondLengths = tool.meanBondLengths();
        double[] varBondLengths = tool.bondLengthVariance(meanBondLengths);
        assertTrue(varBondLengths[0] > 0);

        // Check ordering parameters (against values computed by hand)
        assertArrayEquals(new double[]{0.142857,-0.142857},
                tool.getNeighborOrderingParameters(1, false)[0],
                1e-2);
        assertArrayEquals(new double[]{-0.04,0.04},
                tool.getNeighborOrderingParameters(2, false)[0],
                1e-2);
        assertArrayEquals(new double[]{0.551982,-0.551982},
                tool.getNeighborOrderingParameters(1, true)[0],
                1e-2);
        assertArrayEquals(new double[]{-0.253856,0.253856},
                tool.getNeighborOrderingParameters(2, true)[0],
                1e-2);
        assertEquals(0.142857, tool.warrenCowleyOrderingMagnituide(1, false), 1e-2);
        assertEquals(0.04, tool.warrenCowleyOrderingMagnituide(2, false), 1e-2);
        assertEquals(0.551982, tool.warrenCowleyOrderingMagnituide(1, true), 1e-2);
        assertEquals(0.253856, tool.warrenCowleyOrderingMagnituide(2, true), 1e-2);
    }

    @Test
    public void testB1() throws Exception {
        // Structure of rocksalt
        Cell Structure = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0.0,0.5,0.5};
        basis[1] = new double[]{0.5,0.0,0.5};
        basis[2] = new double[]{0.5,0.5,0.0};
        Structure.setBasis(basis);
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        Structure.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 1);
        Structure.addAtom(atom);

        // Prepare
        VoronoiCellBasedAnalysis tool = new VoronoiCellBasedAnalysis(true);
        tool.analyzeStructure(Structure);

        // Check results
        assertArrayEquals(new double[]{6,6}, tool.getEffectiveCoordinationNumbers(), 1e-6);
        assertEquals(6.0, tool.faceCountAverage(), 1e-2);
        assertEquals(0.0, tool.faceCountVariance(), 1e-2);
        assertEquals(6.0, tool.faceCountMinimum(), 1e-2);
        assertEquals(6.0, tool.faceCountMaximum(), 1e-2);
        assertEquals(1, tool.getUniquePolyhedronShapes().size(), 1e-2);
        assertEquals(0, tool.volumeVariance(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMinimum(), 1e-2);
        assertEquals(0.5, tool.volumeFractionMaximum(), 1e-2);
        assertArrayEquals(new double[]{1,-1},
                tool.getNeighborOrderingParameters(1, false)[0],
                1e-2);
        assertArrayEquals(new double[]{-1,1},
                tool.getNeighborOrderingParameters(2, false)[0],
                1e-2);
        assertArrayEquals(new double[]{1,-1},
                tool.getNeighborOrderingParameters(3, false)[0],
                1e-2);
        assertArrayEquals(new double[]{1,-1},
                tool.getNeighborOrderingParameters(1, true)[0],
                1e-2);
        assertArrayEquals(new double[]{-1,1},
                tool.getNeighborOrderingParameters(2, true)[0],
                1e-2);
        assertArrayEquals(new double[]{1,-1},
                tool.getNeighborOrderingParameters(3, true)[0],
                1e-2);
        assertEquals(1, tool.warrenCowleyOrderingMagnituide(1, false), 1e-2);
        assertEquals(1, tool.warrenCowleyOrderingMagnituide(2, false), 1e-2);
        assertEquals(1, tool.warrenCowleyOrderingMagnituide(1, true), 1e-2);
        assertEquals(1, tool.warrenCowleyOrderingMagnituide(2, true), 1e-2);
        double[][] bondLengths = tool.bondLengths();
        assertEquals(2, bondLengths.length);
        assertEquals(6, bondLengths[0].length);
        assertEquals(0.5, bondLengths[0][0], 1e-6);
        double[] meanBondLengths = tool.meanBondLengths();
        assertEquals(2, meanBondLengths.length);
        assertEquals(0.5, meanBondLengths[0], 1e-6);
        double[] varBondLengths = tool.bondLengthVariance(meanBondLengths);
        assertEquals(0, varBondLengths[0], 1e-6);

        // Neighbor property attributes
        assertArrayEquals(new double[]{1,1}, tool.neighborPropertyDifferences(new double[]{0,1}, 1), 1e-6);
        assertArrayEquals(new double[]{0,0}, tool.neighborPropertyDifferences(new double[]{0,1}, 2), 1e-6);
        assertArrayEquals(new double[]{0,0}, tool.neighborPropertyVariances(new double[]{0,1}, 1), 1e-6);
        assertArrayEquals(new double[]{0,0}, tool.neighborPropertyVariances(new double[]{0,1}, 2), 1e-6);
    }

    @Test
    public void testL12() throws Exception {
        // Structure of  L12
        Cell Structure = new Cell();
		Atom atom = new Atom(new double[]{0,0,0}, 1);
        Structure.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.}, 0);
		Structure.addAtom(atom);
        atom = new Atom(new double[]{0.5,0.,0.5}, 0);
		Structure.addAtom(atom);
        atom = new Atom(new double[]{0.,0.5,0.5}, 0);
		Structure.addAtom(atom);

        // Prepare
        VoronoiCellBasedAnalysis tool = new VoronoiCellBasedAnalysis(true);
        tool.analyzeStructure(Structure);

        // Check results
        assertArrayEquals(new double[]{12,12,12,12}, tool.getEffectiveCoordinationNumbers(), 1e-6);
        assertEquals(12.0, tool.faceCountAverage(), 1e-2);
        assertEquals(0.0, tool.faceCountVariance(), 1e-2);
        assertEquals(12.0, tool.faceCountMinimum(), 1e-2);
        assertEquals(12.0, tool.faceCountMaximum(), 1e-2);
        assertEquals(1, tool.getUniquePolyhedronShapes().size(), 1e-2);
        assertEquals(0, tool.volumeVariance(), 1e-2);
        assertEquals(0.25, tool.volumeFractionMinimum(), 1e-2);
        assertEquals(0.25, tool.volumeFractionMaximum(), 1e-2);
		assertEquals(0.74, tool.maxPackingEfficiency(), 1e-2);
        assertEquals(14 / 14f, tool.meanBCCDissimilarity(), 1e-2);
        assertEquals(0.0, tool.meanFCCDissimilarity(), 1e-2);
        assertEquals(6f / 6f, tool.meanSCDissimilarity(), 1e-2);
        double[][] bondLengths = tool.bondLengths();
        assertEquals(4, bondLengths.length);
        assertEquals(12, bondLengths[0].length);
        assertEquals(Math.sqrt(2)/2, bondLengths[0][0], 1e-6);
        double[] meanBondLengths = tool.meanBondLengths();
        double[] varBondLengths = tool.bondLengthVariance(meanBondLengths);
        assertEquals(0, varBondLengths[0], 1e-6);

        // Neighbor analysis resutls
        double[] neighDiff = tool.neighborPropertyDifferences(new double[]{-1,1}, 1);
        assertArrayEquals(new double[]{2,8f/12,2f/3,2f/3}, neighDiff, 1e-5);

        neighDiff = tool.neighborPropertyDifferences(new double[]{-1,1}, 2);
        assertArrayEquals(new double[]{192f/132,64f/132,64f/132,64f/132}, neighDiff, 1e-5);

        double[] neighVar = tool.neighborPropertyVariances(new double[]{-1,1}, 1);
        // Type 0 has 8 type 0 NNs and 4 type 1, mean = -1/3
        // Type 1 has 12 type 0 NNs, therefore variance = 0
        // variance = 8/12 * (-1 + 1/3)^2 + 4/12 + (1 + 1/3)^2
        double aVar = 2f/3 * (-1 + 1f/3) * (-1 + 1f/3) + 1f/3 * (1 + 1f/3) * (1 + 1f/3);
        assertArrayEquals(new double[]{0,aVar,aVar,aVar}, neighVar, 1e-5);

        neighVar = tool.neighborPropertyVariances(new double[]{-1,1}, 2);
        // Type 0 has 68 type 0 2nd NN and 16 type 1
        // Type 1 has 48 type 0 2nd NN and 36 type 1
        double type0Var = 0.734618916;

        double type1Var = 0.79338843;
        assertArrayEquals(new double[]{type1Var, type0Var, type0Var, type0Var},
                neighVar, 1e-5);
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Test
    public void testConstructor() throws Exception {
        // Create a Voronoi tessellation of a BCC crystal
        Cell bcc = new Cell();
		Atom atom = new Atom(new double[]{0,0,0}, 0);
        bcc.addAtom(atom);
		atom = new Atom(new double[]{0.5,0.5,0.5}, 0);
		bcc.addAtom(atom);
        VoronoiCellBasedAnalysis tool = new VoronoiCellBasedAnalysis(true);
        tool.analyzeStructure(bcc);

        // Create a new instance based on a B2 crystal
        Cell b2 = bcc.clone();
        b2.getAtom(1).setType(1);
        VoronoiCellBasedAnalysis tool2 = new VoronoiCellBasedAnalysis(tool, b2);

        // Make sure it balks if I change the basis
        b2.setBasis(new double[]{2,1,1}, new double[]{90,90,90});
        exception.expect(Exception.class);
        tool2 = new VoronoiCellBasedAnalysis(tool, b2);
    }
}
