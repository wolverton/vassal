package vassal.io;

import org.junit.Test;
import vassal.data.Atom;
import vassal.data.Cell;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * @author Logan Ward
 */
public class VASP5IOTest {

	/**
	 * Test of convertStructureToString method, of class VASP5Writer.
	 */
	@Test
	public void testConversion() throws Exception {
		// Make an FCC Cell
		Cell cell = new Cell();
		cell.setBasis(new double[]{3.5,3.6,3.4}, new double[]{89,90,91});
		cell.addAtom(new Atom(new double[]{  0,  0,  0}, 0));
		cell.addAtom(new Atom(new double[]{0.5,0.5,  0}, 1));
		cell.addAtom(new Atom(new double[]{0.5,  0,0.5}, 1));
		cell.addAtom(new Atom(new double[]{  0,0.5,0.5}, 1));
		cell.setTypeName(0, "Al");
		cell.setTypeName(1, "Ni");

		// Convert it to string
		List<String> temp = new VASP5IO().convertStructureToString(cell);

		// Convert it back
		Cell newCell = new VASP5IO().parseStructure(temp);

		// Check to make sure everything is good
		assertEquals(cell.volume(), newCell.volume(), 1e-4);
		assertEquals(cell.nTypes(), newCell.nTypes());
		assertArrayEquals(cell.getLatticeVectors()[1].toArray(),
				newCell.getLatticeVectors()[1].toArray(), 1e-4);
		List<String> newTemp =  new VASP5IO().convertStructureToString(newCell);
		assertArrayEquals(newTemp.toArray(), temp.toArray());
	}

    @Test
    public void readFromString() throws Exception {
        String contents = new String(Files.readAllBytes(Paths.get("test-files/393-Ta1.vasp")));

        Cell cell = new VASP5IO().parseStructure(contents);
        assertEquals(556.549, cell.volume(), 1e-2);
    }

	@Test
	public void readTa() throws Exception {
		Cell cell = new VASP5IO().parseFile("test-files/393-Ta1.vasp");

		// Tests
		assertEquals(556.549, cell.volume(), 1e-2);
		assertEquals(10.218, cell.getLatticeVectors()[0].getX(), 1e-2);
		assertEquals(30, cell.nAtoms());
		assertArrayEquals(new double[]{0.681,0.818,0.998},
				cell.getAtom(29).getPosition(), 1e-2);
	}
}
