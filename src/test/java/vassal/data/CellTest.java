package vassal.data;

import java.util.*;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Logan Ward
 */
public class CellTest {

    @Test
    public void testSetBasis() throws Exception{
        // Test using angles and lattice parameters as input
        Cell cell = new Cell();
        cell.setBasis(new double[]{5.643, 6.621,4.885},
                new double[]{91.83, 93.58, 107.69});
        assertEquals(173.30, cell.volume(), 1e-2);
        assertArrayEquals(new double[]{5.643,6.621,4.885}, cell.getLatticeParameters(), 1e-6);
        assertArrayEquals(new double[]{91.83,93.58,107.69}, cell.getLatticeAngles(), 1e-6);

        // Simple test with a primitive cell
        double[][] basis = new double[3][];
        basis[0] = new double[]{0,2.986,2.986};
        basis[1] = new double[]{2.986,0,2.986};
        basis[2] = new double[]{2.986,2.986,0};
        cell.setBasis(basis);
        assertEquals(13.312*4, cell.volume(), 1e-3);
        assertArrayEquals(new double[]{4.223,4.223,4.223}, cell.getLatticeParameters(), 1e-3);
        assertArrayEquals(new double[]{60,60,60}, cell.getLatticeAngles(), 1e-6);
    }

    @Test
    public void testAlignedBasis() throws Exception {
        // Create sample primitive cell
        Cell cell = new Cell();
        double[][] basis = new double[3][];
        basis[0] = new double[]{0,2.986,2.986};
        basis[1] = new double[]{2.986,0,2.986};
        basis[2] = new double[]{2.986,2.986,0};
        cell.setBasis(basis);

        // Compute the aligned basis
        double[][] alignedBasis = cell.getAlignedBasis();
        assertEquals(0, alignedBasis[1][0], 1e-6);
        assertEquals(0, alignedBasis[2][0], 1e-6);
        assertEquals(0, alignedBasis[2][1], 1e-6);
    }

    @Test
    public void testClone() throws Exception {
        Cell cellA = new Cell();
        cellA.addAtom(new Atom(new double[]{0,0,0}, 0));
        cellA.setTypeName(0, "A");

        // Test adding atoms
        Cell cellB = cellA.clone();
        assertEquals(cellB, cellA);
        cellB.addAtom(new Atom(new double[]{0,0.5,0}, 0));
        assertFalse(cellB.equals(cellA));

        // Test changing atom
        cellB = cellA.clone();
        cellB.getAtom(0).setType(1);
        assertFalse(cellB.equals(cellA));

        // Test changing basis
        cellB = cellA.clone();
        cellB.setBasis(new double[]{2,1,1}, new double[]{90,90,90});
        assertFalse(cellB.equals(cellA));
    }

	@Test
    public void testLatticeVectors() throws Exception{
        Cell cell = new Cell();
        cell.setBasis(new double[]{1, 2, 3},
                new double[]{80, 90, 70});
		Vector3D[] vectors = cell.getLatticeVectors();
        assertArrayEquals(new double[]{1.0, 0.0, 0.0},
				vectors[0].toArray(),
				1e-3);
		assertArrayEquals(new double[]{0.684, 1.879, 0.0},
				vectors[1].toArray(),
				1e-3);
		assertArrayEquals(new double[]{0.0, 0.554, 2.948},
				vectors[2].toArray(),
				1e-3);

		// FCC Primitive Cell
		cell.setBasis(new double[]{0.70710678118655, 0.70710678118655, 0.70710678118655},
				new double[]{60, 60, 60});
		assertEquals(0.25, cell.volume(), 1e-6);
		vectors = cell.getLatticeVectors();
        assertEquals(0.70710678118655, vectors[0].getNorm(), 1e-2);
    }

	@Test
    public void testFractionalToCartestian() throws Exception {
        Cell cell = new Cell();
        cell.setBasis(new double[]{1, 2, 3},
                new double[]{80, 90, 70});
        assertArrayEquals(new double[]{0.2368, 0.5421, 0.8844},
				cell.convertFractionalToCartestian(new double[]{0.1, 0.2, 0.3}),
				1e-3);
    }

	@Test
    public void testCartesianToFractional() throws Exception {
        Cell cell = new Cell();
        cell.setBasis(new double[]{1, 2, 3},
                new double[]{80, 90, 70});
        assertArrayEquals(new double[]{0.1, 0.2, 0.3},
				cell.convertCartesianToFractional(new double[]{0.2368, 0.5421, 0.8844}),
				1e-3);
    }

	@Test
	public void testSuperCellTranslation() throws Exception {
		Cell cell = new Cell();
		cell.setBasis(new double[]{0.70710678118655, 0.70710678118655, 0.70710678118655},
				new double[]{60, 60, 60});
		assertEquals(0.25, cell.volume(), 1e-6);
		Vector3D[] vectors = cell.getLatticeVectors();

		// Check a few
        double[] pos = cell.getPeriodicImage(new double[]{0,0,0}, 1, 0, 0);
		assertArrayEquals(new double[]{0.70710678000000, 0, 0}, pos, 1e-3);
		pos = cell.getPeriodicImage(new double[]{0,0,0}, 1, 1, 0);
		assertArrayEquals(new double[]{1.06066017000000,
			0.61237243466821, 0}, pos, 1e-3);
		pos = cell.getPeriodicImage(new double[]{0,0,0}, 1, 1, 1);
		assertArrayEquals(new double[]{1.41421356000000,
			0.81649657955762, 0.57735026918963}, pos, 1e-3);
	}

    @Test
    public void testEquals() throws Exception {
        // Make two cells
        Cell cell1 = new Cell(), cell2 = new Cell();

        // First check
        assertTrue(cell1.equals(cell2));

        // Adjust basis
        cell1.setBasis(new double[]{1,2,3}, new double[]{70,80,90});
        assertFalse(cell1.equals(cell2));
        cell2.setBasis(new double[]{1,2,3}, new double[]{70,80,90});
        assertTrue(cell1.equals(cell2));

        // Add an atom to 0,0,0
        cell1.addAtom(new Atom(new double[]{0,0,0}, 0));
        assertFalse(cell1.equals(cell2));
        cell2.addAtom(new Atom(new double[]{0,0,0}, 0));
        assertTrue(cell1.equals(cell2));

        // Changing names
        cell1.setTypeName(0, "Al");
        assertFalse(cell1.equals(cell2));
        cell2.setTypeName(0, "Al");
        assertTrue(cell1.equals(cell2));

        // Adding more atoms of a different type
        cell1.addAtom(new Atom(new double[]{0.5,0.5,0}, 1));
        cell2.addAtom(new Atom(new double[]{0.5,0.5,0}, 0));
        assertFalse(cell1.equals(cell2));
        cell2.getAtom(1).setType(1);
        assertTrue(cell1.equals(cell2));

        // Adding atoms with different positions
        cell1.addAtom(new Atom(new double[]{0.5,0,0.5}, 1));
        cell2.addAtom(new Atom(new double[]{0,0.5,0.5}, 1));
        assertFalse(cell1.equals(cell2));

        // Adding atoms out of sequence
        cell2.addAtom(new Atom(new double[]{0.5,0,0.5}, 1));
        cell1.addAtom(new Atom(new double[]{0,0.5,0.5}, 1));
        assertTrue(cell1.equals(cell2));
    }

    @Test
    public void testClosestImage() throws Exception {
        // Simple case: Orthogonal axes
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 1)); // Origin
        cell.addAtom(new Atom(new double[]{0.5,0.5,0}, 1)); // C face center
        double dist = cell.getMinimumDistance(new double[]{0,0,0}, new double[]{0.5,0.5,0});
        assertEquals(Math.sqrt(0.5), dist, 1e-6);
        dist = cell.getMinimumDistance(new double[]{0,0,0}, new double[]{2.5,0.5,-10});
        assertEquals(Math.sqrt(0.5), dist, 1e-6);


        // Difficult case: Non-conventional unit cell
        double[][] basis = cell.getBasis();
        basis[1][0] = 108;
        cell.setBasis(basis);
        dist = cell.getMinimumDistance(new double[]{0,0,0}, new double[]{0.5,0.5,0});
        assertEquals(Math.sqrt(0.5), dist, 1e-6);
        dist = cell.getMinimumDistance(new double[]{0,0,0}, new double[]{5.5,0.5,0});
        assertEquals(Math.sqrt(0.5), dist, 1e-6);
        dist = cell.getMinimumDistance(new double[]{0,0,0}, new double[]{5.5,-10.5,0});
        assertEquals(Math.sqrt(0.5), dist, 1e-6);
    }

    @Test
    public void getClosestImage() throws Exception {
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 1)); // Origin
        cell.addAtom(new Atom(new double[]{0.75,0.75,0.75}, 1)); // C face center
        AtomImage image = cell.getClosestImage(0, 1);
        assertArrayEquals(new double[]{-0.25,-0.25,-0.25},
                image.getPosition().toArray(), 1e-6);
        assertArrayEquals(new int[]{-1,-1,-1}, image.getSupercell());

        // Hard tests: Non-conventional unit cell
        cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 1)); // Origin
        cell.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1)); // Body face center
        double[][] basis = cell.getBasis();
        basis[1][0] = 108;
        cell.setBasis(basis);
        image = cell.getClosestImage(0, 1);
        assertArrayEquals(new double[]{-0.5,-0.5,0.5},
                image.getPosition().toArray(), 1e-6);
        assertArrayEquals(new int[]{-1,53,0}, image.getSupercell());
        basis[1][0] = 108;
    }

    @Test
    public void testReplacement() throws Exception {
        // Make the original cell B2-CuZr
        Cell cell = new Cell();
        cell.addAtom(new Atom(new double[]{0,0,0}, 0));
        cell.addAtom(new Atom(new double[]{0.5,0.5,0.5}, 1));
        cell.setTypeName(0, "Cu");
        cell.setTypeName(1, "Zr");

        // Replace Cu with Ni
        Map<String,String> toChange = new TreeMap<>();
        toChange.put("Cu", "Ni");
        cell.replaceTypeNames(toChange);
        assertEquals("Ni", cell.getTypeName(0));
        assertEquals("Zr", cell.getTypeName(1));

        // Replace Ni with Cu, and Zr with Ti
        toChange.clear();
        toChange.put("Ni", "Cu");
        toChange.put("Zr", "Ti");
        cell.replaceTypeNames(toChange);
        assertEquals("Cu", cell.getTypeName(0));
        assertEquals("Ti", cell.getTypeName(1));

        // Exchange Cu and Ti
        toChange.clear();
        toChange.put("Cu", "Ti");
        toChange.put("Ti", "Cu");
        cell.replaceTypeNames(toChange);
        assertEquals("Ti", cell.getTypeName(0));
        assertEquals("Cu", cell.getTypeName(1));

        // Make everything Cu
        toChange.clear();
        toChange.put("Ti", "Cu");
        cell.replaceTypeNames(toChange);
        assertEquals("Cu", cell.getTypeName(0));
        assertEquals("Cu", cell.getTypeName(1));

        // Make everything W
        toChange.clear();
        toChange.put("Cu", "W");
        cell.replaceTypeNames(toChange);
        assertEquals("W", cell.getTypeName(0));
        assertEquals("W", cell.getTypeName(1));

        // Merge types
        cell.mergeLikeTypes();
        assertEquals(1, cell.nTypes());
    }
}
