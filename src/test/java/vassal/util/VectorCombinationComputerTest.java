package vassal.util;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Logan Ward
 */
public class VectorCombinationComputerTest {

    @Test
    public void testCombination() {
        Vector3D[] input = new Vector3D[3];
        input[0] = new Vector3D(1, 0, 0);
        input[1] = new Vector3D(0, 1, 0);
        input[2] = new Vector3D(0, 0, 1);

        VectorCombinationComputer computer = new VectorCombinationComputer(input, 1);

        assertArrayEquals(new double[]{1,0,0}, computer.computeVector(1, 0, 0), 1e-6);
        assertArrayEquals(new double[]{0,1,0}, computer.computeVector(0, 1, 0), 1e-6);
        assertArrayEquals(new double[]{0,0,1}, computer.computeVector(0, 0, 1), 1e-6);
        assertArrayEquals(new double[]{-2,5,3}, computer.computeVector(-2, 5, 3), 1e-6);
    }

    @Test
    public void testCollection() {
        Vector3D[] input = new Vector3D[3];
        input[0] = new Vector3D(1, 0, 0);
        input[1] = new Vector3D(0, 1, 0);
        input[2] = new Vector3D(0, 0, 1);

        VectorCombinationComputer computer = new VectorCombinationComputer(input, 1);

        assertEquals(7, computer.getVectors().size());

        computer = new VectorCombinationComputer(input, Math.sqrt(2) * 1.001);

        assertEquals(1 + 6 + 12, computer.getVectors().size());

        computer = new VectorCombinationComputer(input, Math.sqrt(2) * 1.001, false);

        assertEquals(6 + 12, computer.getVectors().size());

        computer = new VectorCombinationComputer(input, 8);
        assertEquals(computer.getVectors().size(),
                computer.getVectors().size());
    }

    @Test
    public void testCollectionOblique() {
        Vector3D[] input = new Vector3D[3];
        input[0] = new Vector3D(1, 0, 0);
        input[1] = new Vector3D(8, 1, 0);
        input[2] = new Vector3D(0, 1, 1);

        VectorCombinationComputer computer = new VectorCombinationComputer(input, 1);

        assertEquals(7, computer.getVectors().size());

        computer = new VectorCombinationComputer(input, Math.sqrt(2) * 1.001);

        assertEquals(1 + 6 + 12, computer.getVectors().size());
    }
}
