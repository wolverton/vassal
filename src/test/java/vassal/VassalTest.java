package vassal;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Logan Ward
 */
public class VassalTest {

    @Test
    public void testSomeMethod() throws Exception {
        Vassal.main(new String[]{"VASP5IO",
            "./test/1004-C1.vasp",
            "CoordinationPolyhedronToolkit",
            "atom"});
    }

}
